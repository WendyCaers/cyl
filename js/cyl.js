/** ------- JAVASCRIPTS FOR PROGRAM CYL -------- */

/** toevoegen van de eventlisteners
 * wordt uitgevoerd bij het laden
 */
function init() {
    //derde paramater slaat op useCapture: gaat de dom af van boven naar beneden; hier aldus
    //op false (=standaardwaarde) omdat we naar boven willen bubbelen.
    //login
    document.getElementById('loginClose').addEventListener("click", emptyLoginFields, false);
    document.getElementById('loginButton').addEventListener("click", login, false);
    document.getElementById('loginLink').addEventListener("click", loginLogout, false);
    //nagaan of men is ingelogd
    var link = document.getElementById('loginLink').innerHTML;
    if (link == 'afmelden') {
        //profielen
        document.getElementById('profileNew').addEventListener("click", showProfilePopup, false);
        document.getElementById('profileChange').addEventListener("click", showProfilePopup, false);
        document.getElementById('profileRemove').addEventListener("click", checkUseProfile, false);
        document.getElementById('profileAction').addEventListener("click", profileAction, false);
        document.getElementById('profileCancel').addEventListener("click", profileCancel, false);
        document.getElementById('close-popup-profile').addEventListener("click", profileCancel, false);
        document.getElementById('listProfiles').addEventListener("click", selectListItem, false);
        document.getElementById('imageUpload').addEventListener("change", loadImage, false);
        document.getElementById('imageRemove').addEventListener("click", removeCanvasProfile, false);
        document.getElementById('scaleImageFit').addEventListener("click", loadImage, false);
        document.getElementById('scaleImageFill').addEventListener("click", loadImage, false);
        //sluitplannen
        document.getElementById('listMkp').addEventListener("click", selectListItem, false);
        document.getElementById('searchMkp').addEventListener("input", searchMKPs ,false);
        document.getElementById('mkpConsult').addEventListener("click", consultMkp ,false);
        document.getElementById('mkpTable').addEventListener("click", consultCylOrKey ,false);
        document.getElementById('mkpNew').addEventListener("click", showMkpInput, false);
        document.getElementById('mkpChange').addEventListener("click", showMkpInput, false);
        document.getElementById('mkpRemove').addEventListener("click", deleteMkp, false);
        document.getElementById('mkpBasicsAction').addEventListener("click", mkpBasicsAction, false);
        document.getElementById('mkpBasicsCancel').addEventListener("click", mkpGoToPrevious, false);
        document.getElementById('mkpBasicsNext').addEventListener("click", proceedWithoutChanges, false);
        //cilinders
        document.getElementById('mkpCylNew').addEventListener("click", showCylinderInput, false);
        document.getElementById('mkpCylChange').addEventListener("click", showCylinderInput, false);
        document.getElementById('mkpCylRemove').addEventListener("click", checkUseCylinder, false);
        document.getElementById('inputCylAction').addEventListener("click", cylAction, false);
        document.getElementById('inputCylCancel').addEventListener("click", cylGoToPrevious, false);
        document.getElementById('listCyl').addEventListener("click", selectListItem, false);
        document.getElementById('mkpCylAction').addEventListener("click", goToKeys, false);
        document.getElementById('mkpCylCancel').addEventListener("click", mkpGoToPrevious, false);
        document.getElementById('mkpCylNext').addEventListener("click", proceedWithoutChanges, false);
        //sleutels
        document.getElementById('mkpKeyNew').addEventListener("click", showKeyInput, false);
        document.getElementById('mkpKeyChange').addEventListener("click", showKeyInput, false);
        document.getElementById('mkpKeyRemove').addEventListener("click", deleteKey, false);
        document.getElementById('inputKeyAction').addEventListener("click", keyAction, false);
        document.getElementById('inputKeyCancel').addEventListener("click", keyGoToPrevious, false);
        document.getElementById('listKey').addEventListener("click", selectListItem, false);
        document.getElementById('key-input-cyls').addEventListener("change", selectCylOnKey, false);
        document.getElementById('listCylsOnKey').addEventListener("click", selectListItem, false);
        document.getElementById('cylOnKeyDelete').addEventListener("click", deleteCylOnKey, false);
        document.getElementById('mkpKeyAction').addEventListener("click", mkpFinish, false);
        document.getElementById('mkpKeyCancel').addEventListener("click", mkpGoToPrevious, false);
        document.getElementById('mkpKeyNext').addEventListener("click", proceedWithoutChanges, false);
        //navigatielinks
        document.getElementById('backLinkProfiles').addEventListener("click", navigateBackProfiles, false);
        //nagaan of men is aangemeld als administrator
        if(document.getElementById('register')){
            document.getElementById('registerButton').addEventListener("click", register, false);
            document.getElementById('registerClose').addEventListener("click", emptyRegisterFields, false);
            document.getElementById('usersLink').addEventListener("click", openUsersPopup, false);
            document.getElementById('listUsers').addEventListener("click", selectListItem, false);
            document.getElementById('userAddButton').addEventListener("click", openRegisterPopup, false);
            document.getElementById('userDeleteButton').addEventListener("click", deleteUser, false);
        }
    }
}


/** ALGEMENE FUNCTIES 
 *
 * functies die gebruikt worden doorheen de hele applicatie of
 * door verschillende gedeelten van de applicatie
 */


/** bijhouden op welke sherm we bezig zijn via de hash */
function changeSessionState(){
    var hash = location.hash;
    sessionStorage.setItem("loaded", hash);
}

/** nagaan of er een item in een lijst is geselecteerd */
function checkItemSelection(value){
    if (value == 'profileUpdate' || value == 'profileDelete')
        var place = document.getElementById('third-floor');
    else if (value == 'masterKeyPlanUpdate' || value == 'masterKeyPlanDelete' || value == 'masterKeyPlanConsult')
        var place = document.getElementById('fourth-floor');
    else if (value == 'cylinderUpdate' || value == 'cylinderDelete' || value == 'keyUpdate' || value == 'keyDelete')
    //misschien deze beter via de container van ul doen ipv de floor omdat op dezelfde floor ook de sleutels zijn
    //niet meer nodig wanneer naar vorig panel wordt gegaan wordt selectie opgeheven
        var place = document.getElementById('sixth-floor');
    else if (value == 'cylOnKeyDelete')
        var place = document.getElementById('eighth-floor');
    else if (value == 'userDelete')
        var place = document.getElementById('users');
    var selected = place.getElementsByClassName('selected');
    if(selected.length == 0){
        alert('GEEN ITEM GESELECTEERD\n\nSelecteer eerst een item voor u verder gaat.');
        return -1;
    }
    else if(selected.length == 1){
        //geef het geselecteerde li element terug
        return selected[0];
    }
    else{
        alert('FOUTMELDING\n\nEr is een fout opgetreden bij de selectie.')
        return -1;
    }
}

/** afhandelen van het klikken op een li element in een ul */
function selectListItem(e){
    //uitzoeken of hetgeen waarop is geklikt een li element is
    if(e.target.nodeName == "LI") {
        //de class van de niet geselecteerde li elementen op none zetten en het 
        //geselecteerde op selected. Zo kan in de css het uiterlijk worden aangepast: 
        //kleur en vinkje
        var all_list_items = e.target.parentNode.childNodes;
        for (i = 0; i < all_list_items.length; i++){
            if (all_list_items[i] != e.target) all_list_items[i].className = "none";
        }
        e.target.className = "selected";

        //nagaan om welke ul lijst het gaat adhv een switch
        switch(e.target.parentNode.id){
            case 'listProfiles':
                //nagaan of het geselecteerde li element een foto heeft. Zo ja de foto tonen in
                //het img tag en anders het icoon in het span element
                if(e.target.id == ""){
                    document.getElementById('image-profile').style.display = 'none';
                    document.getElementById('icon-profile').setAttribute("data-icon", "h");
                }
                else{
                    document.getElementById('icon-profile').removeAttribute("data-icon");
                    var image = document.getElementById('image-profile');
                    image.style.display = 'block';
                    image.src = e.target.id + '?nocache=' + new Date().getTime();
                }
                break;
            case 'listMkp':
                //id van gekozen sluitplan opvragen om detail te tonen
                var id = e.target.value;
                if (id != ""){
                    //selectById doen
                    var my_ajax = new Ajax();
                    var data = '&id=' + id;
                    data += '&action=masterKeyPlanSelectById';
                    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), showMkpInDetail);
                }
                break;
            
        }
    }
}

/** door het plaatsen van een sluier-popup de UI blokkeren */
function disablePanel(popup){
    popup.style.display = 'block';
    popup.style.background = 'black';
    popup.style.padding = '0%';
    popup.style.zIndex = 2;
    if(popup.id == 'mkpVeilCylinders' || popup.id == 'mkpVeilKeys'){
        popup.style.top = '0%';
        popup.style.left = "0%";
        popup.style.width = "100%";
        popup.style.height = "85%";
        popup.style.opacity = '0';
    }
    else{
        popup.style.top = '-1%';
        popup.style.left = "-1%";
        popup.style.width = "102%";
        popup.style.height = "102%";
        popup.style.opacity = '0.4';
    }
}

/** opvullen van een ul element */
function fillUlElement(serverAnswer, ul, li_text, li_value){
     var ul = document.getElementById(ul);
    //lijst leegmaken
    var selectedValue = '';
    if (ul) {
            while (ul.firstChild) {
                if (ul.firstChild.className == 'selected') selectedValue = ul.firstChild.value;
                ul.removeChild(ul.firstChild);
            }
        }
    if(serverAnswer != false){
        var list = JSON.parse(serverAnswer);
        //James Edwards: http://www.sitepoint.com/sophisticated-sorting-in-javascript/
        //sort een array of objects
        list.sort(function(a, b)
        {
            var x = a[li_text].toLowerCase(), y = b[li_text].toLowerCase();
            //als x < y dan -1 anders als x > y dan 1 anders 0;
            return x < y ? -1 : x > y ? 1 : 0;
           
        });
        for (i = 0; i < list.length; i++){
            var li = document.createElement("LI");
            var textnode = document.createTextNode(list[i][li_text]);
            li.appendChild(textnode);
            //om attributen aan het li element toe te voegen zoals value
            li.setAttribute("value", list[i][li_value]);
            if (li.value == selectedValue) li.setAttribute("class", 'selected');
            ul.appendChild(li);
        }
    }
    else ul.innerHTML = 'GEEN RESULTATEN.';
}


/** VALIDATIE 
 *
 * functies om ingevoerde gegevens te valideren: integer check,
 * datumvalidatie, emailvalidatie en tags verwijderen
 */

function isInteger(value){
    //eerst checken of het een nummer is en dan nagaan of het een integer is
    if (isNaN(value)) return false;
    else if (value % 1 === 0) return true;
    else return false;
}

function isValidDate(value){
    var isDate = true;
    //eerst algemene datumnotatie checken 
    var isDateFormat = value.match(/^(0?[1-9]|[12][0-9]|3[01])\-(0?[1-9]|1[012])\-\d{4}$/);
    if (isDateFormat) {
        var dateArray = value.split("-");
        var m = dateArray[1];
        //februari en schrikkeljaar
        if (m == '02') {
            var isLeapYear = false;
            //als men een nieuwe datum invoert met de gegevens 29 februari en het is geen schrikkeljaar
            //zal javascript de datum zetten op 1 maart (js telt aldus gewoon verder op)
            if (new Date(dateArray[2], 1, 29).getMonth() == 1) isLeapYear = true;
            //hoeveel dagen
            if (isLeapYear) {
                if (dateArray[0] > 29) isDate = false;
            } 
            else if (dateArray[0] > 28) isDate = false;
        }
        //maanden met maar 30 dagen
        if (m == '04' || m == '06' || m == '09' || m == '11'){
            if (dateArray[0] > 30){
                isDate = false;
            }
        }
    }
    else isDate = false;  
    return isDate;
}

function isEmail(value){
    //http://www.w3resource.com/javascript/form/email-validation.php
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (value.match(mailformat)) return true;
    else return false;
}

/** Script by hscripts.com - aangepast aan eigen noden */
function stripTags(value){
var re = /(<([^>]+)>)/gi;
return value.replace(re, "");
}

/** tonen foutmelding verplicht veld */
function setErrorRequiredField(errorLabel, inputField){
    document.getElementById(errorLabel).innerHTML = '*';
    document.getElementById(inputField).setAttribute('placeholder', 'Verplicht veld');
    document.getElementById(inputField).style.borderColor = 'red';
}

/** tonen foutmelding integer check */
function setErrorIntegerFail(errorLabel, inputField){
    document.getElementById(errorLabel).innerHTML = 
             '<span class="tooltip">Geheel getal ingeven</span>*';
        document.getElementById(inputField).style.borderColor = 'red';
}

/** tonen foutmelding andere validaties */
function setErrorX(errorLabel, inputField, errorText){
    document.getElementById(errorLabel).innerHTML = 
            '<span class="tooltip">' + errorText + '</span>*';
        document.getElementById(inputField).style.borderColor = 'red';
}

/** tonen foutmelding in membership */
function setErrorMembership(errorLabel, inputField, errorText){
    document.getElementById(errorLabel).innerHTML = 
        '<span class="tooltip">' + errorText + '</span>';
    document.getElementById(inputField).style.borderColor = 'red';
    document.getElementById(inputField).style.backgroundColor = 'pink';
}


/** SECTIE PROFIELEN
 *
 * functies die worden gebruikt binnen het gedeelte profielen, onderverdeeld in
 * afbeeldingen, navigeren en aanpassingen.
*/


/** Afbeeldingen
 */

/** volledige afbeelding in canvas tonen (uitschalen) */
function scaleImageFit(image, canvas){
    //benodigde variabelen aanmaken
    var max_width = 400;
    var max_height = 420;
    var ratio_height_width = max_height / max_width;
    var width_to_be = image.width;
    var height_to_be = image.height;
    var height_pos = 0;
    var width_pos = 0;
    var image_ratio = image.height / image.width;
    //nagaan of afbeeldingbreedte te groot is adhv de ratio hoogte/breedte
    if (image.height / image.width < ratio_height_width) {
        width_to_be = max_width;
        height_to_be = width_to_be * image_ratio;
        height_pos = (max_height - height_to_be) / 2;
    }
    //indien de hoogte te groot is
    else {
        height_to_be = max_height;
        width_to_be = height_to_be / image_ratio;
        width_pos = (max_width - width_to_be) / 2;
    }
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvas.width = max_width;
    canvas.height = max_height;
    ctx.fillStyle = "rgba(0,0,0,0.2)";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(image, width_pos, height_pos, width_to_be, height_to_be);
}

/** canvas vullen met afbeelding (afbeeldingen bijknippen) */
function scaleImageFill(image, canvas){
    //benodigde variabelen aanmaken
    var max_width = 400;
    var max_height = 420;
    var ratio_height_width = max_height / max_width;
    var chop_width = 0;
    var chop_height = 0;
    var width_to_be = image.width;
    var height_to_be = image.height;

    //nagaan of afbeeldingbreedte te groot is adhv de ratio hoogte/breedte
    if (image.height / image.width < ratio_height_width) {
        //de juiste breedte zoeken
        width_to_be = image.height / ratio_height_width;
        //af te knippen stuk van breedte afbeelding zoeken zodat het
        //midden van de afbeelding wordt genomen
        chop_width = (image.width - width_to_be) / 2;
    }
    //indien de hoogte te groot is
    else {
        height_to_be = image.width * ratio_height_width;
        chop_height = (image.height - height_to_be) / 2;
    }
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvas.width = max_width;
    canvas.height = max_height;
    ctx.drawImage(image, chop_width, chop_height, width_to_be, height_to_be, 0, 0, canvas.width, canvas.height);
}

/** afbeelding op canvas plaatsen */
function loadImage(e){
    if (document.getElementById('imageUpload').value == '') return;
    var canvas = document.getElementById('canvas-profile-popup');
    if(canvas.display != 'block') showCanvasProfile();
    var control = document.getElementById('imageUpload');
    if (control.files.length == 1){
        var image_file = control.files[0];
        if (!image_file.type.match(/image.*/)) {
            alert('FOUTMELDING\n\nHet bestand is geen afbeelding.')
        }
        else {
            var image = new Image();
            image.onload = function () {
                if (e.target.id == 'imageUpload' || e.target.id == 'scaleImageFill') {
                    scaleImageFill(image, canvas);
                }
                else if (e.target.id == 'scaleImageFit') {
                    scaleImageFit(image, canvas);
                }
            }
            image.src = window.URL.createObjectURL(image_file);
        }
    }
    else{
        //wanneer er niets of meerdere files in de input geselecteerd zijn
        alert('FOUTMELDING\n\nProbleem bij opladen van afbeelding.')
    }
}

/** canvas tonen en img met afbeelding van fototoestel verbergen */
function showCanvasProfile(){
    document.getElementById('image-profile-popup').style.display = 'none';
    document.getElementById('canvas-profile-popup').style.display = 'inline-block';
    document.getElementById('imageRemove').style.display = 'inline';
    document.getElementById('scaleImageFit').style.display = 'inline';
    document.getElementById('scaleImageFill').style.display = 'inline';
}

/** canvas verbergen en img met afbeelding van fototoestel tonen */
function removeCanvasProfile(){
    document.getElementById('image-profile-popup').style.display = 'inline-block';
    document.getElementById('canvas-profile-popup').style.display = 'none';
    document.getElementById('imageRemove').style.display = 'none';
    document.getElementById('scaleImageFit').style.display = 'none';
    document.getElementById('scaleImageFill').style.display = 'none';
    document.getElementById('imageUpload').value = '';

}


/** Navigeren profielen
*/

/** terugnavigeren met controlpanel wanneer popup open staat bij profiles */
function navigateBackProfiles(){
    if(sessionStorage.getItem('loaded') == '#to-profile-input')
        document.getElementById('backLinkProfiles').href = '#third-floor';
    else document.getElementById('backLinkProfiles').href = '#second-floor';
}

/** openen popup profielen */
function showProfilePopup(){
    //bij een update eerst controleren of er een selectie is gemaakt
    //en het profiel in de db opvragan via ID
    if(this.value == 'profileUpdate'){
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        //selectbyid uitvoeren omdat we brand and type niet apart uit het
        //li element kunnen halen
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        data += '&action=profileSelectById';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInProfile);
    }
    else{
        //velden leegmaken
        document.getElementById('profile-brand').value = '';
        document.getElementById('profile-type').value = '';
        document.getElementById('profile-brand').removeAttribute('placeholder');
        document.getElementById('profile-type').removeAttribute('placeholder');
        document.getElementById('profile-brand').style.border = '1px solid rgba(0,0,0,0.6)';
        document.getElementById('profile-type').style.border = '1px solid rgba(0,0,0,0.6)';

    }
    location.href = "#to-profile-input";
    document.getElementById('profile-brand-error').innerHTML = '';
    document.getElementById('profile-type-error').innerHTML = '';
    //om bij nieuwe invoer zonder afbeelding de display van canvas op 'none' te zetten ipv ""
    removeCanvasProfile()
    document.getElementById('profileAction').value = this.value;
}

/** velden popup profielen invullen */
function fillInProfile(serverAnswer){
    try{
        var profile = JSON.parse(serverAnswer);
        document.getElementById('profile-brand').value = profile['Brand'];
        document.getElementById('profile-type').value = profile['Type'];
        var filepath = profile['Img'];
        if (filepath != null){
            showCanvasProfile();
            var image = new Image();
            image.onload = function () {
                var canvas = document.getElementById('canvas-profile-popup');
                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                canvas.width = image.width;
                canvas.height = image.height;
                ctx.drawImage(image, 0, 0, image.width, image.height);
            }
            // extra unique datastring met datum/tijd meegeven in de query om er zeker
            // van te zijn dat hij de file op de server gaat halen en niet uit de cache
            image.src = filepath + '?nocache=' + new Date().getTime();
        }
        else{
            removeCanvasProfile();
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nProfiel kan NIET worden opgehaald.');
    }
}

/** annuleren van de bewerking */
function profileCancel(){
    location.href = "#third-floor";
    //velden leegmaken
    document.getElementById('profile-brand').value = '';
    document.getElementById('profile-type').value = '';
    document.getElementById('imageUpload').value = '';
    //canvas leegmaken
    var canvas = document.getElementById('canvas-profile-popup');
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //canvas niet meer tonen
    removeCanvasProfile();
}


/** Aanpassingen profielen
 */

/** toevoegen of wijzigen van een profiel */
function profileAction(){
    var brand = document.getElementById('profile-brand').value;
    var type = document.getElementById('profile-type').value;
    document.getElementById('profile-brand-error').innerHTML = '';
    document.getElementById('profile-type-error').innerHTML = '';
    document.getElementById('profile-brand').style.border = '1px solid rgba(0,0,0,0.6)';
    document.getElementById('profile-type').style.border = '1px solid rgba(0,0,0,0.6)';
    //tags verwijderen
    brand = stripTags(brand);
    type = stripTags(type);
    //validatie op verplichte velden
    if(brand.length == 0 || type.length == 0){
        if (brand.length == 0) {
            setErrorRequiredField('profile-brand-error', 'profile-brand');
        }
        if (type.length == 0){
            setErrorRequiredField('profile-type-error', 'profile-type');
        } 
        return;
    }
    //ajax
    var canvas = document.getElementById('canvas-profile-popup');
    if (brand.length > 0) {
        var data = '&brand=';
        data += brand;
    }
    else {
        var data = '';
    }
    if (type.length > 0) {
        data += '&type=';
        data += type;
    }
    if(this.value == 'profileUpdate'){
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        var id = item_selected.value;
        if (id > 0){
            data += '&id=';
            data += item_selected.value;
        }
        if(item_selected.id.length > 0){
            data += '&path=';
            data += item_selected.id;
        }
    }
    if (canvas.style.display != 'none'){
        var image = canvas.toDataURL("image/png");
        data += '&img=' + image;
    }
    data += '&action=' + this.value;
    var my_ajax = new Ajax();
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), refreshPageAfterChanges);
    location.href = "#third-floor";
}

/** eerste stap van verwijderen: controle of het profiel nog in gebruik is door een MKP  */
function checkUseProfile(){
    var item_selected = checkItemSelection(this.value);
    if (item_selected == -1) return;
    var confirmed = confirm('PROFIEL VERWIJDEREN\n\nBent u zeker dat u dit profiel wenst te verwijderen');
    if (confirmed == true) {
        var my_ajax = new Ajax();
        var data = '&idProfile=' + item_selected.value;
        data += '&action=masterKeyPlanSelectByIdProfile';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), deleteProfile);
    }
    else return;
}

/** verwijderen van een profiel */
function deleteProfile(serverAnswer){
    if (serverAnswer != false){
        alert('OPMERKING\n\nHet profiel kan NIET verwijderd worden.\n' + 
            'Er zijn nog sluitplannen die van dit profiel gebruik maken.');
    }
    else {
        var button = document.getElementById('profileRemove');
        var item_selected = checkItemSelection(button.value);
        if (item_selected == -1) return;
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        if(item_selected.id.length > 0) data += '&path=' + item_selected.id;
        data += '&action=' + button.value;
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), refreshPageAfterChanges);
    }
}

/** opvangen serverantwoord na het maken van aanpassingen */
function refreshPageAfterChanges(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        if(answer[1] == true){
            //true om van server te reloaden en niet van cache
            location.reload(true);
        }
        else{
            if (answer[0] == "profileInsert") 
                alert('FOUTMELDING\n\nProfiel is NIET ingevoerd.\n\n(error: ' + answer[2].toString() + ')');
            if (answer[0] == "profileUpdate") 
                alert('FOUTMELDING\n\nProfiel is NIET gewijzigd.\n\n(error: ' +  answer[2].toString() + ')');
            if (answer[0] == "profileDelete") 
                alert('FOUTMELDING\n\nProfiel is NIET verwijderd.\n\n(error: ' + answer[2].toString() + ')');
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is iets misgelopen. Probeer later nog een keer.');
    }
}


/** SECTIE SLUITPLANNEN
 *
 * functies die worden gebruikt binnen het gedeelte sluitplannen, onderverdeeld in
 * navigeren en aanpassingen
*/


/** Navigeren sluitplannen
 */

 /** errors verwijderen*/
 function removeErrorLabelsMkp(){
    document.getElementById('mkp-error-number').innerHTML = '';
    document.getElementById('mkp-error-date').innerHTML = '';
    document.getElementById('mkp-error-numberOfCyls').innerHTML = '';
    document.getElementById('mkp-error-numberOfKeys').innerHTML = '';
    document.getElementById('mkp-input-number').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('mkp-input-date').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('mkp-input-numberOfCyls').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('mkp-input-numberOfKeys').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('mkp-input-number').removeAttribute('placeholder');
    document.getElementById('mkp-input-date').removeAttribute('placeholder');
    document.getElementById('mkp-input-numberOfCyls').removeAttribute('placeholder');
    document.getElementById('mkp-input-numberOfKeys').removeAttribute('placeholder');
 }

/** sluitplannen zoeken adhv zoekvenster */
function searchMKPs(){
    //zorgen dat details gekozen sluitplan verdwijnen en icoon terug zichtbaar wordt
    document.getElementById('MkpStartBasics').style.display = 'none';
    document.getElementById('icon-mkp').style.display = 'block';
    //sluitplannen opvragen die voldoen aan zoekcriteria
    var my_ajax = new Ajax();
    var number = document.getElementById("searchMkp").value;
    number = stripTags(number);
    if(number.length > 0){
        var data = '&number=' + number;
        data += '&action=masterKeyPlanSelectLikeXNumber';
    }
    else{
        var data = '&action=masterKeyPlanSelectAll';
    }
    
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), showListMKPs);
}

/** sluitplannen tonen die voldoen aan de criteria opgegeven in het zoekvenster */
function showListMKPs(serverAnswer){
    fillUlElement(serverAnswer, 'listMkp', 'Number', 'Id');
    var ul = document.getElementById('listMkp');
    for (var i = 0; i < ul.getElementsByTagName('LI').length; i++ ){
        if(ul.childNodes[i].className == 'selected'){
            var my_ajax = new Ajax();
            var data = '&id=' + ul.childNodes[i].value;
            data += '&action=masterKeyPlanSelectById';
            my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), showMkpInDetail);
        }
    }
}

/** het detailluik vullen met gegevens van het gekozen sluitplan */
function showMkpInDetail(serverAnswer){
    if (serverAnswer != false) {
        document.getElementById('icon-mkp').style.display = 'none';
        document.getElementById('MkpStartBasics').style.display = 'block';
        var mkp = JSON.parse(serverAnswer);
        document.getElementById('mkpNumber').innerHTML = mkp['Number'];
        //opgehaalde datum in ons formaat overzetten en opvangen wanneer dit niet lukt
        try{
            var date = new Date(mkp['Date']);
            var d = date.getDate();
            var day = d > 9 ? "" + d : "0" + d;
            var m = date.getMonth() + 1;
            var month = m > 9 ? "" + m : "0" + m;
            var year = date.getFullYear();
            var customDate = day + '-' + month + '-' + year;
        }
        catch (e){
            var customDate = 'NN';
        }
        document.getElementById('mkpDate').innerHTML = customDate ;
        document.getElementById('mkpProfile').innerHTML = mkp['Brand'] + ' ' + mkp['Type'];
        if (mkp['Invoice'] != null) document.getElementById('mkpInvoice').innerHTML = mkp['Invoice'];
        else document.getElementById('mkpInvoice').innerHTML = '-';
        if (mkp['Description'] != null) document.getElementById('mkpDescription').innerHTML = mkp['Description'];
        else document.getElementById('mkpDescription').innerHTML = '';
        //aantal cilinders en sleutels invullen, hiervoor moeten we ze eerst ophalen
        var idMkp = mkp['Id'];
        if (idMkp > 0){
            //cilinders
            var dataCyls = '';
            dataCyls += '&idMkp=' + idMkp;
            dataCyls += '&action=cylinderSelectByIdMasterKeyPlan';
            var my_ajax1 = new Ajax();
            my_ajax1.postRequest('appcode/control/ajax-controller.php', dataCyls.substring(1), showNumberOfCyls);
            //sleutels
            var dataKeys = '';
            dataKeys += '&idMkp=' + idMkp;
            dataKeys += '&action=keySelectByIdMasterKeyPlan';
            var my_ajax2 = new Ajax();
            my_ajax2.postRequest('appcode/control/ajax-controller.php', dataKeys.substring(1), showNumberOfKeys);
        }
        else{
            document.getElementById('mkpNumberOfCyls').innerHTML = 'NA';
            document.getElementById('mkpNumberOfKeys').innerHTML = 'NA';
        }
    }
}

/** het aantal cilinders tonen in het detailluik */
function showNumberOfCyls(serverAnswer){
    if(serverAnswer != false){
        var answer = JSON.parse(serverAnswer);
        document.getElementById('mkpNumberOfCyls').innerHTML = answer.length;
    }
    else document.getElementById('mkpNumberOfCyls').innerHTML = 'NA';
}

/** het aantal sleutelstonen in het detailluik */
function showNumberOfKeys(serverAnswer){
    if(serverAnswer != false){
        var answer = JSON.parse(serverAnswer);
        document.getElementById('mkpNumberOfKeys').innerHTML = answer.length;
    }
    else document.getElementById('mkpNumberOfKeys').innerHTML = 'NA';
}

/** gekozen sluitplan raadplegen */
function consultMkp(){
    //eerst checken of er een selectie gemaakt is
    var item_selected = checkItemSelection('masterKeyPlanConsult');
    if (item_selected == -1) return;
    var idMkp = item_selected.value;
    location.href = '#fifth-floor';
    //basisgegevens overzetten van detail vorige pagina
    document.getElementById('mkp-consult-number').innerHTML = document.getElementById('mkpNumber').innerHTML;
    document.getElementById('mkp-consult-date').innerHTML = document.getElementById('mkpDate').innerHTML;
    document.getElementById('mkp-consult-profile').innerHTML = document.getElementById('mkpProfile').innerHTML;
    document.getElementById('mkp-consult-invoice').innerHTML = document.getElementById('mkpInvoice').innerHTML;
    document.getElementById('mkp-consult-description').innerHTML = document.getElementById('mkpDescription').innerHTML;
    document.getElementById('mkp-consult-numberOfCyls').innerHTML = document.getElementById('mkpNumberOfCyls').innerHTML;
    document.getElementById('mkp-consult-numberOfKeys').innerHTML = document.getElementById('mkpNumberOfKeys').innerHTML;
    //alle gegevens uit de db ophalen om de kruistabel te kunnen tekenen
    if (idMkp > 0) {
        var data = '';
        data += '&idMkp=' + idMkp;
        data += '&action=masterKeyPlanConsult';
        var my_ajax = new Ajax();
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), drawMkpTable);
    }
    
}

/** kruistabel maken van het gekozen sluitplan */
function drawMkpTable(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        //antwoord opsplitsen in cilinders en sleutels
        var cyls = answer[0];
        var keys = answer[1];
        //tabel opvragen en leegmaken
        var table = document.getElementById('mkpTable');
        table.innerHTML = '';
        //als er cilinders en sleutels zijn opgehaald
        if (cyls != false && keys != false) {
            //cilinders sorteren naar index
            cyls.sort(function (a, b) {
                var x = a['Index'].toLowerCase(), y = b['Index'].toLowerCase();
                //als x < y dan -1 anders als x > y dan 1 anders 0;
                return x < y ? -1 : x > y ? 1 : 0;

            });
            //sleutels sorteren naar index
            keys.sort(function (a, b) {
                var x = a['Index'].toLowerCase(), y = b['Index'].toLowerCase();
                //als x < y dan -1 anders als x > y dan 1 anders 0;
                return x < y ? -1 : x > y ? 1 : 0;

            });
            //eerste rij van de tabel aanmaken voor de sleutelnamen
            var trHeader = document.createElement('tr');
            table.appendChild(trHeader);
            //eerste table-header aanmaken, hier komt geen knop
            var thEmpty = document.createElement('th');
            thEmpty.innerHTML = 'CIL \\ SL';
            thEmpty.style.color = 'rgba(0,0,0,0.5)';
            trHeader.appendChild(thEmpty);
            //de andere th's van de eeste rij opvullen met knoppen, 1 knop per sleutel
            //sleutelnaam meegeven als text en sleutel-id als value van de knop
            //classname op 'key' zetten
            for (var i = 0; i < keys.length; i++) {
                var th = document.createElement('th');
                var button = document.createElement('button');
                button.value = keys[i]['Id'];
                button.textContent = keys[i]['Name'];
                button.className = 'key';
                th.appendChild(button);
                trHeader.appendChild(th);
            }
            //de andere rijen aanmaken, zoveel als er cilinders zijn
            for (var i = 0; i < cyls.length; i++) {
                var tr = document.createElement('tr');
                table.appendChild(tr);
                //telkens de eerste cel van elke rij opvullen met een knop voor de cilinder.
                //cilindernaam als text, cilinder-id als value, classname op 'cyl' zetten.
                var th = document.createElement('th');
                var button = document.createElement('button');
                button.value = cyls[i]['Id'];
                button.textContent = cyls[i]['Name'];
                button.className = 'cyl';
                th.appendChild(button);
                tr.appendChild(th);
                //per rij de sleutels van het sluitplan doorlopen en een vakje(td) in de tabel
                //aanmaken voor elke sleutel
                for (var j = 0; j < keys.length; j++) {
                    var td = document.createElement('td');
                    td.style.fontSize = '200%';
                    //van de cilinder waar we mee bezig zijn de gekoppelde sleutels 
                    //doorlopen en nagaan of 1 van de id's van deze gekoppelde sleutels
                    //overeenkomt met de id van de sleutel waarvoor we een vakje hebben
                    //aangemaakt. Is dit het geval, een icoontje van een sleutel in
                    //dat vakje plaatsen.
                    for (var x = 0; x < cyls[i]['Keys'].length; x++) {
                        if (keys[j]['Id'] == cyls[i]['Keys'][x]['IdKey']) {
                            td.className = 'icon';
                            td.setAttribute('data-icon', 'a');
                            td.heigth = '30%';
                        }
                    
                    }
                    tr.appendChild(td);
                }
            }
        }
        //als er geen cilinders en/of sleutels zijn opgehaald
        else if (cyls == false) alert('FOUTMELDING\n\n Er is iets fout gelopen bij het ophalen' + 
            'van de cilinders. Er kan geen tabel getoond worden.');
        else if (keys == false) alert('FOUTMELDING\n\n Er is iets fout gelopen bij het ophalen' +  
            'van de sleutels. Er kan geen tabel getoond worden.');
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is iets misgelopen met het ophalen van de gegevens.');
    }
}

/** raadplegen van een bepaalde cilinder of sleutel */
function consultCylOrKey(e){
    if(e.target.nodeName == "BUTTON"){
        switch (e.target.className){
            case 'cyl':
                location.href = '#seventh-floor';
                document.getElementById('backLinkCylinders').href = '#fifth-floor';
                document.getElementById('inputCylCancel').value = 'consult';
                document.getElementById('inputCylAction').style.display = 'none';
                disablePanel(document.getElementById('mkpVeilCylinders'));
                var idCyl = e.target.value;
                if (idCyl > 0) {
                    var data = '';
                    data += '&id=' + idCyl;
                    data += '&action=cylinderSelectById';
                    var my_ajax = new Ajax();
                    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInCylinder);
                }
                break;
            case 'key':
                location.href = '#eighth-floor';
                document.getElementById('backLinkKeys').href = '#fifth-floor';
                document.getElementById('inputKeyCancel').value = 'consult';
                document.getElementById('inputKeyAction').style.display = 'none';
                disablePanel(document.getElementById('mkpVeilKeys'));
                var idKey = e.target.value;
                if (idKey > 0) {
                    var data = '';
                    data += '&id=' + idKey;
                    data += '&action=keySelectById';
                    var my_ajax = new Ajax();
                    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInKey);
                }
                break;
        }
    }
}

/** tonen invoerscherm sluitplan */
function showMkpInput(){
    if(this.value == 'masterKeyPlanUpdate'){
        //eerst checken of er een selectie gemaakt is
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        var idMkp = item_selected.value;
        //basisgegevens overzetten van detail vorige pagina
        document.getElementById('mkp-input-number').value = document.getElementById('mkpNumber').innerHTML;
        document.getElementById('mkp-input-date').value = document.getElementById('mkpDate').innerHTML;
        if (document.getElementById('mkpInvoice').innerHTML != '-')
            document.getElementById('mkp-input-invoice').value = document.getElementById('mkpInvoice').innerHTML;
        document.getElementById('mkp-input-description').value = document.getElementById('mkpDescription').innerHTML;
        document.getElementById('mkp-input-numberOfCyls').value = document.getElementById('mkpNumberOfCyls').innerHTML;
        document.getElementById('mkp-input-numberOfKeys').value = document.getElementById('mkpNumberOfKeys').innerHTML;
        //profiel overzetten van vorige pagina
        var profiel = document.getElementById('mkpProfile').innerHTML;
        var select = document.getElementById('mkp-input-profile');
        for (var i = 0 ; i < select.options.length ; i++){
            if (select.options[i].text == profiel) select.selectedIndex = i;
        }
        //cilinders en sleutels opvragen via id van sluitplan
        if (idMkp > 0){
            //cilinders
            var dataCyls = '';
            dataCyls += '&idMkp=';
            dataCyls += idMkp;
            dataCyls += '&action=cylinderSelectByIdMasterKeyPlan';
            var my_ajax1 = new Ajax();
            my_ajax1.postRequest('appcode/control/ajax-controller.php', dataCyls.substring(1), fillListCylinders);
            //sleutels
            var dataKeys = '';
            dataKeys += '&idMkp=';
            dataKeys += idMkp;
            dataKeys += '&action=keySelectByIdMasterKeyPlan';
            var my_ajax2 = new Ajax();
            my_ajax2.postRequest('appcode/control/ajax-controller.php', dataKeys.substring(1), fillListKeys);
        }
        //name attribuut van buttons cilinders en sleutels zetten op de id mkp
        document.getElementById('inputCylAction').name = idMkp;
        document.getElementById('mkpCylRemove').name = idMkp;
        document.getElementById('inputKeyAction').name = idMkp;
        document.getElementById('mkpKeyRemove').name = idMkp;
        //pijlen naar rechts zichtbaar maken
        document.getElementById('mkpBasicsNext').style.visibility = 'visible';
        document.getElementById('mkpCylNext').style.visibility = 'visible';
        document.getElementById('mkpKeyNext').style.visibility = 'visible';
        //nog aanwezige vinkjes in ul cilinders en sleutels verwijderen
        var all_list_items_cyl = document.getElementById('listCyl').childNodes;
        for (i = 0; i < all_list_items_cyl.length; i++){
            all_list_items_cyl[i].className = "none";
        }
        var all_list_items_key = document.getElementById('listKey').childNodes;
        for (i = 0; i < all_list_items_key.length; i++){
            all_list_items_key[i].className = "none";
        }
    }
    else{
        //eventuele selectie in lijst sluitplannen wissen door
        //alle classnames op 'none' te zetten
        var ul = document.getElementById('listMkp');
        var children = ul.getElementsByTagName('LI');
        for (i = 0; i < children.length; i++){
            children[i].className = 'none';
        }
        searchMKPs();
        //fields leegmaken
        document.getElementById('mkp-input-number').value = '';
        document.getElementById('mkp-input-date').value = '';
        document.getElementById('mkp-input-invoice').value = '';
        document.getElementById('mkp-input-description').value = '';
        document.getElementById('mkp-input-numberOfCyls').value = '';
        document.getElementById('mkp-input-numberOfKeys').value = '';
        document.getElementById('mkp-input-profile').selectedIndex = 0;
        var ulCyl = document.getElementById('listCyl');
        if (ulCyl) {
            while (ulCyl.firstChild) {
                ulCyl.removeChild(ulCyl.firstChild);
            }
        }
        var ulKey = document.getElementById('listKey');
        if (ulKey) {
            while (ulKey.firstChild) {
                ulKey.removeChild(ulKey.firstChild);
            }
        }
        //pijlen naar rechts onzichtbaar maken
        document.getElementById('mkpBasicsNext').style.visibility = 'hidden';
        document.getElementById('mkpCylNext').style.visibility = 'hidden';
        document.getElementById('mkpKeyNext').style.visibility = 'hidden';
    }
    location.href = "#sixth-floor";
    disablePanel(document.getElementById('mkpInputVeil2'));
    disablePanel(document.getElementById('mkpInputVeil3'));
    document.getElementById('mkpInputVeil1').style.display = 'none';
    document.getElementById('mkpVeilCylinders').style.display = 'none';
    document.getElementById('mkpVeilKeys').style.display = 'none';
    //errorvelden leegmaken
    removeErrorLabelsMkp();
    //value insert of update aan de actionknop meegeven
    document.getElementById('mkpBasicsAction').value = this.value;
}

/** lijst gekoppelde cilinders vullen */
function fillListCylinders(serverAnswer){
    fillUlElement(serverAnswer, 'listCyl', 'Name', 'Id');
}

/** lijst gekoppelde sleutels vullen */
function fillListKeys(serverAnswer){
    fillUlElement(serverAnswer, 'listKey', 'Name', 'Id');
}

/** afhandelen terugknoppen op het invoerscherm */
function mkpGoToPrevious(){
    switch(this.id){
        case 'mkpBasicsCancel':
            location.href = '#fourth-floor';
            break;

        case 'mkpCylCancel':
            disablePanel(document.getElementById('mkpInputVeil2'));
            disablePanel(document.getElementById('mkpInputVeil3'));
            document.getElementById('mkpInputVeil1').style.display = 'none';
            //selectie verwijderen uit ul cilinders
            var all_list_items = document.getElementById('listCyl').childNodes;
            for (i = 0; i < all_list_items.length; i++) {
                all_list_items[i].className = "none";
            }
            //actieknop van de basisgegevens op update zetten want als je met een nieuw bezig bent
            //en je teruggaat om iets aan de basisgegevens te wijzigen moet dit als een update aanzien
            //worden en niet als een insert.
            document.getElementById('mkpBasicsAction').value = 'masterKeyPlanUpdate';
            document.getElementById('mkpBasicsAction').name = 'masterKeyPlanInsertChange';
            break;

        case 'mkpKeyCancel':
            disablePanel(document.getElementById('mkpInputVeil1'));
            disablePanel(document.getElementById('mkpInputVeil3'));
            document.getElementById('mkpInputVeil2').style.display = 'none';
            //selectie verwijderen uit ul cilinders
            var all_list_items = document.getElementById('listKey').childNodes;
                for (i = 0; i < all_list_items.length; i++){
                    all_list_items[i].className = "none";
                }
            break;

    }
}

/** afhandelen verderknoppen op het invoerscherm */
function proceedWithoutChanges(){
    switch(this.id){
        case 'mkpBasicsNext':
            //basisgegevens overzetten van detail vorige pagina moest de gebruiker al iets hebben gewijzigd
            document.getElementById('mkp-input-number').value = document.getElementById('mkpNumber').innerHTML;
            document.getElementById('mkp-input-date').value = document.getElementById('mkpDate').innerHTML;
            if (document.getElementById('mkpInvoice').innerHTML != '-')
                document.getElementById('mkp-input-invoice').value = document.getElementById('mkpInvoice').innerHTML;
            document.getElementById('mkp-input-description').value = document.getElementById('mkpDescription').innerHTML;
            document.getElementById('mkp-input-numberOfCyls').value = document.getElementById('mkpNumberOfCyls').innerHTML;
            document.getElementById('mkp-input-numberOfKeys').value = document.getElementById('mkpNumberOfKeys').innerHTML;
            var profiel = document.getElementById('mkpProfile').innerHTML;
            var select = document.getElementById('mkp-input-profile');
            for (var i = 0 ; i < select.options.length ; i++){
                if (select.options[i].text == profiel) select.selectedIndex = i;
            }
            //paneel cilinders enablen en de andere 2 disablen
            document.getElementById('mkpInputVeil2').style.display = 'none';
            disablePanel(document.getElementById('mkpInputVeil1'));
            disablePanel(document.getElementById('mkpInputVeil3'));
            break;

        case 'mkpCylNext':
            goToKeys();
            break;

        case 'mkpKeyNext':
            mkpFinish();
            break;

    }
    
}


/** Aanpassingen sluitplannen
 */

/** toevoegen of wijzigen sluitplan */
function mkpBasicsAction(){
    //variabelen voor datum in op te slaan na validatie
    var date;
    //gegevens uit textvelden halen
    var number = document.getElementById('mkp-input-number').value;
    var dateInput = document.getElementById('mkp-input-date').value;
    var invoice = document.getElementById('mkp-input-invoice').value;
    var numberOfCyls = document.getElementById('mkp-input-numberOfCyls').value;
    var numberOfKeys = document.getElementById('mkp-input-numberOfKeys').value;
    var description = document.getElementById('mkp-input-description').value;
    //profileId uit select halen
    var select = document.getElementById('mkp-input-profile');
    var index = select.selectedIndex;
    var profileId = select.options[index].value;
    //errorlabels leegmaken
    removeErrorLabelsMkp();

    //VALIDATIE
    var isValid = true;
    //tags verwijderen
    number = stripTags(number);
    dateInput = stripTags(dateInput);
    invoice = stripTags(invoice);
    numberOfCyls = stripTags(numberOfCyls);
    numberOfKeys = stripTags(numberOfKeys);
    description = stripTags(description);
    //sluitplannummer: verplicht veld + uniek
    if (number.length == 0){
        setErrorRequiredField('mkp-error-number', 'mkp-input-number');
        isValid = false;
    }
    else{
        //uniek nummer controleren
        //alle bestaande namen ophalen via lijst sluitplannen
        var ul = document.getElementById('listMkp');
        var listMkp = ul.getElementsByTagName('LI');
        for (var i=0 ; i < listMkp.length; i++){
            //het geselecteerde item niet meerekenen (voor update)
            if(listMkp[i].className != 'selected'){
                //trim() om spaties vooraan en achteraan te verwijderen
                if(listMkp[i].innerHTML.trim() == number.trim()){
                    setErrorX('mkp-error-number', 'mkp-input-number', 'Dit sluitplannummer is reeds in gebruik');
                    isValid = false;
                }
            }
        }
    }
    //datum: verplicht veld + notatiecheck
    if (dateInput.length == 0){
        setErrorRequiredField('mkp-error-date', 'mkp-input-date');
        isValid = false;
    }
    else if(dateInput.length > 0){
        var isDate = isValidDate(dateInput);
        if (!isDate){
            setErrorX('mkp-error-date', 'mkp-input-date', 'Ongeldige datum! (dd-mm-yyyy)');
            isValid = false;
        } 
    }
    //aantal cilinders: verplicht veld + integer check + groter of gelijk aan 2
    if (numberOfCyls.length == 0){
        setErrorRequiredField('mkp-error-numberOfCyls', 'mkp-input-numberOfCyls');
        isValid = false;
    }
    else if(!isInteger(numberOfCyls) ){
        setErrorIntegerFail('mkp-error-numberOfCyls', 'mkp-input-numberOfCyls');
        isValid = false;
    }
    else if (numberOfCyls < 2){
        setErrorX('mkp-error-numberOfCyls', 'mkp-input-numberOfCyls', '2 of meer cilinders ingeven');
        isValid = false;
    }
     //aantal cilinders: verplicht veld + integer check + groter of gelijk aan 2
    if (numberOfKeys.length == 0){
        setErrorRequiredField('mkp-error-numberOfKeys', 'mkp-input-numberOfKeys');
        isValid = false;
    }
    else if(!isInteger(numberOfKeys)){
        setErrorIntegerFail('mkp-error-numberOfKeys', 'mkp-input-numberOfKeys');
        isValid = false;
    }
    else if (numberOfKeys < 2){
        setErrorX('mkp-error-numberOfKeys', 'mkp-input-numberOfKeys', '2 of meer sleutels ingeven');
        isValid = false;
    }
    //alles de validatie niet slaagt terugkeren naar UI
    if (!isValid) return;

    //datum in formaat plaatsen om door te geven aan server
    var dateArray = dateInput.split("-");
    date = dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];

    if (number.length > 0) {
        var data = '&number=';
        data += number;
    }
    else {
        var data = '';
    }
    if (date.length > 0) {
        data += '&date=';
        data += date;
    }
    if (profileId.length > 0) {
        data += '&profileId=';
        data += profileId;
    }
    if (invoice.length > 0) {
        data += '&invoice=';
        data += invoice;
    }
    if (description.length > 0) {
        data += '&description=';
        data += description;
    }

    if(this.value == 'masterKeyPlanUpdate'){
        var id;
        if (this.name == 'masterKeyPlanInsertChange'){
            id =  document.getElementById('inputCylAction').name;
            this.name = "";
        }
        else{
            var item_selected = checkItemSelection(this.value);
            if (item_selected == -1) return;
            id = item_selected.value;
            
        }
        if (id > 0){
            data += '&id=';
            data += id;
        }
    }

    data += '&action=' + this.value;
    var my_ajax = new Ajax();
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), goToCylinders);
}

/** verwijderen sluitplan */
function deleteMkp(){
    var item_selected = checkItemSelection(this.value);
    if (item_selected == -1) return;
    var confirmed = confirm('SLUITPLAN VERWIJDEREN\n\nBent u zeker dat u dit sluitplan wenst te verwijderen');
    if (confirmed == true) {
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        data += '&action=' + this.value;
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), goToCylinders);
    }
    else return;
}

/** opvangen serverantwoord na aanpassingen */
function goToCylinders(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        if(answer[1] == true){
            if (answer[0] == "masterKeyPlanDelete") location.reload(true);
            else{
                document.getElementById('mkpInputVeil2').style.display = 'none';
                disablePanel(document.getElementById('mkpInputVeil1'));
                disablePanel(document.getElementById('mkpInputVeil3'));
                //id van mkp meegeven aan action-button en remove-button van cilinders en sleutels
                document.getElementById('inputCylAction').name = answer[2];
                document.getElementById('mkpCylRemove').name = answer[2];
                document.getElementById('inputKeyAction').name = answer[2];
                document.getElementById('mkpKeyRemove').name = answer[2];
            }
        }
        else{
            if (answer[0] == "masterKeyPlanInsert") 
                alert('FOUTMELDING\n\nSluitplan is NIET ingevoerd.\n\n(Error: ' + answer[3].toString() + ')');
            if (answer[0] == "masterKeyPlanUpdate") 
                alert('FOUTMELDING\n\nSluitplan is NIET gewijzigd.\n\n(Error: ' + answer[3].toString() + ')');
            if (answer[0] == "masterKeyPlanDelete") 
                alert('FOUTMELDING\n\nSluitplan is NIET verwijderd.\n\n(Error: ' + answer[2].toString() + ')');
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is iets misgelopen. Probeer later nog een keer.');
    }
    
}


/** SECTIE CILINDERS
 *
 * functies die worden gebruikt binnen het gedeelte cilinders, onderverdeeld in
 * navigeren en aanpassingen
*/


/** Navigeren cilinders
 */

 /** errors verwijderen */
 function removeErrorLabelsCyl(){
    document.getElementById('cyl-error-index').innerHTML = '';
    document.getElementById('cyl-error-name').innerHTML = '';
    document.getElementById('cyl-error-initialQuantity').innerHTML = '';
    document.getElementById('cyl-error-backorder').innerHTML = '';
    document.getElementById('cyl-error-activeBase').innerHTML = '';
    document.getElementById('cyl-error-activeAdd1').innerHTML = '';
    document.getElementById('cyl-error-activeAdd2').innerHTML = '';
    document.getElementById('cyl-error-activeAdd3').innerHTML = '';
    document.getElementById('cyl-error-passive').innerHTML = '';
    document.getElementById('cyl-error-lateral').innerHTML = '';
    document.getElementById('cyl-input-index').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-name').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-initialQuantity').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-backorder').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-activeBase').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-activeAdd1').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-activeAdd2').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-activeAdd3').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-passive').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-lateral').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('cyl-input-index').removeAttribute('placeholder');
    document.getElementById('cyl-input-name').removeAttribute('placeholder');
    document.getElementById('cyl-input-initialQuantity').removeAttribute('placeholder');
    document.getElementById('cyl-input-activeBase').removeAttribute('placeholder');
    document.getElementById('cyl-input-passive').removeAttribute('placeholder');
    document.getElementById('cyl-input-lateral').removeAttribute('placeholder');
 }

 /** openen invoerscherm cilinder */
function showCylinderInput(){
    if(this.value == 'cylinderUpdate'){
        //eerst checken of er een selectie gemaakt is
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        //cilinder opvragen uit db
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        data += '&action=cylinderSelectById';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInCylinder);
    }
    else{
        //velden leegmaken
        document.getElementById('cyl-input-index').value = '';
        document.getElementById('cyl-input-name').value = '';
        document.getElementById('cyl-input-initialQuantity').value = '';
        document.getElementById('cyl-input-backorder').value = '';
        document.getElementById('cyl-input-activeBase').value = '';
        document.getElementById('cyl-input-activeAdd1').value = '';
        document.getElementById('cyl-input-activeAdd2').value = '';
        document.getElementById('cyl-input-activeAdd3').value = '';
        document.getElementById('cyl-input-passive').value = '';
        document.getElementById('cyl-input-lateral').value = '';
        document.getElementById('cyl-input-size').selectedIndex = 0;
    }
    location.href = "#seventh-floor";
    //waarden die eventueel na raadplegen zijn gewijzigd terug goedzetten
    document.getElementById('backLinkCylinders').href = '#sixth-floor';
    document.getElementById('inputCylCancel').value = '';
    document.getElementById('inputCylAction').style.display = 'inline-block';
    //errorlabels leegmaken
    removeErrorLabelsCyl();
    //value insert of update aan de actionknop meegeven
    document.getElementById('inputCylAction').value = this.value;
}

/** velden invoerscherm cilinder invullen bij update */
function fillInCylinder(serverAnswer){
    if(serverAnswer != false){
        var cylinder = JSON.parse(serverAnswer);
        document.getElementById('cyl-input-index').value = cylinder['Index'];
        document.getElementById('cyl-input-name').value = cylinder['Name'];
        document.getElementById('cyl-input-initialQuantity').value = cylinder['InitialQuantity'];
        document.getElementById('cyl-input-backorder').value = cylinder['Backorder'];
        document.getElementById('cyl-input-activeBase').value = cylinder['Active'];
        if(cylinder['Add1'] == -1) document.getElementById('cyl-input-activeAdd1').value = '';
        else document.getElementById('cyl-input-activeAdd1').value = cylinder['Add1'];
        if(cylinder['Add2'] == -1) document.getElementById('cyl-input-activeAdd2').value = '';
        else document.getElementById('cyl-input-activeAdd2').value = cylinder['Add2'];
        if(cylinder['Add3'] == -1) document.getElementById('cyl-input-activeAdd3').value = '';
        else document.getElementById('cyl-input-activeAdd3').value = cylinder['Add3'];
        document.getElementById('cyl-input-passive').value = cylinder['Passive'];
        document.getElementById('cyl-input-lateral').value = cylinder['Lateral'];
        var select = document.getElementById('cyl-input-size');
        for (var i = 0 ; i < select.options.length ; i++){
            if (select.options[i].value == cylinder['IdCylinderSize']) select.selectedIndex = i;
        }
    }
}

/** afhandelen terugknop invoerscherm cilinder */
function cylGoToPrevious(e){
    //waneer men raadpleegt terug naar detailscherm MKP
    if(e.currentTarget.value == 'consult'){
        location.href = "#fifth-floor";
        document.getElementById('inputCylCancel').value = '';
        document.getElementById('inputCylAction').style.display = 'inline-block';
    }
    //wanneer men aanpast terug naar invoer MKP
    else{
        location.href = "#sixth-floor";
        //veils opvangen
        disablePanel(document.getElementById('mkpInputVeil1'));
        disablePanel(document.getElementById('mkpInputVeil3'));
        document.getElementById('mkpInputVeil2').style.display = 'none';
    }
}

/** doorgaan naar sleutelgedeelte */
function goToKeys(){
    //validatie of aantal cilinders gelijk is aan aantal opgegeven in basiscode
    var ul = document.getElementById('listCyl');
    var ul_length = ul.getElementsByTagName('LI').length;
    var number_basics = document.getElementById('mkp-input-numberOfCyls').value;
    if(ul_length != number_basics){
        alert('FOUTMELDING\n\nHet aantal toegevoegde cilinders komt niet overeen met het aantal meegedeeld in de basisgegevens.');
    }
    else{
        document.getElementById('mkpInputVeil3').style.display = 'none';
        disablePanel(document.getElementById('mkpInputVeil1'));
        disablePanel(document.getElementById('mkpInputVeil2'));
        //selectie verwijderen uit ul cilinders
        var all_list_items = document.getElementById('listCyl').childNodes;
        for (i = 0; i < all_list_items.length; i++){
            all_list_items[i].className = "none";
        }
    }
}


 /** Aanpassingen cilinders
 */

 /** invoegen of wijzigen cilinder */
function cylAction(){
    //gegevens uit textvelden halen
    var index = document.getElementById('cyl-input-index').value;
    var name = document.getElementById('cyl-input-name').value;
    var initialQuantity = document.getElementById('cyl-input-initialQuantity').value;
    var backorder = document.getElementById('cyl-input-backorder').value;
    var activeBase = document.getElementById('cyl-input-activeBase').value;
    var activeAdd1 = document.getElementById('cyl-input-activeAdd1').value;
    var activeAdd2 = document.getElementById('cyl-input-activeAdd2').value;
    var activeAdd3 = document.getElementById('cyl-input-activeAdd3').value;
    var passive = document.getElementById('cyl-input-passive').value;
    var lateral = document.getElementById('cyl-input-lateral').value;
    //cilindersize-id uit select halen
    var select = document.getElementById('cyl-input-size');
    var indexSelect = select.selectedIndex;
    var cylinderSizeId = select.options[indexSelect].value;
    //errorlabels leegmaken
    removeErrorLabelsCyl();
    
    //VALIDATIE
     var isValid = true;
    //scripttags verwijderen
    index = stripTags(index);
    name = stripTags(name);
    initialQuantity = stripTags(initialQuantity);
    backorder = stripTags(backorder);
    activeBase = stripTags(activeBase);
    activeAdd1 = stripTags(activeAdd1);
    activeAdd2 = stripTags(activeAdd2);
    activeAdd3 = stripTags(activeAdd3);
    passive = stripTags(passive);
    lateral = stripTags(lateral);
    //index: verplicht veld + integer check
    if (index.length == 0){
        setErrorRequiredField('cyl-error-index', 'cyl-input-index');
        isValid = false;
    }
    else if(!isInteger(index)){
        setErrorIntegerFail('cyl-error-index', 'cyl-input-index');
        isValid = false;
    }
    //name: verplicht veld
    if (name.length == 0){
        setErrorRequiredField('cyl-error-name', 'cyl-input-name');
        isValid = false;
    } 
    //initialQuantity: verplicht veld + integer check
    if (initialQuantity.length == 0){
        setErrorRequiredField('cyl-error-initialQuantity', 'cyl-input-initialQuantity');
        isValid = false;
    }
    else if(!isInteger(initialQuantity)){
        setErrorIntegerFail('cyl-error-initialQuantity', 'cyl-input-initialQuantity');
        isValid = false;
    } 
    //backorder: integer check 
    if(backorder.length > 0){
        if(!isInteger(backorder)){
            setErrorIntegerFail('cyl-error-backorder', 'cyl-input-backorder');
            isValid = false;
        }
    } 
    //activeBase: verplicht veld + integer check
    if (activeBase.length == 0){
        setErrorRequiredField('cyl-error-activeBase', 'cyl-input-activeBase');
        isValid = false;
    } 
    else if(!isInteger(activeBase)){
        setErrorIntegerFail('cyl-error-activeBase', 'cyl-input-activeBase');
        isValid = false;
    }
    //activeAdd1: integer check
    if(activeAdd1.length > 0){
        if(!isInteger(activeAdd1)){
            setErrorIntegerFail('cyl-error-activeAdd1', 'cyl-input-activeAdd1');
            isValid = false;
        }
    } 
    //activeAdd2: integer check
    if(activeAdd2.length > 0){
        if(!isInteger(activeAdd2)){
            setErrorIntegerFail('cyl-error-activeAdd2', 'cyl-input-activeAdd2');
            isValid = false;
        }
    } 
    //activeAdd3: integer check
    if(activeAdd3.length > 0){
        if(!isInteger(activeAdd3)){
            setErrorIntegerFail('cyl-error-activeAdd3', 'cyl-input-activeAdd3');
            isValid = false;
        }
    } 
    //passive: verplicht veld + integer check
    if (passive.length == 0){
        setErrorRequiredField('cyl-error-passive', 'cyl-input-passive');
        isValid = false;
    } 
    else if(!isInteger(passive)){
        setErrorIntegerFail('cyl-error-passive', 'cyl-input-passive');
        isValid = false;
    } 
    //lateral : verplicht veld + integer check
    if (lateral.length == 0){
        setErrorRequiredField('cyl-error-lateral', 'cyl-input-lateral');
        isValid = false;
    } 
    else if(!isInteger(lateral)){
        setErrorIntegerFail('cyl-error-lateral', 'cyl-input-lateral');
        isValid = false;
    } 
    //als de validatie niet slaagt terugkeren naar de UI
    if (!isValid) return;

    //ajax
    var data = '';
    if (index.length > 0) {
        data += '&index=' + index;
    }
    if (name.length > 0) {
        data += '&name=' + name;
    }
    if (cylinderSizeId.length > 0) {
        data += '&cylinderSizeId=' + cylinderSizeId;
    }
    if (initialQuantity.length > 0) {
        data += '&initialQuantity=' + initialQuantity;
    }
    if (backorder.length > 0) {
        data += '&backorder=' + backorder;
    }
    if (activeBase.length > 0) {
        data += '&active=' + activeBase;
    }
    if (activeAdd1.length > 0) {
        data += '&add1=' + activeAdd1;
    }
    if (activeAdd2.length > 0) {
        data += '&add2=' + activeAdd2;
    }
    if (activeAdd3.length > 0) {
        data += '&add3=' + activeAdd3;
    }
    if (passive.length > 0) {
        data += '&passive=' + passive;
    }
    if (lateral.length > 0) {
        data += '&lateral=' + lateral;
    }
    if(this.value == 'cylinderUpdate'){
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        var id = item_selected.value;
        if (id > 0){
            data += '&id=';
            data += id;
        }
    }
    data += '&mkpId=' + this.name;
    data += '&action=' + this.value;
    var my_ajax = new Ajax();
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), adjustCylinderList);
}

/** verwijderen cilinder: eerst controleren op gekopelde sleutels */
function checkUseCylinder(){
    var item_selected = checkItemSelection(this.value);
    if (item_selected == -1) return;
    var confirmed = confirm('CILINDER VERWIJDEREN\n\nBent u zeker dat u deze cilinder wenst te verwijderen');
    if (confirmed == true) {
        var my_ajax = new Ajax();
        var data = '&idCylinder=' + item_selected.value;
        data += '&action=cylinderKeySelectByIdCylinder';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), deleteCylinder);
    }
    else return;
}

/** verwijderen van een cilinder */
function deleteCylinder(serverAnswer){
    if (serverAnswer != false){
        alert('OPMERKING\n\nDe cilinder kan NIET verwijderd worden.\n' + 
            'Er zijn nog sleutels aan gekoppeld.');
    }
    else {
        var button = document.getElementById('mkpCylRemove');
        var item_selected = checkItemSelection(button.value);
        if (item_selected == -1) return;
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        data += '&mkpId=' + button.name;
        data += '&action=' + button.value;
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), adjustCylinderList);
    }
}

/** opvangen serverantwoord na aanpassingen */
function adjustCylinderList(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        if(answer[1] == true){
            location.href = '#sixth-floor';
            //cilinders opvragen voor ul
            var dataCyls = '';
            dataCyls += '&idMkp=';
            dataCyls += answer[2];
            dataCyls += '&action=cylinderSelectByIdMasterKeyPlan';
            var my_ajax = new Ajax();
            my_ajax.postRequest('appcode/control/ajax-controller.php', dataCyls.substring(1), fillListCylinders);
        }
        else{
            if (answer[0] == "cylinderInsert") 
                alert('FOUTMELDING\n\nCilinder is NIET ingevoerd.\n\n(Error: ' + answer[3].toString() + ')');
            if (answer[0] == "cylinderUpdate") 
                alert('FOUTMELDING\n\nCilinder is NIET gewijzigd.\n\n(Error: ' + answer[3].toString() + ')');
            if (answer[0] == "cylinderDelete") 
                alert('FOUTMELDING\n\nCilinder is NIET verwijderd.\n\n(Error: ' + answer[3].toString() + ')');
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is iets misgelopen. Probeer later nog eens.');
    }
    
}


/** SECTIE SLEUTELS
 *
 * functies die worden gebruikt binnen het gedeelte sleutels, onderverdeeld in
 * navigeren en aanpassingen
*/


/** Navigeren sleutels
 */

 /** errors verwijderen */
 function removeErrorLabelsKey(){
    document.getElementById('key-error-index').innerHTML = '';
    document.getElementById('key-error-name').innerHTML = '';
    document.getElementById('key-error-initialQuantity').innerHTML = '';
    document.getElementById('key-error-backorder').innerHTML = '';
    document.getElementById('key-error-active').innerHTML = '';
    document.getElementById('key-error-passive').innerHTML = '';
    document.getElementById('key-error-lateral').innerHTML = '';
    document.getElementById('key-error-cyls').innerHTML = '';
    document.getElementById('key-input-index').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-name').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-initialQuantity').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-backorder').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-active').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-passive').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-lateral').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-cyls').style.borderColor = 'rgba(255,255,255,0.5)';
    document.getElementById('key-input-index').removeAttribute('placeholder');
    document.getElementById('key-input-name').removeAttribute('placeholder');
    document.getElementById('key-input-initialQuantity').removeAttribute('placeholder');
    document.getElementById('key-input-active').removeAttribute('placeholder');
    document.getElementById('key-input-passive').removeAttribute('placeholder');
    document.getElementById('key-input-lateral').removeAttribute('placeholder');
 }

 /** openen invoerscherm sleutel */
function showKeyInput(){
    if(this.value == 'keyUpdate'){
        //eerst checken of er een selectie gemaakt is
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        //sleutel opvragen uit db
        var my_ajax = new Ajax();
        var data = '&id=' + item_selected.value;
        data += '&action=keySelectById';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInKey);
    }
    else{
        //velden leegmaken
        document.getElementById('key-input-index').value = '';
        document.getElementById('key-input-name').value = '';
        document.getElementById('key-input-initialQuantity').value = '';
        document.getElementById('key-input-backorder').value = '';
        document.getElementById('key-input-active').value = '';
        document.getElementById('key-input-passive').value = '';
        document.getElementById('key-input-lateral').value = '';
        document.getElementById('key-input-cyls').selectedIndex = 0;
        var ul = document.getElementById('listCylsOnKey');
        if (ul) {
            while (ul.firstChild) {
                ul.removeChild(ul.firstChild);
            }
        }
    }
    location.href = "#eighth-floor";
    //waarden die eventueel na raadplegen zijn gewijzigd terug goedzetten
    document.getElementById('backLinkKeys').href = '#sixth-floor';
    document.getElementById('inputKeyCancel').value = '';
    document.getElementById('inputKeyAction').style.display = 'inline-block';
    //errorlabels leegmaken
    removeErrorLabelsKey();
    //select vullen adhv ul cyls maar eerst leegmaken
    var select = document.getElementById('key-input-cyls');
    select.innerHTML = '';
    var listCyls = document.getElementById('listCyl');
    for (var i = 0; i < listCyls.childNodes.length; i++){
        var option = document.createElement("option");
        option.text = listCyls.childNodes[i].innerHTML;
        option.value = listCyls.childNodes[i].value;
        select.options.add(option, i);
    }
    select.selectedIndex = -1;
    //value insert of update aan de actionknop meegeven
    document.getElementById('inputKeyAction').value = this.value;
}

/** velden invoerscherm sleutel invullen bij update */
function fillInKey(serverAnswer){
    if (serverAnswer != false) {
        var key = JSON.parse(serverAnswer);
        document.getElementById('key-input-index').value = key['Index'];
        document.getElementById('key-input-name').value = key['Name'];
        document.getElementById('key-input-initialQuantity').value = key['InitialQuantity'];
        document.getElementById('key-input-backorder').value = key['Backorder'];
        document.getElementById('key-input-active').value = key['Active'];
        document.getElementById('key-input-passive').value = key['Passive'];
        document.getElementById('key-input-lateral').value = key['Lateral'];
        //ul listCylsOnKey vullen met gekoppelde cilinders
        var my_ajax = new Ajax();
        var data = '&idKey=' + key['Id'];
        data += '&action=cylinderKeySelectByIdKey';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), fillInListCylsOnKey);
    }
}

/** lijst vullen met gekoppelde cilinders */
function fillInListCylsOnKey(serverAnswer){
    if(serverAnswer != false){
        fillUlElement(serverAnswer, 'listCylsOnKey', 'CylinderName', 'IdCylinder');
    }
    else{
        //lijst leegmaken
        var ul = document.getElementById('listCylsOnKey');
        if (ul) {
            while (ul.firstChild) {
                ul.removeChild(ul.firstChild);
            }
        }
    }
}

/** cilinder toevoegen aan de lijst met gekoppelde cilinders */
function selectCylOnKey(e){
    var ul = document.getElementById('listCylsOnKey');
    var select = document.getElementById('key-input-cyls');
    var index = select.selectedIndex;
    //eerst testen of de cilinder reeds in de ul staat
    var alreadySelected = false;
    for (var i = 0; i < ul.getElementsByTagName('LI').length; i++ ){
        if (ul.childNodes[i].value == select.options[index].value) alreadySelected = true;
    }
    if(!alreadySelected){
        var li = document.createElement("LI");
        var textnode = document.createTextNode(select.options[index].text);
        li.appendChild(textnode);
        //om attributen aan het li element toe te voegen zoals value
        li.setAttribute("value", select.options[index].value);
        ul.appendChild(li);
    }
    
}

/** cilinder verwijderen uit de lijst met gekoppelde cilinders */
function deleteCylOnKey(){
    //checken selectie
    var item_selected = checkItemSelection('cylOnKeyDelete');
    if (item_selected == -1) return;
    //li uit ul halen
    var ul = document.getElementById('listCylsOnKey');
    ul.removeChild(item_selected);
}

/** afhandelen terugknop invoerscherm sleutel */
function keyGoToPrevious(e){
    if(e.currentTarget.value == 'consult'){
        location.href = "#fifth-floor";
        document.getElementById('inputKeyCancel').value = '';
        document.getElementById('inputKeyAction').style.display = 'inline-block';
    }
    else{
        location.href = "#sixth-floor";
        //veils opvangen
        disablePanel(document.getElementById('mkpInputVeil1'));
        disablePanel(document.getElementById('mkpInputVeil2'));
        document.getElementById('mkpInputVeil3').style.display = 'none';
    }
}

/** het afronden van het aanpassen van het volledige sluitplan */
function mkpFinish(){
    //validatie of aantal sleutels gelijk is aan aantal opgegeven in basiscode
    var ul = document.getElementById('listKey');
    var ul_length = ul.getElementsByTagName('LI').length;
    var number_basics = document.getElementById('mkp-input-numberOfKeys').value;
    if(ul_length != number_basics){
        alert('FOUTMELDING\n\nHet aantal toegevoegde sleutels komt niet overeen met het aantal meegedeeld in de basisgegevens.');
    }
    else{
        //selectie verwijderen uit ul sleutels
        var all_list_items = document.getElementById('listKey').childNodes;
            for (i = 0; i < all_list_items.length; i++){
                all_list_items[i].className = "none";
            }
        location.href = '#fourth-floor';
        searchMKPs();
    }
}


 /** Aanpassingen sleutels
 */

/** invoegen of wijzigen sleutel */
function keyAction(){
    //errorlabels leegmaken
    removeErrorLabelsKey()
    //gegevens uit textvelden halen
    var index = document.getElementById('key-input-index').value;
    var name = document.getElementById('key-input-name').value;
    var initialQuantity = document.getElementById('key-input-initialQuantity').value;
    var backorder = document.getElementById('key-input-backorder').value;
    var active = document.getElementById('key-input-active').value;
    var passive = document.getElementById('key-input-passive').value;
    var lateral = document.getElementById('key-input-lateral').value;
    
    //VALIDATIE
    var isValid = true;
    //scripttags verwijderen
    index = stripTags(index);
    name = stripTags(name);
    initialQuantity = stripTags(initialQuantity);
    backorder = stripTags(backorder);
    active = stripTags(active);
    passive = stripTags(passive);
    lateral = stripTags(lateral);
    //verplichte velden en integer checks
    if (index.length == 0){
        setErrorRequiredField('key-error-index', 'key-input-index');
        isValid = false;
    }
    else if(!isInteger(index)){
        setErrorIntegerFail('key-error-index', 'key-input-index');
        isValid = false;
    }
    if (name.length == 0){
        setErrorRequiredField('key-error-name', 'key-input-name');
        isValid = false;
    } 
    if (initialQuantity.length == 0){
        setErrorRequiredField('key-error-initialQuantity', 'key-input-initialQuantity');
        isValid = false;
    }
    else if(!isInteger(initialQuantity)){
        setErrorIntegerFail('key-error-initialQuantity', 'key-input-initialQuantity');
        isValid = false;
    } 
    if(backorder.length > 0){
        if(!isInteger(backorder)){
        setErrorIntegerFail('key-error-backorder', 'key-input-backorder');
        isValid = false;
        }
    } 
    if (active.length == 0){
        setErrorRequiredField('key-error-active', 'key-input-active');
        isValid = false;
    } 
    else if(!isInteger(active)){
        setErrorIntegerFail('key-error-active', 'key-input-active');
        isValid = false;
    }
    if (passive.length == 0){
        setErrorRequiredField('key-error-passive', 'key-input-passive');
        isValid = false;
    } 
    else if(!isInteger(passive)){
        setErrorIntegerFail('key-error-passive', 'key-input-passive');
        isValid = false;
    } 
    if (lateral.length == 0){
        setErrorRequiredField('key-error-lateral', 'key-input-lateral');
        isValid = false;
    } 
    else if(!isInteger(lateral)){
        setErrorIntegerFail('key-error-lateral', 'key-input-lateral');
        isValid = false;
    } 
    //controle dat er minstens 1 cylinder gekozen is
    var ul = document.getElementById('listCylsOnKey');
    var li_items = ul.getElementsByTagName('LI');
    if(li_items.length <= 0){
        setErrorX('key-error-cyls', 'key-input-cyls', 'Selecteer 1 of meerdere cilinders.');
        isValid = false;
    }
    //als de validatie niet slaagt terugkeren naar de UI
    if (!isValid) return;
    //cylinders opvragen uit ul
    var cyls = [];
    if (li_items.length > 0){
        for (var i = 0; i < li_items.length; i++){
            cyls.push(li_items[i].value);
        }
    }
    //ajax
    var data = '';
    if (index.length > 0) {
        data += '&index=' + index;
    }
    if (name.length > 0) {
        data += '&name=' + name;
    }
    if (initialQuantity.length > 0) {
        data += '&initialQuantity=' + initialQuantity;
    }
    if (backorder.length > 0) {
        data += '&backorder=' + backorder;
    }
    if (active.length > 0) {
        data += '&active=' + active;
    }
    if (passive.length > 0) {
        data += '&passive=' + passive;
    }
    if (lateral.length > 0) {
        data += '&lateral=' + lateral;
    }
    if (cyls.length > 0){
        var cylinders = JSON.stringify(cyls);
    }
    if(this.value == 'keyUpdate'){
        var item_selected = checkItemSelection(this.value);
        if (item_selected == -1) return;
        var id = item_selected.value;
        if (id > 0){
            data += '&id=';
            data += id;
        }
    }
    data += '&mkpId=' + this.name;
    data += '&action=' + this.value;
    data += '&cylinders=' + cylinders;
    var my_ajax = new Ajax();
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), adjustKeyList);
}

/** verwijderen sleutel */
function deleteKey(){
    var item_selected = checkItemSelection(this.value);
    if (item_selected == -1) return;
    var confirmed = confirm('SLEUTEL VERWIJDEREN\n\nBent u zeker dat u deze sleutel wenst te verwijderen?');
    if (confirmed == false) return;
    var my_ajax = new Ajax();
    var data = '&id=' + item_selected.value;
    data += '&mkpId=' + this.name;
    data += '&action=' + this.value;
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), adjustKeyList);
}

/** opvangen serverantwoord na aanpassingen */
function adjustKeyList(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        if(answer[1] == true){
            location.href = '#sixth-floor';
            if(answer[3]== false){
                alert('FOUTMELDING\n\nEr is iets fout gelopen waardoor de cilinderkoppelingen niet goed zijn verlopen.' +
                'Probeer nogmaals via het wijzigen van de sleutel.')
            }
            //sleutels opvragen voor ul
            var dataKeys = '';
            dataKeys += '&idMkp=';
            dataKeys += answer[2];
            dataKeys += '&action=keySelectByIdMasterKeyPlan';
            var my_ajax = new Ajax();
            my_ajax.postRequest('appcode/control/ajax-controller.php', dataKeys.substring(1), fillListKeys);
        
        }
        else{
            if (answer[0] == "keyInsert") 
                alert('FOUTMELDING\n\nSleutel is NIET ingevoerd.\n\n(Error: ' + answer[4].toString() + ')');
            if (answer[0] == "keyUpdate") 
                alert('FOUTMELDING\n\nSleutel is NIET gewijzigd.\n\n(Error: ' + answer[4].toString() + ')');
            if (answer[0] == "keyDelete") 
                alert('FOUTMELDING\n\nSleutel is NIET verwijderd.\n\n(Error: ' + answer[3].toString() + ')');
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is iets misgelopen. Probeer later nog eens.');
    }
    
}


/** SECTIE MEMBERSHIP
 *
 * functies die worden gebruikt binnen het gedeelte membership
 * 
*/

/** ledigen velden login */
function emptyLoginFields(){
    document.getElementById('username').value = '';
    document.getElementById('password').value = '';
    document.getElementById('loginFeedback').innerHTML = '';
    emptyErrorFieldsLogin();
}

/** errors login verwijderen */
function emptyErrorFieldsLogin(){
    document.getElementById('username-error').innerHTML = '';
    document.getElementById('password-error').innerHTML = '';
    document.getElementById('username').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('password').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('username').style.backgroundColor = 'white';
    document.getElementById('password').style.backgroundColor = 'white';
}

/** ledigen velden registreren */
function emptyRegisterFields() {
    document.getElementById('usernameregister').value = '';
    document.getElementById('emailregister').value = '';
    document.getElementById('passwordregister').value = '';
    document.getElementById('passwordregister-confirm').value = '';
    document.getElementById('roleregister').selectedIndex = 0;
    emptyErrorFieldsRegister();
}

/** errors registreren verwijderen */
function emptyErrorFieldsRegister(){
    document.getElementById('usernameregister-error').innerHTML = '';
    document.getElementById('emailregister-error').innerHTML = '';
    document.getElementById('passwordregister-error').innerHTML = '';
    document.getElementById('passwordregister-confirm-error').innerHTML = '';
    document.getElementById('roleregister-error').innerHTML = '';
    document.getElementById('usernameregister').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('emailregister').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('passwordregister').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('passwordregister-confirm').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('roleregister').style.border = '1px solid rgba(0,0,0,0.5)';
    document.getElementById('usernameregister').style.backgroundColor = 'white';
    document.getElementById('emailregister').style.backgroundColor = 'white';
    document.getElementById('passwordregister').style.backgroundColor = 'white';
    document.getElementById('passwordregister-confirm').style.backgroundColor = 'white';
    document.getElementById('roleregister').style.backgroundColor = 'white';
}

/** openen popup van de gebruikers */
function openUsersPopup(){
    var hash = location.hash;
    document.getElementById('usersClose').setAttribute('href', hash);
    document.getElementById('registerClose').setAttribute('href', hash);
    var my_ajax = new Ajax();
    var data = '&action=getAllUsers';
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), showUsers);
}

/** tonen van gebruikers in popup */
function showUsers(serverAnswer){
    fillUlElement(serverAnswer, 'listUsers', 'UserName', 'Id');
}

/** openen popup voor registreren */
function openRegisterPopup(){
    location.href = '#to-register';
}

/** inloggen van gebruiker */
function login() {
    //errorlabels leegmaken, rode kleur verwijderen
    emptyErrorFieldsLogin();
    //waarden van velden opvragen
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    //VALIDATIE
    //tags verwijderen
    username = stripTags(username);
    password = stripTags(password);
    //validatie op verplichte velden
    if (username.length == 0 || password.length == 0) {
        if(username.length == 0 && password.length == 0){
            document.getElementById('loginFeedback').innerHTML = 'Gebruikersnaam en wachtwoord zijn verplicht';
            setErrorMembership('username-error', 'username', 'Gebruikersnaam is een verplicht veld');
            setErrorMembership('password-error', 'password', 'Wachtwoord is een verplicht veld');
        }
        else if(username.length == 0){
            document.getElementById('loginFeedback').innerHTML = 'Gebruikersnaam is verplicht';
            setErrorMembership('username-error', 'username', 'Gebruikersnaam is een verplicht veld');
        }
        else if (password.length == 0){
            document.getElementById('loginFeedback').innerHTML = 'Wachtwoord is verplicht';
            setErrorMembership('password-error', 'password', 'Wachtwoord is een verplicht veld');
        }
        return;
    }
    //ajax
    var my_ajax = new Ajax();
    if (username.length > 0) {
        var data = '&username=';
        data += username;
    }
    else {
        data = '';
    }
    if (password.length > 0) {
        data += '&password=';
        data += password;
    }
    data += '&action=' + this.value;
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), displayLogin);
}

/** opvangen serverantwoord na inloggen */
function displayLogin(serverAnswer){
    if(serverAnswer == 1){
        emptyLoginFields();
        sessionStorage.setItem("loaded", "#first-floor");
        location.reload(true);
    }
    else if(serverAnswer == -1){
        document.getElementById('loginFeedback').innerHTML =
        'Teveel mislukte pogingen!<br\>U moet 2 uur wachten alvorens u terug kan inloggen.';
    }
    else{
        document.getElementById('loginFeedback').innerHTML =
        'Gebruikersnaam en/of wachtwoord zijn niet gekend.';
    }
}

/** registreren van nieuwe gebruiker */
function register() {
    //errorlabels leegmaken, rode kleur verwijderen
    emptyErrorFieldsRegister();
    //waarden van velden opvragen
    var usernameregister = document.getElementById('usernameregister').value;
    var emailregister = document.getElementById('emailregister').value;
    var passwordregister = document.getElementById('passwordregister').value;
    var passwordregisterconfirm = document.getElementById('passwordregister-confirm').value;
    var select = document.getElementById('roleregister');
    var indexSelect = select.selectedIndex;
    var roleId = select.options[indexSelect].value;
    //tags verwijderen
    usernameregister = stripTags(usernameregister);
    emailregister = stripTags(emailregister);
    passwordregister = stripTags(passwordregister);
    passwordregisterconfirm = stripTags(passwordregisterconfirm);
    //VALIDATIE
    var isValid = true;
    if(usernameregister.length == 0) {
        setErrorMembership('usernameregister-error', 'usernameregister', 'Gebruikersnaam is een verplicht veld');
        isValid = false;
    }
    if(emailregister.length == 0) {
        setErrorMembership('emailregister-error', 'emailregister', 'Email is een verplicht veld');
        isValid = false;
    }
    else if(!isEmail(emailregister)){
        setErrorMembership('emailregister-error', 'emailregister', 'Ongeldig email adres');
        isValid = false;
    }
    if(passwordregister.length < 6) {
        setErrorMembership('passwordregister-error', 'passwordregister', 'Kies een wachtwoord met minstens 6 karakters');
        isValid = false;
    }
    else if (passwordregister != passwordregisterconfirm){
        setErrorMembership('passwordregister-confirm-error', 'passwordregister-confirm', 'Wachtwoorden komen niet overeen');
        isValid = false;
    }
    if(roleId == 'none'){
        setErrorMembership('roleregister-error', 'roleregister', 'Kies een accounttype');
        isValid = false;
    }
    if (!isValid) return;
    //ajax
    var my_ajax = new Ajax();
    if (usernameregister.length > 0) {
        var data = '&usernameregister=';
        data += usernameregister;
    }
    else {
        var data = '';
    }
    if (emailregister.length > 0) {
        data += '&emailregister=';
        data += emailregister;
    }
    if (passwordregister.length > 5) {
        data += '&passwordregister=';
        data += passwordregister;
    }
    if (roleId != 'none'){
        data += '&roleId=';
        data += roleId;
    }
    data += '&action=' + this.value;
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), displayRegister);
}

/** opvangen serverantwoord na registreren */
function displayRegister(serverAnswer){
    try{
        var answer = JSON.parse(serverAnswer);
        if(answer[0] == true){
            emptyRegisterFields();
            if(answer[1] == true){
                alert('Gebruiker werd succesvol geregistreerd en kan vanaf nu inloggen met zijn of haar gegevens.')
            }
            else{
                alert('FOUTMELDING\n\nGebruiker werd geregistreerd maar het accounttype werd niet toegekend.');
            }
        }
        else{
            alert('FOUTMELDING\n\nGebruiker is niet geregistreerd. Probeer later nog een keer.');
        }
    }
    catch(e){
        alert('FOUTMELDING\n\nEr is een onverwachte fout opgetreden bij het registreren. Gelieve later nog eens te proberen.');
    }
}

/** verwijderen van gebruiker */
function deleteUser(){
    var item_selected = checkItemSelection(this.value);
    if (item_selected == -1) return;
    var confirmed = confirm('GEBRUIKER VERWIJDEREN\n\nBent u zeker dat u deze gebruiker wenst te verwijderen?');
    if (confirmed == false) return;
    var my_ajax = new Ajax();
    var data = '&memberId=' + item_selected.value;
    data += '&action=' + this.value;
    my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), refreshAfterUserDelete);
}

/** opvangen serverantwoord na verwijderen gebruiker */
function refreshAfterUserDelete(serverAnswer){
    if(serverAnswer == true){
        var my_ajax = new Ajax();
        var data = '&action=getAllUsers';
        my_ajax.postRequest('appcode/control/ajax-controller.php', data.substring(1), showUsers);
    }
    else alert('FOUTMELDING\n\nDe gebruiker kon NIET verwijderd worden. Probeer later nog eens.')
}

/** afmelden */
function loginLogout() {
    var link = document.getElementById('loginLink').innerHTML;
    if (link == 'afmelden') {
        var my_ajax = new Ajax();
        my_ajax.postRequest('appcode/control/ajax-controller.php', 'action=logout', refreshAfterLogout);
    }
}

/** refresh na het afmelden */
function refreshAfterLogout() {
    location.reload(true);
}