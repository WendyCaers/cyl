﻿--testen van stored procedures tabel KeyCode
--30/12/2014

use stshbe1q_cyl;
--insert
CALL KeyCodeInsert(@In, 10011, 001110, 510231, 'Wendy');
CALL KeyCodeInsert(@In, 01010, 110100, 432012, 'Wendy');
CALL KeyCodeInsert(@In, 11100, 101010, 351023, 'Wendy');
CALL KeyCodeInsert(@In, 00011, 010101, 210103, 'Wendy');
CALL KeyCodeInsert(@In, 10101, 000111, 135042, 'Wendy');
--update
CALL KeyCodeUpdate(1 , 10011, 111111, 510231, 'WendyC');
CALL KeyCodeUpdate(2 , 11111, 110100, 432012, 'WendyC');
--delete
CALL KeyCodeDelete(5);
--selectOne
CALL KeyCodeSelectOne(3);
--selectAll
CALL KeyCodeSelectAll();
--count
CALL KeyCodeCount();
--selectById
CALL KeyCodeSelectById(2);
--selectByCode
CALL KeyCodeSelectByCode(00011, 010101, 210103);