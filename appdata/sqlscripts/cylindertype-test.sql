﻿--testen van stored procedures tabel CylinderType
--30/12/2014

use stshbe1q_cyl;
--insert
CALL CylinderTypeInsert(@In, 'dubbelcil', 'Wendy');
CALL CylinderTypeInsert(@In, 'halfcil', 'Wendy');
CALL CylinderTypeInsert(@In, 'oplegcilinder', 'Wendy');
CALL CylinderTypeInsert(@In, 'knopcilinder', 'Wendy');
CALL CylinderTypeInsert(@In, 'brievenbusslot', 'Wendy');
--update
CALL CylinderTypeUpdate(1 , 'dubbelcilinder', 'WendyC');
CALL CylinderTypeUpdate(2 , 'halfcilinder', 'WendyC');
--delete
CALL CylinderTypeDelete(5);
--selectOne
CALL CylinderTypeSelectOne(3);
--selectAll
CALL CylinderTypeSelectAll();
--count
CALL CylinderTypeCount();
--selectById
CALL CylinderTypeSelectById(2);
--selectByName
CALL CylinderTypeSelectByName('knopcilinder');
--selectLikeName
CALL CylinderTypeSelectLikeName('opleg');
--selectLikeXName
CALL CylinderTypeSelectLikeXName('o');