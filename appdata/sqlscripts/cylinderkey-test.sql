﻿--testen van stored procedures tabel CylinderKey
--5/2/2014

use stshbe1q_cyl;
--insert
CALL CylinderKeyInsert(@In, 1, 2, 'Wendy');
CALL CylinderKeyInsert(@In, 2, 3, 'Wendy');
CALL CylinderKeyInsert(@In, 3, 4, 'Wendy');
CALL CylinderKeyInsert(@In, 4, 1, 'Wendy');
CALL CylinderKeyInsert(@In, 1, 4, 'Wendy');
--update
CALL CylinderKeyUpdate(1, 1, 3, 'WendyC');
CALL CylinderKeyUpdate(2, 2, 4, 'WendyC');
--delete
CALL CylinderKeyDelete(5);
--selectOne
CALL CylinderKeySelectOne(3);
--selectAll
CALL CylinderKeySelectAll();
--count
CALL CylinderKeyCount();
--selectById
CALL CylinderKeySelectById(2);
--selectByIdCylinderKeyType
CALL CylinderKeySelectByIdCylinder(1);
--selectByName
CALL CylinderKeySelectByIdKey(4);
