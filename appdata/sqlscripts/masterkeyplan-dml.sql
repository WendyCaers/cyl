﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML Insert Stored Procedure for MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanInsert`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanInsert`
(
	OUT pId INT ,
	IN pIdProfile INT ,
	IN pNumber VARCHAR (20) CHARACTER SET UTF8 ,
	IN pDate DATE ,
	IN pInvoice VARCHAR (50) CHARACTER SET UTF8 ,
	IN pDescription VARCHAR (1000) CHARACTER SET UTF8 ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `MasterKeyPlan`
	(
		`MasterKeyPlan`.`IdProfile`,
		`MasterKeyPlan`.`Number`,
		`MasterKeyPlan`.`Date`,
		`MasterKeyPlan`.`Invoice`,
		`MasterKeyPlan`.`Description`,
		`MasterKeyPlan`.`InsertedBy`,
		`MasterKeyPlan`.`InsertedOn`
	)
	VALUES
	(
		pIdProfile,
		pNumber,
		pDate,
		pInvoice,
		pDescription,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML Update Stored Procedure for MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanUpdate`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanUpdate`
(
	IN pId INT ,
	IN pIdProfile INT ,
	IN pNumber VARCHAR (20) CHARACTER SET UTF8 ,
	IN pDate DATE ,
	IN pInvoice VARCHAR (50) CHARACTER SET UTF8 ,
	IN pDescription VARCHAR (1000) CHARACTER SET UTF8 ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `MasterKeyPlan`
SET
	`IdProfile` = pIdProfile,
	`Number` = pNumber,
	`Date` = pDate,
	`Invoice` = pInvoice,
	`Description` = pDescription,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `MasterKeyPlan`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML Delete Stored Procedure for MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanDelete`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `MasterKeyPlan`
WHERE `MasterKeyPlan`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectOne Stored Procedure for MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectOne`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `MasterKeyPlan`
	WHERE `MasterKeyPlan`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectAll Stored Procedure for table MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectAll`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectAll`
(
)
BEGIN
	SELECT * FROM `MasterKeyPlan`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML Count Stored Procedure for table MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanCount`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanCount`
(
)
BEGIN
	SELECT count(*) FROM `MasterKeyPlan`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectById Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectById`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectByIdProfile Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectByIdProfile`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectByIdProfile`
(
	IN pIdProfile INT 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`IdProfile` = pIdProfile;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectByNumber Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectByNumber`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectByNumber`
(
	IN pNumber VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Number` = pNumber;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectByDate Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectByDate`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectByDate`
(
	IN pDate DATE
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Date` = pDate;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectByInvoice Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectByInvoice`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectByInvoice`
(
	IN pInvoice VARCHAR (50) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Invoice` = pInvoice;
END //
DELIMITER ;


-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectLikeNumber Stored Procedure for table MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectLikeNumber`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectLikeNumber`
(
	IN pNumber VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Number` like CONCAT(pNumber, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectLikeInvoice Stored Procedure for table MasterKeyPlan 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectLikeInvoice`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectLikeInvoice`
(
	IN pInvoice VARCHAR (50) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Invoice` like CONCAT(pInvoice, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectLikeXNumber Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectLikeXNumber`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectLikeXNumber`
(
	IN pNumber VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Number` like CONCAT('%', pNumber, '%')
;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 03:52:38 AM
-- DML SelectLikeXInvoice Stored Procedure for table MasterKeyPlan
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `MasterKeyPlanSelectLikeXInvoice`;
DELIMITER //
CREATE PROCEDURE `MasterKeyPlanSelectLikeXInvoice`
(
	IN pInvoice VARCHAR (50) CHARACTER SET UTF8 
)
BEGIN
	SELECT `MasterKeyPlan`.`Id`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, 
    `MasterKeyPlan`.`Invoice`, `MasterKeyPlan`.`Description`,
    `MasterKeyPlan`.`IdProfile`, `Profile`.`Brand`, `Profile`.`Type`
 FROM `MasterKeyPlan`
    INNER JOIN `Profile`
		ON `MasterKeyPlan`.`IdProfile` = `Profile`.`Id`
	WHERE `MasterKeyPlan`.`Invoice` like CONCAT('%', pInvoice, '%')
;
END //
DELIMITER ;
