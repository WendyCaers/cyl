﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML Insert Stored Procedure for CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeInsert`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeInsert`
(
	OUT pId INT ,
	IN pName VARCHAR (20) CHARACTER SET UTF8 ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `CylinderType`
	(
		`CylinderType`.`Name`,
		`CylinderType`.`InsertedBy`,
		`CylinderType`.`InsertedOn`
	)
	VALUES
	(
		pName,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML Update Stored Procedure for CylinderType
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeUpdate`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeUpdate`
(
	IN pId INT ,
	IN pName VARCHAR (20) CHARACTER SET UTF8 ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `CylinderType`
SET
	`Name` = pName,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `CylinderType`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML Delete Stored Procedure for CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeDelete`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `CylinderType`
WHERE `CylinderType`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectOne Stored Procedure for CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectOne`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `CylinderType`
	WHERE `CylinderType`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectAll Stored Procedure for table CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectAll`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectAll`
(
)
BEGIN
	SELECT * FROM `CylinderType`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML Count Stored Procedure for table CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeCount`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeCount`
(
)
BEGIN
	SELECT count(*) FROM `CylinderType`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectById Stored Procedure for table CylinderType
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectById`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `CylinderType`.`Id`, `CylinderType`.`Name`
 FROM `CylinderType`

	WHERE `CylinderType`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectByName Stored Procedure for table CylinderType
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectByName`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectByName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderType`.`Id`, `CylinderType`.`Name`
 FROM `CylinderType`

	WHERE `CylinderType`.`Name` = pName;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectLikeName Stored Procedure for table CylinderType 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectLikeName`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectLikeName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderType`.`Id`, `CylinderType`.`Name`
 FROM `CylinderType`

	WHERE `CylinderType`.`Name` like CONCAT(pName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:37:19 AM
-- DML SelectLikeXName Stored Procedure for table CylinderType
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderTypeSelectLikeXName`;
DELIMITER //
CREATE PROCEDURE `CylinderTypeSelectLikeXName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderType`.`Id`, `CylinderType`.`Name`
 FROM `CylinderType`

	WHERE `CylinderType`.`Name` like CONCAT('%', pName, '%')
;
END //
DELIMITER ;
