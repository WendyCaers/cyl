﻿--testen van stored procedures tabel MasterKeyPlan
--30/12/2014

use stshbe1q_cyl;
--insert
--date format = YYYY-MM-DD
CALL MasterKeyPlanInsert(@In , 1, 'SH9999001', '2014-01-01', 'F/9999001', 'test', 'Wendy');
CALL MasterKeyPlanInsert(@In , 3, 'SH9999002', '2014-01-02', 'F/9999002', 'test2', 'Wendy');
CALL MasterKeyPlanInsert(@In , 2, 'SH9999003', '2014-02-10', 'F/9999101', 'test3', 'Wendy');
CALL MasterKeyPlanInsert(@In , 1, 'SH9999004', '2014-06-15', 'F/9999222', 'test4', 'Wendy');
CALL MasterKeyPlanInsert(@In , 1, 'SH9999005', '2014-12-30', 'F/9999450', 'test5', 'Wendy'); 
--update
CALL MasterKeyPlanUpdate(1 , 1, 'SH9999001', '2014-01-20', 'F/9999025', 'test1', 'WendyC');
CALL MasterKeyPlanUpdate(2 , 3, 'SH9999002', '2014-02-05', 'F/9999050', 'test2', 'WendyC');
--delete
CALL MasterKeyPlanDelete(5);
--selectOne
CALL MasterKeyPlanSelectOne(3);
--selectAll
CALL MasterKeyPlanSelectAll();
--count
CALL MasterKeyPlanCount();
--selectById
CALL MasterKeyPlanSelectById(2);
--selectByIdProfile
CALL MasterKeyPlanSelectByIdProfile(1);
--selectByNumber
CALL MasterKeyPlanSelectByNumber('SH9999004');
--selectByDate
CALL MasterKeyPlanSelectByDate('2014-06-15');
--selectByInvoice
CALL MasterKeyPlanSelectByInvoice('F/9999025');
--selectLikeNumber
CALL MasterKeyPlanSelectLikeNumber('SH9999');
--selectLikeInvoice
CALL MasterKeyPlanSelectLikeInvoice('F/99990');
--selectLikeXNumber
CALL MasterKeyPlanSelectLikeXNumber('003');
--selectLikeXInvoice
CALL MasterKeyPlanSelectLikeXInvoice('5');



