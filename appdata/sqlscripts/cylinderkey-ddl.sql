﻿-- An Orm Apart -- Thursday 5th of February 2015 02:09:37 PM
-- 
-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS stshbe1q_cyl;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE CylinderKey
-- Created on Thursday 5th of February 2015 02:09:37 PM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `CylinderKey`;
CREATE TABLE `CylinderKey` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdCylinder` INT NOT NULL,
	`IdKey` INT NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`IdCylinder`) REFERENCES `Cylinder` (`Id`),
	CONSTRAINT FOREIGN KEY (`IdKey`) REFERENCES `Key` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;


