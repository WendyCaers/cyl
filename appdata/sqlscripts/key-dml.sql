﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML Insert Stored Procedure for Key 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyInsert`;
DELIMITER //
CREATE PROCEDURE `KeyInsert`
(
	OUT pId INT ,
	IN pIdMasterKeyPlan INT ,
	IN pIdKeyCode INT ,
	IN pName VARCHAR (10) CHARACTER SET UTF8 ,
	IN pIndex INT ,
	IN pInitialQuantity INT ,
	IN pBackorder INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Key`
	(
		`Key`.`IdMasterKeyPlan`,
		`Key`.`IdKeyCode`,
		`Key`.`Name`,
		`Key`.`Index`,
		`Key`.`InitialQuantity`,
		`Key`.`Backorder`,
		`Key`.`InsertedBy`,
		`Key`.`InsertedOn`
	)
	VALUES
	(
		pIdMasterKeyPlan,
		pIdKeyCode,
		pName,
		pIndex,
		pInitialQuantity,
		pBackorder,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML Update Stored Procedure for Key
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyUpdate`;
DELIMITER //
CREATE PROCEDURE `KeyUpdate`
(
	IN pId INT ,
	IN pIdMasterKeyPlan INT ,
	IN pIdKeyCode INT ,
	IN pName VARCHAR (10) CHARACTER SET UTF8 ,
	IN pIndex INT ,
	IN pInitialQuantity INT ,
	IN pBackorder INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Key`
SET
	`IdMasterKeyPlan` = pIdMasterKeyPlan,
	`IdKeyCode` = pIdKeyCode,
	`Name` = pName,
	`Index` = pIndex,
	`InitialQuantity` = pInitialQuantity,
	`Backorder` = pBackorder,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `Key`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML Delete Stored Procedure for Key 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyDelete`;
DELIMITER //
CREATE PROCEDURE `KeyDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Key`
WHERE `Key`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML SelectOne Stored Procedure for Key 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeySelectOne`;
DELIMITER //
CREATE PROCEDURE `KeySelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `Key`
	WHERE `Key`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML SelectAll Stored Procedure for table Key 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeySelectAll`;
DELIMITER //
CREATE PROCEDURE `KeySelectAll`
(
)
BEGIN
	SELECT * FROM `Key`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML Count Stored Procedure for table Key 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCount`;
DELIMITER //
CREATE PROCEDURE `KeyCount`
(
)
BEGIN
	SELECT count(*) FROM `Key`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML SelectById Stored Procedure for table Key
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeySelectById`;
DELIMITER //
CREATE PROCEDURE `KeySelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `Key`.`Id`, `Key`.`Name`, `Key`.`Index`, `Key`.`InitialQuantity`, `Key`.`Backorder`,
      `Key`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Key`.`IdKeyCode`, `KeyCode`.`Passive`, `KeyCode`.`Lateral`, `KeyCode`.`Active`
    FROM `Key`
    INNER JOIN `MasterKeyPlan`
		ON `Key`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `KeyCode`
		ON `Key`.`IdKeyCode` = `KeyCode`.`Id`
	WHERE `Key`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML SelectByIdMasterKeyPlan Stored Procedure for table Key
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeySelectByIdMasterKeyPlan`;
DELIMITER //
CREATE PROCEDURE `KeySelectByIdMasterKeyPlan`
(
	IN pIdMasterKeyPlan INT 
)
BEGIN
	SELECT `Key`.`Id`, `Key`.`Name`, `Key`.`Index`, `Key`.`InitialQuantity`, `Key`.`Backorder`,
      `Key`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Key`.`IdKeyCode`, `KeyCode`.`Passive`, `KeyCode`.`Lateral`, `KeyCode`.`Active`
    FROM `Key`
    INNER JOIN `MasterKeyPlan`
		ON `Key`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `KeyCode`
		ON `Key`.`IdKeyCode` = `KeyCode`.`Id`
	WHERE `Key`.`IdMasterKeyPlan` = pIdMasterKeyPlan;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:41:01 AM
-- DML SelectByIdKeyCode Stored Procedure for table Key
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeySelectByIdKeyCode`;
DELIMITER //
CREATE PROCEDURE `KeySelectByIdKeyCode`
(
	IN pIdKeyCode INT 
)
BEGIN
	SELECT `Key`.`Id`, `Key`.`Name`, `Key`.`Index`, `Key`.`InitialQuantity`, `Key`.`Backorder`,
      `Key`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Key`.`IdKeyCode`, `KeyCode`.`Passive`, `KeyCode`.`Lateral`, `KeyCode`.`Active`
    FROM `Key`
    INNER JOIN `MasterKeyPlan`
		ON `Key`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `KeyCode`
		ON `Key`.`IdKeyCode` = `KeyCode`.`Id`
	WHERE `Key`.`IdKeyCode` = pIdKeyCode;
END //
DELIMITER ;
