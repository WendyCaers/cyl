﻿--testen van stored procedures tabel CylinderSize
--30/12/2014

use stshbe1q_cyl;
--insert
CALL CylinderSizeInsert(@In, 1, '30*30', 'Wendy');
CALL CylinderSizeInsert(@In, 2, '30*35', 'Wendy');
CALL CylinderSizeInsert(@In, 2, '30/10' , 'Wendy');
CALL CylinderSizeInsert(@In, 3, 'opleg cu', 'Wendy');
CALL CylinderSizeInsert(@In, 3, 'opleg ni', 'Wendy');
--update
CALL CylinderSizeUpdate(1 , 1, '30/30', 'WendyC');
CALL CylinderSizeUpdate(2 , 1, '30/35', 'WendyC');
--delete
CALL CylinderSizeDelete(5);
--selectOne
CALL CylinderSizeSelectOne(3);
--selectAll
CALL CylinderSizeSelectAll();
--count
CALL CylinderSizeCount();
--selectById
CALL CylinderSizeSelectById(2);
--selectByIdCylinderType
CALL CylinderSizeSelectByIdCylinderType(1);
--selectByName
CALL CylinderSizeSelectByName('opleg cu');
--selectLikeName
CALL CylinderSizeSelectLikeName('30');
--selectLikeXName
CALL CylinderSizeSelectLikeXName('5');