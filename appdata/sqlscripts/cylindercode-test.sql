﻿--testen van stored procedures tabel CylinderCode
--30/12/2014

use stshbe1q_cyl;
--insert
CALL CylinderCodeInsert(@In, 10011, 001110, 510231, 0, 0, 0, 'Wendy');
CALL CylinderCodeInsert(@In, 01010, 110100, 432012, 001001, 0, 0, 'Wendy');
CALL CylinderCodeInsert(@In, 11100, 101010, 351023, 010000, 100000, 0, 'Wendy');
CALL CylinderCodeInsert(@In, 00011, 010101, 210103, 0, 0, 0, 'Wendy');
CALL CylinderCodeInsert(@In, 10101, 000111, 135042, 0, 0, 0, 'Wendy');
--update
CALL CylinderCodeUpdate(1 , 10011, 001110, 510231, 111111, 111111, 111111 , 'WendyC');
CALL CylinderCodeUpdate(2 , 01010, 110100, 432012, 001001, 101010, 101010, 'WendyC');
--delete
CALL CylinderCodeDelete(5);
--selectOne
CALL CylinderCodeSelectOne(3);
--selectAll
CALL CylinderCodeSelectAll();
--count
CALL CylinderCodeCount();
--selectById
CALL CylinderCodeSelectById(2);
--selectByCode
CALL CylinderCodeSelectByCode(01010, 110100, 432012, 001001, 101010, 101010);




