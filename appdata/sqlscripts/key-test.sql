﻿--testen van stored procedures tabel Key
--5/01/2014

--insert
CALL KeyInsert(@In, 4, 1, 'GMK', 1, 2, 0, 'Wendy');
CALL KeyInsert(@In, 3, 2, 'A1', 2, 1, 0, 'Wendy');
CALL KeyInsert(@In, 2, 3, 'A2', 3, 2, 0, 'Wendy');
CALL KeyInsert(@In, 3, 4, 'A3', 4, 1, 0, 'Wendy');
CALL KeyInsert(@In, 2, 1, 'A4', 5, 2, 0, 'Wendy');
--update
CALL KeyUpdate(1, 4, 1, 'A1', 1, 2, 1, 'WendyC');
CALL KeyUpdate(2, 3, 2, 'A2', 2, 1, 2, 'WendyC');
--delete
CALL KeyDelete(5);
--selectOne
CALL KeySelectOne(3);
--selectAll
CALL KeySelectAll();
--count
CALL KeyCount();
--selectById
CALL KeySelectById(2);
--selectByIdMasterKeyPlan
CALL KeySelectByIdMasterKeyPlan(3);
--selectByIdKeyCode
CALL KeySelectByIdKeyCode(3);

