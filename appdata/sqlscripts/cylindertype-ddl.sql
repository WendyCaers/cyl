﻿-- An Orm Apart -- Monday 29th of December 2014 05:37:02 AM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE CylinderType
-- Created on Monday 29th of December 2014 05:37:02 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `CylinderType`;
CREATE TABLE `CylinderType` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Name` VARCHAR (20) CHARACTER SET UTF8 NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Name UNIQUE (Name));

SET FOREIGN_KEY_CHECKS = 1;
