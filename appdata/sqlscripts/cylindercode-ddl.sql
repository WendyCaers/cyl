﻿-- An Orm Apart -- Monday 29th of December 2014 04:55:09 AM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE CylinderCode
-- Created on Monday 29th of December 2014 04:55:09 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `CylinderCode`;
CREATE TABLE `CylinderCode` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Passive` INT NOT NULL,
	`Lateral` INT NOT NULL,
	`Active` INT NOT NULL,
	`Add1` INT NULL,
	`Add2` INT NULL,
	`Add3` INT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL);

SET FOREIGN_KEY_CHECKS = 1;
