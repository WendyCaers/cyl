﻿-- An Orm Apart -- Monday 29th of December 2014 03:52:00 AM
-- 

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE MasterKeyPlan
-- Created on Monday 29th of December 2014 03:52:00 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `MasterKeyPlan`;
CREATE TABLE `MasterKeyPlan` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdProfile` INT NOT NULL,
	`Number` VARCHAR (20) CHARACTER SET UTF8 NOT NULL,
	`Date` DATE NOT NULL,
	`Invoice` VARCHAR (50) CHARACTER SET UTF8 NULL,
	`Description` VARCHAR (1000) CHARACTER SET UTF8 NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Number UNIQUE (Number),
	CONSTRAINT FOREIGN KEY (`IdProfile`) REFERENCES `Profile` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;
