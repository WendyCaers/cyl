﻿--testen van stored procedures tabel Profile
--29/12/2014

use stshbe1q_cyl;
--insert
CALL ProfileInsert(@In , 'Iseo', 'CSR', 'Wendy');
CALL ProfileInsert(@In , 'Iseo', 'CSF', 'Wendy');
CALL ProfileInsert(@In , 'Abus', 'XP2', 'Wendy');
CALL ProfileInsert(@In , 'Abus', 'Pfaffenhein', 'Wendy');
CALL ProfileInsert(@In , 'Dom', 'Diamant', 'Wendy'); 
--update
CALL ProfileUpdate(1 , 'Iseo', 'CSR R9 plus', 'Wendy');
CALL ProfileUpdate(2 , 'Iseo', 'CSF F9', 'Wendy');
--delete
CALL ProfileDelete(5);
--selectOne
CALL ProfileSelectOne(3);
--selectAll
CALL ProfileSelectAll();
--count
CALL ProfileCount();
--selectById
CALL ProfileSelectById(2);
--selectByBrand
CALL ProfileSelectByBrand('Iseo');
--SelectLikeBrand
CALL ProfileSelectLikeBrand('Ab');
--SelectLikeXBrand
CALL ProfileSelectLikeXBrand('se');