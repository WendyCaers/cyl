﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML Insert Stored Procedure for KeyCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeInsert`;
DELIMITER //
CREATE PROCEDURE `KeyCodeInsert`
(
	OUT pId INT ,
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `KeyCode`
	(
		`KeyCode`.`Passive`,
		`KeyCode`.`Lateral`,
		`KeyCode`.`Active`,
		`KeyCode`.`InsertedBy`,
		`KeyCode`.`InsertedOn`
	)
	VALUES
	(
		pPassive,
		pLateral,
		pActive,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML Update Stored Procedure for KeyCode
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeUpdate`;
DELIMITER //
CREATE PROCEDURE `KeyCodeUpdate`
(
	IN pId INT ,
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `KeyCode`
SET
	`Passive` = pPassive,
	`Lateral` = pLateral,
	`Active` = pActive,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `KeyCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML Delete Stored Procedure for KeyCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeDelete`;
DELIMITER //
CREATE PROCEDURE `KeyCodeDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `KeyCode`
WHERE `KeyCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML SelectOne Stored Procedure for KeyCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeSelectOne`;
DELIMITER //
CREATE PROCEDURE `KeyCodeSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `KeyCode`
	WHERE `KeyCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML SelectAll Stored Procedure for table KeyCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeSelectAll`;
DELIMITER //
CREATE PROCEDURE `KeyCodeSelectAll`
(
)
BEGIN
	SELECT * FROM `KeyCode`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML Count Stored Procedure for table KeyCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeCount`;
DELIMITER //
CREATE PROCEDURE `KeyCodeCount`
(
)
BEGIN
	SELECT count(*) FROM `KeyCode`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:12:08 AM
-- DML SelectById Stored Procedure for table KeyCode
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeSelectById`;
DELIMITER //
CREATE PROCEDURE `KeyCodeSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `KeyCode`.`Id`, `KeyCode`.`Passive`, `KeyCode`.`Lateral`, `KeyCode`.`Active`
 FROM `KeyCode`

	WHERE `KeyCode`.`Id` = pId;
END //
DELIMITER ;


-- CUSTOM
-- Created by Wendy Caers
-- DML SelectByCode Stored Procedure for table CylinderCode
--
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `KeyCodeSelectByCode`;
DELIMITER //
CREATE PROCEDURE `KeyCodeSelectByCode`
(
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT 
)
BEGIN
	SELECT `KeyCode`.`Id` 
   FROM `KeyCode`
	WHERE `KeyCode`.`Passive` = pPassive AND 
        `KeyCode`.`Lateral` = pLateral AND
        `KeyCode`.`Active` = pActive;
END //
DELIMITER ;