﻿-- An Orm Apart -- Monday 29th of December 2014 05:50:02 AM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE CylinderSize
-- Created on Monday 29th of December 2014 05:50:02 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `CylinderSize`;
CREATE TABLE `CylinderSize` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdCylinderType` INT NOT NULL,
	`Name` VARCHAR (20) CHARACTER SET UTF8 NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Name UNIQUE (Name),
	CONSTRAINT FOREIGN KEY (`IdCylinderType`) REFERENCES `CylinderType` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;
