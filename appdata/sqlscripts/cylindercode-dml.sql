﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML Insert Stored Procedure for CylinderCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeInsert`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeInsert`
(
	OUT pId INT ,
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT ,
	IN pAdd1 INT ,
	IN pAdd2 INT ,
	IN pAdd3 INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `CylinderCode`
	(
		`CylinderCode`.`Passive`,
		`CylinderCode`.`Lateral`,
		`CylinderCode`.`Active`,
		`CylinderCode`.`Add1`,
		`CylinderCode`.`Add2`,
		`CylinderCode`.`Add3`,
		`CylinderCode`.`InsertedBy`,
		`CylinderCode`.`InsertedOn`
	)
	VALUES
	(
		pPassive,
		pLateral,
		pActive,
		pAdd1,
		pAdd2,
		pAdd3,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML Update Stored Procedure for CylinderCode
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeUpdate`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeUpdate`
(
	IN pId INT ,
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT ,
	IN pAdd1 INT ,
	IN pAdd2 INT ,
	IN pAdd3 INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `CylinderCode`
SET
	`Passive` = pPassive,
	`Lateral` = pLateral,
	`Active` = pActive,
	`Add1` = pAdd1,
	`Add2` = pAdd2,
	`Add3` = pAdd3,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `CylinderCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML Delete Stored Procedure for CylinderCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeDelete`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `CylinderCode`
WHERE `CylinderCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML SelectOne Stored Procedure for CylinderCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeSelectOne`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `CylinderCode`
	WHERE `CylinderCode`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML SelectAll Stored Procedure for table CylinderCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeSelectAll`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeSelectAll`
(
)
BEGIN
	SELECT * FROM `CylinderCode`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML Count Stored Procedure for table CylinderCode 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeCount`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeCount`
(
)
BEGIN
	SELECT count(*) FROM `CylinderCode`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 04:55:45 AM
-- DML SelectById Stored Procedure for table CylinderCode
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeSelectById`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `CylinderCode`.`Id`, `CylinderCode`.`Passive`, `CylinderCode`.`Lateral`,
        `CylinderCode`.`Active`, `CylinderCode`.`Add1`, `CylinderCode`.`Add2`, `CylinderCode`.`Add3`
 FROM `CylinderCode`

	WHERE `CylinderCode`.`Id` = pId;
END //
DELIMITER ;

-- CUSTOM
-- Created by Wendy Caers
-- DML SelectByCode Stored Procedure for table CylinderCode
--
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCodeSelectByCode`;
DELIMITER //
CREATE PROCEDURE `CylinderCodeSelectByCode`
(
	IN pPassive INT ,
	IN pLateral INT ,
	IN pActive INT ,
	IN pAdd1 INT ,
	IN pAdd2 INT ,
	IN pAdd3 INT
)
BEGIN
	SELECT `CylinderCode`.`Id` 
   FROM `CylinderCode`
	WHERE `CylinderCode`.`Passive` = pPassive AND 
        `CylinderCode`.`Lateral` = pLateral AND
        `CylinderCode`.`Active` = pActive AND
        `CylinderCode`.`Add1` = pAdd1 AND
        `CylinderCode`.`Add2` = pAdd2 AND
        `CylinderCode`.`Add3` = pAdd3;
END //
DELIMITER ;