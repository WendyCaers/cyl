﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML Insert Stored Procedure for Cylinder 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderInsert`;
DELIMITER //
CREATE PROCEDURE `CylinderInsert`
(
	OUT pId INT ,
	IN pIdMasterKeyPlan INT ,
	IN pIdCylinderCode INT ,
	IN pIdCylinderSize INT ,
	IN pName VARCHAR (10) CHARACTER SET UTF8 ,
	IN pIndex INT ,
	IN pInitialQuantity INT ,
	IN pBackorder INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Cylinder`
	(
		`Cylinder`.`IdMasterKeyPlan`,
		`Cylinder`.`IdCylinderCode`,
		`Cylinder`.`IdCylinderSize`,
		`Cylinder`.`Name`,
		`Cylinder`.`Index`,
		`Cylinder`.`InitialQuantity`,
		`Cylinder`.`Backorder`,
		`Cylinder`.`InsertedBy`,
		`Cylinder`.`InsertedOn`
	)
	VALUES
	(
		pIdMasterKeyPlan,
		pIdCylinderCode,
		pIdCylinderSize,
		pName,
		pIndex,
		pInitialQuantity,
		pBackorder,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML Update Stored Procedure for Cylinder
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderUpdate`;
DELIMITER //
CREATE PROCEDURE `CylinderUpdate`
(
	IN pId INT ,
	IN pIdMasterKeyPlan INT ,
	IN pIdCylinderCode INT ,
	IN pIdCylinderSize INT ,
	IN pName VARCHAR (10) CHARACTER SET UTF8 ,
	IN pIndex INT ,
	IN pInitialQuantity INT ,
	IN pBackorder INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Cylinder`
SET
	`IdMasterKeyPlan` = pIdMasterKeyPlan,
	`IdCylinderCode` = pIdCylinderCode,
	`IdCylinderSize` = pIdCylinderSize,
	`Name` = pName,
	`Index` = pIndex,
	`InitialQuantity` = pInitialQuantity,
	`Backorder` = pBackorder,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `Cylinder`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML Delete Stored Procedure for Cylinder 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderDelete`;
DELIMITER //
CREATE PROCEDURE `CylinderDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Cylinder`
WHERE `Cylinder`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectOne Stored Procedure for Cylinder 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectOne`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `Cylinder`
	WHERE `Cylinder`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectAll Stored Procedure for table Cylinder 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectAll`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectAll`
(
)
BEGIN
	SELECT * FROM `Cylinder`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML Count Stored Procedure for table Cylinder 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderCount`;
DELIMITER //
CREATE PROCEDURE `CylinderCount`
(
)
BEGIN
	SELECT count(*) FROM `Cylinder`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectById Stored Procedure for table Cylinder
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectById`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `Cylinder`.`Id`, `Cylinder`.`Name`, `Cylinder`.`Index`, `Cylinder`.`InitialQuantity`, `Cylinder`.`Backorder`,
      `Cylinder`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Cylinder`.`IdCylinderCode`, `CylinderCode`.`Passive`, `CylinderCode`.`Lateral`, `CylinderCode`.`Active`,
            `CylinderCode`.`Add1`, `CylinderCode`.`Add2`, `CylinderCode`.`Add3`,
      `Cylinder`.`IdCylinderSize`, `CylinderSize`.`Name` as `Size`
    FROM `Cylinder`
    INNER JOIN `MasterKeyPlan`
		ON `Cylinder`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `CylinderCode`
		ON `Cylinder`.`IdCylinderCode` = `CylinderCode`.`Id`
    INNER JOIN `CylinderSize`
		ON `Cylinder`.`IdCylinderSize` = `CylinderSize`.`Id`
	WHERE `Cylinder`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectByIdMasterKeyPlan Stored Procedure for table Cylinder
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectByIdMasterKeyPlan`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectByIdMasterKeyPlan`
(
	IN pIdMasterKeyPlan INT 
)
BEGIN
	SELECT `Cylinder`.`Id`, `Cylinder`.`Name`, `Cylinder`.`Index`, `Cylinder`.`InitialQuantity`, `Cylinder`.`Backorder`,
      `Cylinder`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Cylinder`.`IdCylinderCode`, `CylinderCode`.`Passive`, `CylinderCode`.`Lateral`, `CylinderCode`.`Active`,
            `CylinderCode`.`Add1`, `CylinderCode`.`Add2`, `CylinderCode`.`Add3`,
      `Cylinder`.`IdCylinderSize`, `CylinderSize`.`Name` as `Size`
    FROM `Cylinder`
    INNER JOIN `MasterKeyPlan`
		ON `Cylinder`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `CylinderCode`
		ON `Cylinder`.`IdCylinderCode` = `CylinderCode`.`Id`
    INNER JOIN `CylinderSize`
		ON `Cylinder`.`IdCylinderSize` = `CylinderSize`.`Id`
	WHERE `Cylinder`.`IdMasterKeyPlan` = pIdMasterKeyPlan;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectByIdCylinderCode Stored Procedure for table Cylinder
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectByIdCylinderCode`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectByIdCylinderCode`
(
	IN pIdCylinderCode INT 
)
BEGIN
	SELECT `Cylinder`.`Id`, `Cylinder`.`Name`, `Cylinder`.`Index`, `Cylinder`.`InitialQuantity`, `Cylinder`.`Backorder`,
      `Cylinder`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Cylinder`.`IdCylinderCode`, `CylinderCode`.`Passive`, `CylinderCode`.`Lateral`, `CylinderCode`.`Active`,
            `CylinderCode`.`Add1`, `CylinderCode`.`Add2`, `CylinderCode`.`Add3`,
      `Cylinder`.`IdCylinderSize`, `CylinderSize`.`Name` as `Size`
    FROM `Cylinder`
    INNER JOIN `MasterKeyPlan`
		ON `Cylinder`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `CylinderCode`
		ON `Cylinder`.`IdCylinderCode` = `CylinderCode`.`Id`
    INNER JOIN `CylinderSize`
		ON `Cylinder`.`IdCylinderSize` = `CylinderSize`.`Id`
	WHERE `Cylinder`.`IdCylinderCode` = pIdCylinderCode;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 06:18:09 AM
-- DML SelectByIdCylinderSize Stored Procedure for table Cylinder
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSelectByIdCylinderSize`;
DELIMITER //
CREATE PROCEDURE `CylinderSelectByIdCylinderSize`
(
	IN pIdCylinderSize INT 
)
BEGIN
	SELECT `Cylinder`.`Id`, `Cylinder`.`Name`, `Cylinder`.`Index`, `Cylinder`.`InitialQuantity`, `Cylinder`.`Backorder`,
      `Cylinder`.`IdMasterKeyPlan`, `MasterKeyPlan`.`Number`, `MasterKeyPlan`.`Date`, `MasterKeyPlan`.`Invoice`,
            `MasterKeyPlan`.`Description`,
       `Cylinder`.`IdCylinderCode`, `CylinderCode`.`Passive`, `CylinderCode`.`Lateral`, `CylinderCode`.`Active`,
            `CylinderCode`.`Add1`, `CylinderCode`.`Add2`, `CylinderCode`.`Add3`,
      `Cylinder`.`IdCylinderSize`, `CylinderSize`.`Name` as `Size`
    FROM `Cylinder`
    INNER JOIN `MasterKeyPlan`
		ON `Cylinder`.`IdMasterKeyPlan` = `MasterKeyPlan`.`Id`
	INNER JOIN `CylinderCode`
		ON `Cylinder`.`IdCylinderCode` = `CylinderCode`.`Id`
    INNER JOIN `CylinderSize`
		ON `Cylinder`.`IdCylinderSize` = `CylinderSize`.`Id`
	WHERE `Cylinder`.`IdCylinderSize` = pIdCylinderSize;
END //
DELIMITER ;
