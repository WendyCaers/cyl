﻿-- An Orm Apart -- Monday 29th of December 2014 06:40:36 AM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Key
-- Created on Monday 29th of December 2014 06:40:36 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `Key`;
CREATE TABLE `Key` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMasterKeyPlan` INT NOT NULL,
	`IdKeyCode` INT NOT NULL,
	`Name` VARCHAR (10) CHARACTER SET UTF8 NOT NULL,
	`Index` INT NOT NULL,
	`InitialQuantity` INT NOT NULL,
	`Backorder` INT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`IdMasterKeyPlan`) REFERENCES `MasterKeyPlan` (`Id`),
	CONSTRAINT FOREIGN KEY (`IdKeyCode`) REFERENCES `KeyCode` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;
