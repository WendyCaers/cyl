﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML Insert Stored Procedure for Profile 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileInsert`;
DELIMITER //
CREATE PROCEDURE `ProfileInsert`
(
	OUT pId INT ,
	IN pBrand VARCHAR (20) CHARACTER SET UTF8 ,
	IN pType VARCHAR (20) CHARACTER SET UTF8 ,
    IN pImg VARCHAR (256) CHARACTER SET UTF8 ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Profile`
	(
		`Profile`.`Brand`,
		`Profile`.`Type`,
        `Profile`.`Img`,
		`Profile`.`InsertedBy`,
		`Profile`.`InsertedOn`
	)
	VALUES
	(
		pBrand,
		pType,
        pImg,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML Update Stored Procedure for Profile
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileUpdate`;
DELIMITER //
CREATE PROCEDURE `ProfileUpdate`
(
	IN pId INT ,
	IN pBrand VARCHAR (20) CHARACTER SET UTF8 ,
	IN pType VARCHAR (20) CHARACTER SET UTF8 ,
    IN pImg VARCHAR (256) CHARACTER SET UTF8 ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Profile`
SET
	`Brand` = pBrand,
	`Type` = pType,
    `Img` = pImg,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `Profile`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML Delete Stored Procedure for Profile 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileDelete`;
DELIMITER //
CREATE PROCEDURE `ProfileDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Profile`
WHERE `Profile`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectOne Stored Procedure for Profile 
-- NG
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectOne`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `Profile`
	WHERE `Profile`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectAll Stored Procedure for table Profile 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectAll`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectAll`
(
)
BEGIN
	SELECT * FROM `Profile`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML Count Stored Procedure for table Profile 
-- NG
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileCount`;
DELIMITER //
CREATE PROCEDURE `ProfileCount`
(
)
BEGIN
	SELECT count(*) FROM `Profile`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectById Stored Procedure for table Profile
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectById`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `Profile`.`Id`, `Profile`.`Brand`, `Profile`.`Type`, `Profile`.`Img`
 FROM `Profile`

	WHERE `Profile`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectByBrand Stored Procedure for table Profile
-- NG
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectByBrand`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectByBrand`
(
	IN pBrand VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Profile`.`Id`, `Profile`.`Brand`, `Profile`.`Type`, `Profile`.`Img`
 FROM `Profile`

	WHERE `Profile`.`Brand` = pBrand;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectLikeBrand Stored Procedure for table Profile 
-- NG
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectLikeBrand`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectLikeBrand`
(
	IN pBrand VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Profile`.`Id`, `Profile`.`Brand`, `Profile`.`Type`, `Profile`.`Img`
 FROM `Profile`

	WHERE `Profile`.`Brand` like CONCAT(pBrand, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 02:11:45 AM
-- DML SelectLikeXBrand Stored Procedure for table Profile
-- NG
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `ProfileSelectLikeXBrand`;
DELIMITER //
CREATE PROCEDURE `ProfileSelectLikeXBrand`
(
	IN pBrand VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Profile`.`Id`, `Profile`.`Brand`, `Profile`.`Type`, `Profile`.`Img`
 FROM `Profile`

	WHERE `Profile`.`Brand` like CONCAT('%', pBrand, '%');
END //
DELIMITER ;


