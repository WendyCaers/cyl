﻿-- An Orm Apart -- Monday 29th of December 2014 06:17:39 AM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Cylinder
-- Created on Monday 29th of December 2014 06:17:39 AM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `Cylinder`;
CREATE TABLE `Cylinder` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMasterKeyPlan` INT NOT NULL,
	`IdCylinderCode` INT NOT NULL,
	`IdCylinderSize` INT NOT NULL,
	`Name` VARCHAR (10) CHARACTER SET UTF8 NOT NULL,
	`Index` INT NOT NULL,
	`InitialQuantity` INT NOT NULL,
	`Backorder` INT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`IdMasterKeyPlan`) REFERENCES `MasterKeyPlan` (`Id`),
	CONSTRAINT FOREIGN KEY (`IdCylinderCode`) REFERENCES `CylinderCode` (`Id`),
	CONSTRAINT FOREIGN KEY (`IdCylinderSize`) REFERENCES `CylinderSize` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;
