﻿-- An Orm Apart -- Tuesday 10th of June 2014 12:30:29 PM
-- 
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Member
-- Created on Tuesday 10th of June 2014 12:30:29 PM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `Member`;
CREATE TABLE `Member` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`UserName` VARCHAR (80) CHARACTER SET UTF8 NOT NULL,
	`Email` VARCHAR (80) CHARACTER SET UTF8 NOT NULL,
	`Password` VARCHAR (255) NOT NULL,
	`LastActivity` TIMESTAMP NULL,
	`FirstLogin` TIMESTAMP NULL,
	`Authenticated` BOOL NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_UserName UNIQUE (UserName),
	CONSTRAINT uc_Email UNIQUE (Email));

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE LoginAttempt
-- Created on Tuesday 10th of June 2014 12:30:29 PM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `LoginAttempt`;
CREATE TABLE `LoginAttempt` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`Time` VARCHAR (30) NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`IdMember`) REFERENCES `Member` (`Id`));

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Role
-- Created on Tuesday 10th of June 2014 12:30:29 PM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Name` VARCHAR (100) CHARACTER SET UTF8 NOT NULL,
	`Description` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Name UNIQUE (Name));

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE MemberRole
-- Created on Tuesday 10th of June 2014 12:30:29 PM
-- 
USE `stshbe1q_cyl`;
DROP TABLE IF EXISTS `MemberRole`;
CREATE TABLE `MemberRole` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`IdRole` INT NOT NULL,
	`InsertedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` VARCHAR (256) CHARACTER SET UTF8 NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT FOREIGN KEY (`IdMember`) REFERENCES `Member` (`Id`),
	CONSTRAINT FOREIGN KEY (`IdRole`) REFERENCES `Role` (`Id`));

SET FOREIGN_KEY_CHECKS = 1;
