﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Insert Stored Procedure for Member 
-- 
USE `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberInsert`;
DELIMITER //
CREATE PROCEDURE `MemberInsert`
(
	OUT pId INT ,
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (80) CHARACTER SET UTF8 ,
	IN pPassword VARCHAR (255) ,
	IN pAuthenticated BOOL ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Member`
	(
		`Member`.`UserName`,
		`Member`.`Email`,
		`Member`.`Password`,
		`Member`.`FirstLogin`,
		`Member`.`Authenticated`,
		`Member`.`InsertedBy`,
		`Member`.`InsertedOn`
	)
	VALUES
	(
		pUserName,
		pEmail,
		pPassword,
		NOW(),
		pAuthenticated,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Update Stored Procedure for Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberUpdate`;
DELIMITER //
CREATE PROCEDURE `MemberUpdate`
(
	IN pId INT ,
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 ,
	IN pEmail VARCHAR (80) CHARACTER SET UTF8 ,
	IN pPassword VARCHAR (255) ,
	IN pAuthenticated BOOL ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Member`
SET
	`UserName` = pUserName,
	`Email` = pEmail,
	`Password` = pPassword,
	`LastActivity` = NOW(),
	`Authenticated` = pAuthenticated,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `Member`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Delete Stored Procedure for Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberDelete`;
DELIMITER //
CREATE PROCEDURE `MemberDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Member`
WHERE `Member`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectOne Stored Procedure for Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectOne`;
DELIMITER //
CREATE PROCEDURE `MemberSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `Member`
	WHERE `Member`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectAll Stored Procedure for table Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectAll`;
DELIMITER //
CREATE PROCEDURE `MemberSelectAll`
(
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Count Stored Procedure for table Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberCount`;
DELIMITER //
CREATE PROCEDURE `MemberCount`
(
)
BEGIN
	SELECT count(*) FROM `Member`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectById Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectById`;
DELIMITER //
CREATE PROCEDURE `MemberSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByUserName Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectByUserName`;
DELIMITER //
CREATE PROCEDURE `MemberSelectByUserName`
(
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`UserName` = pUserName;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByEmail Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectByEmail`;
DELIMITER //
CREATE PROCEDURE `MemberSelectByEmail`
(
	IN pEmail VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Email` = pEmail;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeId Stored Procedure for table Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeId`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeId`
(
	IN pId INT 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Id` like CONCAT(pId, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeUserName Stored Procedure for table Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeUserName`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeUserName`
(
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`UserName` like CONCAT(pUserName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeEmail Stored Procedure for table Member 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeEmail`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeEmail`
(
	IN pEmail VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Email` like CONCAT(pEmail, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeXId Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeXId`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeXId`
(
	IN pId INT 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Id` like CONCAT('%', pId, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeXUserName Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeXUserName`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeXUserName`
(
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`UserName` like CONCAT('%', pUserName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeXEmail Stored Procedure for table Member
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberSelectLikeXEmail`;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeXEmail`
(
	IN pEmail VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`
 FROM `Member`

	WHERE `Member`.`Email` like CONCAT('%', pEmail, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Insert Stored Procedure for LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptInsert`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptInsert`
(
	OUT pId INT ,
	IN pIdMember INT ,
	IN pTime VARCHAR (30) ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `LoginAttempt`
	(
		`LoginAttempt`.`IdMember`,
		`LoginAttempt`.`Time`,
		`LoginAttempt`.`InsertedBy`,
		`LoginAttempt`.`InsertedOn`
	)
	VALUES
	(
		pIdMember,
		pTime,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Update Stored Procedure for LoginAttempt
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptUpdate`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptUpdate`
(
	IN pId INT ,
	IN pIdMember INT ,
	IN pTime VARCHAR (30) ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `LoginAttempt`
SET
	`IdMember` = pIdMember,
	`Time` = pTime,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `LoginAttempt`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Delete Stored Procedure for LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptDelete`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `LoginAttempt`
WHERE `LoginAttempt`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectOne Stored Procedure for LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptSelectOne`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `LoginAttempt`
	WHERE `LoginAttempt`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectAll Stored Procedure for table LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptSelectAll`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectAll`
(
)
BEGIN
	SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`Time`, `LoginAttempt`.`IdMember`, `Member`.`UserName`
 FROM `LoginAttempt`
 INNER JOIN `Member`
		ON `LoginAttempt`.`IdMember` = `Member`.`Id`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Count Stored Procedure for table LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptCount`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptCount`
(
)
BEGIN
	SELECT count(*) FROM `LoginAttempt`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Count Stored Procedure for table LoginAttempt 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptCountIdMemberByTime`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptCountIdMemberByTime`
(
		IN pTime VARCHAR (30) 
)
BEGIN
	SELECT COUNT(`IdMember`) AS Counted FROM `LoginAttempt`
		WHERE `Time` = pTime;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectById Stored Procedure for table LoginAttempt
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptSelectById`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`Time`, `LoginAttempt`.`IdMember`, `Member`.`UserName`
 FROM `LoginAttempt`
 INNER JOIN `Member`
		ON `LoginAttempt`.`IdMember` = `Member`.`Id`
	WHERE `LoginAttempt`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByIdMember Stored Procedure for table LoginAttempt
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `LoginAttemptSelectByIdMember`;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectByIdMember`
(
	IN pIdMember INT 
)
BEGIN
	SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`Time`, `LoginAttempt`.`IdMember`, `Member`.`UserName`
 FROM `LoginAttempt`
 INNER JOIN `Member`
		ON `LoginAttempt`.`IdMember` = `Member`.`Id`
	WHERE `LoginAttempt`.`IdMember` = pIdMember;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Insert Stored Procedure for Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleInsert`;
DELIMITER //
CREATE PROCEDURE `RoleInsert`
(
	OUT pId INT ,
	IN pName VARCHAR (100) CHARACTER SET UTF8 ,
	IN pDescription VARCHAR (256) CHARACTER SET UTF8 ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `Role`
	(
		`Role`.`Name`,
		`Role`.`Description`,
		`Role`.`InsertedBy`,
		`Role`.`InsertedOn`
	)
	VALUES
	(
		pName,
		pDescription,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Update Stored Procedure for Role
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleUpdate`;
DELIMITER //
CREATE PROCEDURE `RoleUpdate`
(
	IN pId INT ,
	IN pName VARCHAR (100) CHARACTER SET UTF8 ,
	IN pDescription VARCHAR (256) CHARACTER SET UTF8 ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Role`
SET
	`Name` = pName,
	`Description` = pDescription,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `Role`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Delete Stored Procedure for Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleDelete`;
DELIMITER //
CREATE PROCEDURE `RoleDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `Role`
WHERE `Role`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectOne Stored Procedure for Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectOne`;
DELIMITER //
CREATE PROCEDURE `RoleSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `Role`
	WHERE `Role`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectAll Stored Procedure for table Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectAll`;
DELIMITER //
CREATE PROCEDURE `RoleSelectAll`
(
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`, `Role`.`Description`
 FROM `Role`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Count Stored Procedure for table Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleCount`;
DELIMITER //
CREATE PROCEDURE `RoleCount`
(
)
BEGIN
	SELECT count(*) FROM `Role`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectById Stored Procedure for table Role
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectById`;
DELIMITER //
CREATE PROCEDURE `RoleSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`, `Role`.`Description`
 FROM `Role`

	WHERE `Role`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByName Stored Procedure for table Role
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectByName`;
DELIMITER //
CREATE PROCEDURE `RoleSelectByName`
(
	IN pName VARCHAR (100) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`, `Role`.`Description`
 FROM `Role`

	WHERE `Role`.`Name` = pName;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeName Stored Procedure for table Role 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectLikeName`;
DELIMITER //
CREATE PROCEDURE `RoleSelectLikeName`
(
	IN pName VARCHAR (100) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`, `Role`.`Description`
 FROM `Role`

	WHERE `Role`.`Name` like CONCAT(pName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectLikeXName Stored Procedure for table Role
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `RoleSelectLikeXName`;
DELIMITER //
CREATE PROCEDURE `RoleSelectLikeXName`
(
	IN pName VARCHAR (100) CHARACTER SET UTF8 
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`, `Role`.`Description`
 FROM `Role`

	WHERE `Role`.`Name` like CONCAT('%', pName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Insert Stored Procedure for MemberRole 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleInsert`;
DELIMITER //
CREATE PROCEDURE `MemberRoleInsert`
(
	OUT pId INT ,
	IN pIdMember INT ,
	IN pIdRole INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `MemberRole`
	(
		`MemberRole`.`IdMember`,
		`MemberRole`.`IdRole`,
		`MemberRole`.`InsertedBy`,
		`MemberRole`.`InsertedOn`
	)
	VALUES
	(
		pIdMember,
		pIdRole,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Update Stored Procedure for MemberRole
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleUpdate`;
DELIMITER //
CREATE PROCEDURE `MemberRoleUpdate`
(
	IN pId INT ,
	IN pIdMember INT ,
	IN pIdRole INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `MemberRole`
SET
	`IdMember` = pIdMember,
	`IdRole` = pIdRole,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `MemberRole`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Delete Stored Procedure for MemberRole 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleDelete`;
DELIMITER //
CREATE PROCEDURE `MemberRoleDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `MemberRole`
WHERE `MemberRole`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectOne Stored Procedure for MemberRole 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectOne`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `MemberRole`
	WHERE `MemberRole`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectAll Stored Procedure for table MemberRole 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectAll`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectAll`
(
)
BEGIN
	SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `Member`.`UserName`, `MemberRole`.`IdRole`, `Role`.`Name`
 FROM `MemberRole`	
 INNER JOIN `Member`
		ON `MemberRole`.`IdMember` = `Member`.`Id`
 INNER JOIN `Role`
		ON `MemberRole`.`IdRole` = `Role`.`Id`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML Count Stored Procedure for table MemberRole 
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleCount`;
DELIMITER //
CREATE PROCEDURE `MemberRoleCount`
(
)
BEGIN
	SELECT count(*) FROM `MemberRole`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectById Stored Procedure for table MemberRole
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectById`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `Member`.`UserName`, `MemberRole`.`IdRole`, `Role`.`Name`
 FROM `MemberRole`
	INNER JOIN `Member`
		ON `MemberRole`.`IdMember` = `Member`.`Id`
	INNER JOIN `Role`
		ON `MemberRole`.`IdRole` = `Role`.`Id`
	WHERE `MemberRole`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByIdMember Stored Procedure for table MemberRole
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectByIdMember`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectByIdMember`
(
	IN pIdMember INT 
)
BEGIN
	SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `Member`.`UserName`, `MemberRole`.`IdRole`, `Role`.`Name`
 FROM `MemberRole`
	INNER JOIN `Member`
		ON `MemberRole`.`IdMember` = `Member`.`Id`
	INNER JOIN `Role`
		ON `MemberRole`.`IdRole` = `Role`.`Id`
	WHERE `MemberRole`.`IdMember` = pIdMember;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Tuesday 10th of June 2014 12:29:52 PM
-- DML SelectByIdRole Stored Procedure for table MemberRole
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectByIdRole`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectByIdRole`
(
	IN pIdRole INT 
)
BEGIN
	SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `Member`.`UserName`, `MemberRole`.`IdRole`, `Role`.`Name`
 FROM `MemberRole`
	INNER JOIN `Member`
		ON `MemberRole`.`IdMember` = `Member`.`Id`
	INNER JOIN `Role`
		ON `MemberRole`.`IdRole` = `Role`.`Id`
	WHERE `MemberRole`.`IdRole` = pIdRole;
END //
DELIMITER ;

-- inantwerpen.com
-- created by Wendy Caers
-- MySql DML
-- Created : Wednesday 1th of April 2015
-- DML SelectByIdMemberRoleNameOnly Stored Procedure for table MemberRole
-- 
USE  `stshbe1q_cyl`;
DROP PROCEDURE IF EXISTS `MemberRoleSelectByIdMemberRoleNameOnly`;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectByIdMemberRoleNameOnly`
(
	IN pIdMember INT 
)
BEGIN
	SELECT `Role`.`Name`
 FROM `MemberRole`
	INNER JOIN `Role`
		ON `MemberRole`.`IdRole` = `Role`.`Id`
	WHERE `MemberRole`.`IdMember` = pIdMember;
END //
DELIMITER ;

