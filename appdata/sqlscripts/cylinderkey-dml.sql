﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML Insert Stored Procedure for CylinderKey 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeyInsert`;
DELIMITER //
CREATE PROCEDURE `CylinderKeyInsert`
(
	OUT pId INT ,
	IN pIdCylinder INT ,
	IN pIdKey INT ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `CylinderKey`
	(
		`CylinderKey`.`IdCylinder`,
		`CylinderKey`.`IdKey`,
		`CylinderKey`.`InsertedBy`,
		`CylinderKey`.`InsertedOn`
	)
	VALUES
	(
		pIdCylinder,
		pIdKey,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML Update Stored Procedure for CylinderKey
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeyUpdate`;
DELIMITER //
CREATE PROCEDURE `CylinderKeyUpdate`
(
	IN pId INT ,
	IN pIdCylinder INT ,
	IN pIdKey INT ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `CylinderKey`
SET
	`IdCylinder` = pIdCylinder,
	`IdKey` = pIdKey,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `CylinderKey`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML Delete Stored Procedure for CylinderKey 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeyDelete`;
DELIMITER //
CREATE PROCEDURE `CylinderKeyDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `CylinderKey`
WHERE `CylinderKey`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML SelectOne Stored Procedure for CylinderKey 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeySelectOne`;
DELIMITER //
CREATE PROCEDURE `CylinderKeySelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `CylinderKey`
	WHERE `CylinderKey`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML SelectAll Stored Procedure for table CylinderKey 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeySelectAll`;
DELIMITER //
CREATE PROCEDURE `CylinderKeySelectAll`
(
)
BEGIN
	SELECT  * FROM `CylinderKey`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML Count Stored Procedure for table CylinderKey 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeyCount`;
DELIMITER //
CREATE PROCEDURE `CylinderKeyCount`
(
)
BEGIN
	SELECT count(*) FROM `CylinderKey`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML SelectById Stored Procedure for table CylinderKey
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeySelectById`;
DELIMITER //
CREATE PROCEDURE `CylinderKeySelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `CylinderKey`.`Id`, 
        `CylinderKey`.`IdCylinder`, `Cylinder`.`Name` as `CylinderName`, `Cylinder`.`Index` as `CylinderIndex` ,
        `CylinderKey`.`IdKey`, `Key`.`Name` as `KeyName` , `Key`.`Index` as `KeyIndex`
 FROM `CylinderKey`
    INNER JOIN `Cylinder`
        ON `CylinderKey`.`IdCylinder` = `Cylinder`.`Id`
    INNER JOIN `Key`
        ON `CylinderKey`.`IdKey` = `Key`.`Id`
	WHERE `CylinderKey`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML SelectByIdCylinder Stored Procedure for table CylinderKey
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeySelectByIdCylinder`;
DELIMITER //
CREATE PROCEDURE `CylinderKeySelectByIdCylinder`
(
	IN pIdCylinder INT 
)
BEGIN
	SELECT `CylinderKey`.`Id`, 
        `CylinderKey`.`IdCylinder`, `Cylinder`.`Name` as `CylinderName`, `Cylinder`.`Index` as `CylinderIndex` ,
        `CylinderKey`.`IdKey`, `Key`.`Name` as `KeyName` , `Key`.`Index` as `KeyIndex`
 FROM `CylinderKey`
    INNER JOIN `Cylinder`
        ON `CylinderKey`.`IdCylinder` = `Cylinder`.`Id`
    INNER JOIN `Key`
        ON `CylinderKey`.`IdKey` = `Key`.`Id`
	WHERE `CylinderKey`.`IdCylinder` = pIdCylinder;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Thursday 5th of February 2015 02:08:58 PM
-- DML SelectByIdKey Stored Procedure for table CylinderKey
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderKeySelectByIdKey`;
DELIMITER //
CREATE PROCEDURE `CylinderKeySelectByIdKey`
(
	IN pIdKey INT 
)
BEGIN
	SELECT `CylinderKey`.`Id`, 
        `CylinderKey`.`IdCylinder`, `Cylinder`.`Name` as `CylinderName`, `Cylinder`.`Index` as `CylinderIndex` ,
        `CylinderKey`.`IdKey`, `Key`.`Name` as `KeyName` , `Key`.`Index` as `KeyIndex`
 FROM `CylinderKey`
    INNER JOIN `Cylinder`
        ON `CylinderKey`.`IdCylinder` = `Cylinder`.`Id`
    INNER JOIN `Key`
        ON `CylinderKey`.`IdKey` = `Key`.`Id`
	WHERE `CylinderKey`.`IdKey` = pIdKey;
END //
DELIMITER ;


