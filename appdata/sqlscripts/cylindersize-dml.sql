﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML Insert Stored Procedure for CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeInsert`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeInsert`
(
	OUT pId INT ,
	IN pIdCylinderType INT ,
	IN pName VARCHAR (20) CHARACTER SET UTF8 ,
	IN pInsertedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
	INSERT INTO `CylinderSize`
	(
		`CylinderSize`.`IdCylinderType`,
		`CylinderSize`.`Name`,
		`CylinderSize`.`InsertedBy`,
		`CylinderSize`.`InsertedOn`
	)
	VALUES
	(
		pIdCylinderType,
		pName,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML Update Stored Procedure for CylinderSize
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeUpdate`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeUpdate`
(
	IN pId INT ,
	IN pIdCylinderType INT ,
	IN pName VARCHAR (20) CHARACTER SET UTF8 ,
	IN pUpdatedBy VARCHAR (256) CHARACTER SET UTF8 
)
BEGIN
UPDATE `CylinderSize`
SET
	`IdCylinderType` = pIdCylinderType,
	`Name` = pName,
	`UpdatedBy` = pUpdatedBy,
	`UpdatedOn` = NOW()
WHERE `CylinderSize`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML Delete Stored Procedure for CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeDelete`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeDelete`
(
	IN pId INT
)
BEGIN
DELETE FROM `CylinderSize`
WHERE `CylinderSize`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectOne Stored Procedure for CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectOne`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectOne`
(
	IN pId INT
)
BEGIN
	SELECT * FROM `CylinderSize`
	WHERE `CylinderSize`.Id = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectAll Stored Procedure for table CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectAll`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectAll`
(
)
BEGIN
	SELECT * FROM `CylinderSize`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML Count Stored Procedure for table CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeCount`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeCount`
(
)
BEGIN
	SELECT count(*) FROM `CylinderSize`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectById Stored Procedure for table CylinderSize
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectById`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectById`
(
	IN pId INT 
)
BEGIN
	SELECT `CylinderSize`.`Id`, `CylinderSize`.`IdCylinderType`, `CylinderSize`.`Name`,
        `CylinderType`.`Name` as `Type`
 FROM `CylinderSize`
    INNER JOIN `CylinderType`
        ON `CylinderSize`.`IdCylinderType` = `CylinderType`.`Id`
	WHERE `CylinderSize`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectByIdCylinderType Stored Procedure for table CylinderSize
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectByIdCylinderType`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectByIdCylinderType`
(
	IN pIdCylinderType INT 
)
BEGIN
	SELECT `CylinderSize`.`Id`, `CylinderSize`.`IdCylinderType`, `CylinderSize`.`Name`,
        `CylinderType`.`Name` as `Type`
 FROM `CylinderSize`
    INNER JOIN `CylinderType`
        ON `CylinderSize`.`IdCylinderType` = `CylinderType`.`Id`
	WHERE `CylinderSize`.`IdCylinderType` = pIdCylinderType;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectByName Stored Procedure for table CylinderSize
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectByName`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectByName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderSize`.`Id`, `CylinderSize`.`IdCylinderType`, `CylinderSize`.`Name`,
        `CylinderType`.`Name` as `Type`
    FROM `CylinderSize`
    INNER JOIN `CylinderType`
        ON `CylinderSize`.`IdCylinderType` = `CylinderType`.`Id`
	WHERE `CylinderSize`.`Name` = pName;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectLikeName Stored Procedure for table CylinderSize 
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectLikeName`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectLikeName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderSize`.`Id`, `CylinderSize`.`IdCylinderType`, `CylinderSize`.`Name`,
        `CylinderType`.`Name` as `Type`
    FROM `CylinderSize`
    INNER JOIN `CylinderType`
        ON `CylinderSize`.`IdCylinderType` = `CylinderType`.`Id`
	WHERE `CylinderSize`.`Name` like CONCAT(pName, '%');
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Monday 29th of December 2014 05:50:31 AM
-- DML SelectLikeXName Stored Procedure for table CylinderSize
-- 
USE stshbe1q_cyl;
DROP PROCEDURE IF EXISTS `CylinderSizeSelectLikeXName`;
DELIMITER //
CREATE PROCEDURE `CylinderSizeSelectLikeXName`
(
	IN pName VARCHAR (20) CHARACTER SET UTF8 
)
BEGIN
	SELECT `CylinderSize`.`Id`, `CylinderSize`.`IdCylinderType`, `CylinderSize`.`Name`,
        `CylinderType`.`Name` as `Type`
    FROM `CylinderSize`
    INNER JOIN `CylinderType`
        ON `CylinderSize`.`IdCylinderType` = `CylinderType`.`Id`
	WHERE `CylinderSize`.`Name` like CONCAT('%', pName, '%')
;
END //
DELIMITER ;
