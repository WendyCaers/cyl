﻿--testen van stored procedures tabel Cylinder
--5/01/2014

use stshbe1q_cyl;
--insert
CALL CylinderInsert(@In, 4, 1, 3, 'app1', 1, 2, 0, 'Wendy');
CALL CylinderInsert(@In, 3, 2, 4, 'app2', 2, 1, 0, 'Wendy');
CALL CylinderInsert(@In, 2, 3, 1, 'app3', 3, 2, 0, 'Wendy');
CALL CylinderInsert(@In, 3, 4, 3, 'app4', 4, 1, 0, 'Wendy');
CALL CylinderInsert(@In, 2, 1, 3, 'app5', 5, 2, 0, 'Wendy');
--update
CALL CylinderUpdate(1, 4, 1, 3, 'app1', 1, 2, 1, 'WendyC');
CALL CylinderUpdate(2, 3, 2, 4, 'app2', 2, 1, 2, 'WendyC');
--delete
CALL CylinderDelete(5);
--selectOne
CALL CylinderSelectOne(3);
--selectAll
CALL CylinderSelectAll();
--count
CALL CylinderCount();
--selectById
CALL CylinderSelectById(2);
--selectByIdMasterKeyPlan
CALL CylinderSelectByIdMasterKeyPlan(3);
--selectByIdCylinderCode
CALL CylinderSelectByIdCylinderCode(3);
--selectByIdCylinderSize
CALL CylinderSelectByIdCylinderSize(3);
