<?php
    //includes van de nodige helpers
    include ('appcode/helpers/feedback.class.php');
	include ('appcode/helpers/log.class.php');
	include ('appcode/helpers/connection.class.php');
	include ('appcode/helpers/base-bll.class.php');
	include ('appcode/helpers/base-dal.class.php');
    include ('appcode/cyl/helpers/array-sort.php');
    //includes van model cyl
    include ('appcode/cyl/model/profile-dal.class.php');
    include ('appcode/cyl/model/masterkeyplan-dal.class.php');
    include ('appcode/cyl/model/cylindersize-dal.class.php');
    //includes van membership
    include ('appcode/membership/helpers/session.class.php');
    include ('appcode/membership/model/membership-bll.class.php');
    include ('appcode/membership/model/membership-dal.class.php');
    include ('appcode/membership/helpers/password.php');
    include ('appcode/membership/model/membership.class.php');
    //een logboek maken
    //hoe werkt log? Het aanmaken van log houdt in dat er een nieuw logboek wordt aangemaakt (array).
    //Bij het aanmaken van de connectie wordt de log meegegeven en wordt daar in de constructor een 
    //feedback object aangemaakt en opgestart.
    //Wanneer de connectie wordt geopend gaat de feedback gewijzigd worden en gelogd in
    //het logboek dat hetzelfde is als hierboven omdat deze is meegegeven bij een aanmaken van de 
    //connectie.
    //Zo gebeurt dit ook bij bv profileDal
    $log = new \AnormApart\Helpers\Log();
    //connectie maken en openen
    $connection = new \AnOrmApart\Dal\Connection($log);
    $connection->open();
    //membership object aanmaken
     $membership = new \AnOrmApart\Membership($connection);
    //dal objecten aanmaken
    $profileDal = new Cyl\Profile\Dal($log);
    $mkpDal = new Cyl\MasterKeyPlan\Dal($log);
    $cylSizeDal = new Cyl\CylinderSize\Dal($log);
    //connectie aan dal meegeven en al de profielen ophalen
    $profileDal->setConnection($connection);
    $mkpDal->setConnection($connection);
    $cylSizeDal->setConnection($connection);
    //lijst van profielen ophalen alsook de log
    $listProfiles = $profileDal->selectAll();
    $logbook = $log->getBook();
    $logTextProfile = $log->getLogTextByName($logbook, 'SELECT ALL');
    $sortedListProfiles = NULL;
    if(is_array($listProfiles) && (count($listProfiles) > 0)){
        //lijst met profielen alfabetisch sorteren (cyl/helpers)
        $sortedListProfiles = sortListOnTwoColumns($listProfiles, 'Brand', 'Type');
    }
    
    //lijst van sluitplannen ophalen alsook de log
    $listMKPs = $mkpDal->selectAll();
    $logbook = $log->getBook();
    $logTextMkp = $log->getLogTextByName($logbook, 'SELECT ALL');
    $sortedListMkp = NULL;
    if(is_array($listMKPs) && (count($listMKPs) > 0)){
        //lijst met sluitplannen alfabetisch sorteren (cyl/helpers)
        $sortedListMkp = sortListOnOneColumn($listMKPs, 'Number');
    }
    //lijst van cilindermaten ophalen, geen log nodig
    $listCylSizes = $cylSizeDal->selectAll();
    $sortedListCylSizes = NULL;
    if(is_array($listCylSizes) && (count($listCylSizes) > 0)){
        //lijst met cilinders alfabetisch sorteren (cyl/helpers)
        $sortedListCylSizes = sortListOnOneColumn($listCylSizes, 'Name');
    }
   
    //testen ophalen lijsten
    //print_r($listProfiles);
    //print_r($listMKPs);
    //logboek raadplegen tijdens programmeren
    //print_r($log);
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Create Your Locksystem</title>
        <!--css links-->
        <link type="text/css" href="css/tower.css" rel="stylesheet" />
        <link type="text/css" href="css/floor.css" rel="stylesheet" />
        <link type="text/css" href="css/control-panel.css" rel="stylesheet" />
        <link type="text/css" href="css/room.css" rel="stylesheet" />
        <link type="text/css" href="css/tile.css" rel="stylesheet" />
        <link type="text/css" href="css/icon.css" rel="stylesheet" />
        <link type="text/css" href="css/popup.css" rel="stylesheet" />
        <link type="text/css" href="css/footer.css" rel="stylesheet" />
        <link type="text/css" href="css/cyl.css" rel="stylesheet" />
        <!--js scripts en links-->
        <script type="text/javascript" src="js/ajax.js"></script>
        <script type="text/javascript" src="js/cyl.js"></script>
        <script>
            window.addEventListener("hashchange", changeSessionState, false);
            window.onload = function () {
                //gebruik van sessionStorage om de juiste floor te laden bij het load of refresh.
                //Zo wordt de floor geladen van waaruit de refresh is gedaan
                var loaded = sessionStorage.getItem('loaded');
                if (loaded == null || loaded == 'null') {
                    location.href = '#first-floor';
                }
                else {
                    location.href = loaded;
                }
                init();
            }
        </script>
    </head>
    <body id="body">
        <!--HIDDEN ANCHORS AND POPUPS-->
        <a class="hiddenanchor" id="to-profile-input"></a>
        <a class="hiddenanchor" id="to-login"></a>
        <?php 
        if ($membership->isInRole('Administrator'))
        { ?>
            <a class="hiddenanchor" id="to-register"></a>
            <a class="hiddenanchor" id="to-users"></a>
            <?php 
            include('appcode/membership/view/register-view.php');
            include('appcode/membership/view/users-view.php');
        } ?>

        <?php 
        include('appcode/membership/view/login-view.php');
        include('appcode/cyl/view/popup_profile-input.php');
        ?>
        <!--TOWER-->
        <div id="tower">
            <?php 
            include('appcode/cyl/view/user-icon.php'); 
            if (!$membership->loginCheck())
            { 
                include('appcode/cyl/view/startpage.php');
            }
            else
            {
                include('appcode/cyl/view/f1_homepage.php');
                include('appcode/cyl/view/f2_home-masterkeyplans.php');
                include('appcode/cyl/view/f3_profiles.php');
                include('appcode/cyl/view/f4_masterkeyplans.php');
                include('appcode/cyl/view/f5_mkp-detail.php');
                include('appcode/cyl/view/f6_mkp-input.php');
                include('appcode/cyl/view/f7_cylinders.php');
                include('appcode/cyl/view/f8_keys.php');
            }
            ?>
        </div>
        <!--FOOTER-->
        <footer>
            <div>
                <p>Copyright @ 2015 <a href="http://www.sleutelhuisje.be">'t Sleutelhuisje BVBA</a></p>
            </div>
        </footer>
    </body>
</html>
