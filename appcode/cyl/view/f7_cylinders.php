<!--CILINDERs-->
<div class="floor" id="seventh-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <!--terugknop afhankelijk van raadplegen of invoeren-->
        <a id="backLinkCylinders" class="tile _1x14x1 nozoom" href="#sixth-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Cilinder</h2>
    </div>
    <div class="room olive-green">
        <div class="tile _1x1x1 nozoom stacked">
            <div id="mkpVeilCylinders" class="popup"></div>
            <div class="tile _2x3x1 nozoom">
                <div class="tile _1x2x1 nozoom stacked-page">
                    <h3>Basisgegevens</h3>
                    <div id="mkpCylBasics" class= "inputsCylKey scrollable">
                            <div class="inputWithError">
                                <label for="">Indexnummer:</label>
                                <input type="text" id="cyl-input-index" />
                                <label class="error" id="cyl-error-index"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Cilindernaam:</label>
                                <input type="text" id="cyl-input-name" />
                                <label class="error" id="cyl-error-name"></label>
                            </div>
                            <div>
                                <label for="">Cilindermaat:</label>
                                <select id="cyl-input-size">
                                    <?php
                                    if($sortedListCylSizes != NULL){
                                        //voor elke rij een option element aanmaken
                                        foreach($sortedListCylSizes as $row){ ?>
                                            <option value="<?php echo $row['Id']?>">
                                                <?php echo $row['Name']; ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select> 
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal bij bestelling:</label>
                                <input type="text" id="cyl-input-initialQuantity" />
                                <label class="error" id="cyl-error-initialQuantity"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal backorder:</label>
                                <input type="text" id="cyl-input-backorder"/>
                                <label class="error" id="cyl-error-backorder"></label>
                            </div>
                        </div>
                    <div class="buttonsMkpAction">
                    </div>
                </div>
                <div class="tile _1x2x1 nozoom stacked-page">
                    <h3>Actieve Code</h3>
                    <div class= "inputsCylKey scrollable">
                        <div class="inputWithError">
                            <label for="">Basiscode:</label>
                            <input type="text" id="cyl-input-activeBase"/>
                            <label class="error" id="cyl-error-activeBase"></label>
                        </div>
                        <div class="inputWithError">
                            <label for="">Ophoging 1:</label>
                            <input type="text" id="cyl-input-activeAdd1" />
                            <label class="error" id="cyl-error-activeAdd1"></label>
                        </div>
                        <div class="inputWithError">
                            <label for="">Ophoging 2:</label>
                            <input type="text" id="cyl-input-activeAdd2" />
                            <label class="error" id="cyl-error-activeAdd2"></label>
                        </div>
                        <div class="inputWithError">
                            <label for="">Ophoging 3:</label>
                            <input type="text" id="cyl-input-activeAdd3"/>
                            <label class="error" id="cyl-error-activeAdd3"></label>
                        </div>
                    </div> 
                    <div class="buttonsMkpAction">
                                    
                    </div>
                </div>
            </div>
            <div class="tile _1x3x1 nozoom stacked-page">
                <h3>Passieve code</h3>
                <div class= "half-page inputsCylKey scrollable">
                    <div class="inputWithError">
                        <label for="">Passieve code:</label>
                        <input type="text" id="cyl-input-passive" />
                        <label class="error" id="cyl-error-passive"></label>
                    </div>
                </div>
                <h3>Laterale code</h3>
                <div class="half-page inputsCylKey scrollable">
                    <div class="inputWithError">
                        <label for="">Laterale code:</label>
                        <input type="text" id="cyl-input-lateral"/>
                        <label class="error" id="cyl-error-lateral"></label>
                    </div>
                </div>
                <div class="buttonsMkpAction">
                    <button id="inputCylCancel"><span class="icon" data-icon="v"></span></button>
                    <button id="inputCylAction"><span class="icon" data-icon="#"></span></button>
                </div>
            </div>
        </div>
    </div>
</div>
