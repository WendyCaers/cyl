<!--INVOER SLUITPLAN-->
<div class="floor" id="sixth-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <a class="tile _1x14x1 nozoom" href="#fourth-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Sluitplan</h2>
    </div>
    <div class="room olive-green">
        <div class="tile _1x1x1 nozoom stacked">
            <div class="tile _2x3x1 nozoom">
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div id="mkpInputVeil1" class="popup"></div>
                    <h3>Basisgegevens</h3>
                    <div>
                        <div id="MkpInputBasics" class="MkpBasics scrollable">
                            <div class="inputWithError">
                                <label for="">Sluitplan:</label>
                                <input id="mkp-input-number" type="text" />
                                <label class="error" id="mkp-error-number"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Datum:</label>
                                <input id="mkp-input-date" type="text" />
                                <label class="error" id="mkp-error-date"></label>
                            </div>
                            <div>
                                <label for="">Profiel:</label>
                                <select id="mkp-input-profile">
                                    <?php
                                    if($sortedListProfiles != NULL){
                                        //voor elke rij een option element aanmaken
                                        foreach($sortedListProfiles as $row){ ?>
                                            <option value="<?php echo $row['Id']?>">
                                                <?php echo $row['Brand'] .' '. $row['Type']; ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div>
                                <label for="">Factuurnummer:</label>
                                <input id="mkp-input-invoice" type="text" />
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal cilinders:</label>
                                <input id="mkp-input-numberOfCyls" type="text" />
                                <label class="error" id="mkp-error-numberOfCyls"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal sleutels:</label>
                                <input id="mkp-input-numberOfKeys" type="text" />
                                <label class="error" id="mkp-error-numberOfKeys"></label>
                            </div>
                            <div>
                                <label for="">Omschrijving:</label>
                                <textarea id="mkp-input-description" class="labelShow" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="buttonsMkpAction">
                        <button id="mkpBasicsCancel"><span class="icon" data-icon="v"></span></button>
                        <button id="mkpBasicsNext"><span class="icon" data-icon="u"></span></button>
                        <button id="mkpBasicsAction"><span class="icon" data-icon="#"></span></button>
                    </div>
                </div>
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div id="mkpInputVeil2" class="popup"></div>
                    <h3>Cilinders</h3>
                    <div>
                        <div class="buttonsMkpInput">
                            <button id="mkpCylNew" value="cylinderInsert">
                                <span class="icon" data-icon="@"></span>
                            </button>
                            <button id="mkpCylChange" value="cylinderUpdate">
                                <span class="icon" data-icon="q"></span>
                            </button>
                            <button id="mkpCylRemove" value="cylinderDelete">
                                <span class="icon" data-icon="x"></span>
                            </button>
                        </div>
                        <div id="containerListCyl" class="scrollable">
                            <ul id="listCyl">
                            </ul>
                        </div>
                    </div> 
                    <div class="buttonsMkpAction">
                        <button id="mkpCylCancel"><span class="icon" data-icon="v"></span></button>
                        <button id="mkpCylNext"><span class="icon" data-icon="u"></span></button>
                        <button id="mkpCylAction"><span class="icon" data-icon="#"></span></button>
                    </div>
                </div>
            </div>
            <div class="tile _1x3x1 nozoom stacked-page">
                <div id="mkpInputVeil3" class="popup"></div>
                <h3>Sleutels</h3>
                <div>
                    <div class="buttonsMkpInput">
                        <button id="mkpKeyNew" value="keyInsert">
                            <span class="icon" data-icon="@"></span>
                        </button>
                        <button id="mkpKeyChange" value="keyUpdate">
                            <span class="icon" data-icon="q"></span>
                        </button>
                        <button id="mkpKeyRemove" value="keyDelete">
                            <span class="icon" data-icon="x"></span>
                        </button>
                    </div>
                    <div id="containerListKey" class="scrollable">
                        <ul id="listKey">
                        </ul>
                    </div>
                </div>
                <div class="buttonsMkpAction">
                    <button id="mkpKeyCancel"><span class="icon" data-icon="v"></span></button>
                    <button id="mkpKeyNext"><span class="icon" data-icon="u"></span></button>
                    <button id="mkpKeyAction"><span class="icon" data-icon="#"></span></button>
                </div>
            </div>
        </div>
    </div>
</div>