<!--SLEUTELS-->
<div class="floor" id="eighth-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <!--terugknop is afhankelijk van raadplegen of invoeren-->
        <a id="backLinkKeys" class="tile _1x14x1 nozoom" href="#sixth-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Sleutel</h2>
    </div>
    <div class="room olive-green">
        <div class="tile _1x1x1 nozoom stacked">
            <div id="mkpVeilKeys" class="popup"></div>
            <div class="tile _2x3x1 nozoom">
                <div class="tile _1x2x1 nozoom stacked-page">
                    <h3>Basisgegevens</h3>
                    <div>
                        <div id="mkpKeyBasics" class= "inputsCylKey scrollable">
                            <div class="inputWithError">
                                <label for="">Indexnummer:</label>
                                <input type="text" id="key-input-index" />
                                <label class="error" id="key-error-index"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Sleutelnaam:</label>
                                <input type="text" id="key-input-name"/>
                                <label class="error" id="key-error-name"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal bij bestelling:</label>
                                <input type="text" id="key-input-initialQuantity"/>
                                <label class="error" id="key-error-initialQuantity"></label>
                            </div>
                            <div class="inputWithError">
                                <label for="">Aantal backorder:</label>
                                <input type="text" id="key-input-backorder" />
                                <label class="error" id="key-error-backorder"></label>
                            </div>
                        </div>
                    </div>
                    <div class="buttonsMkpAction">
                    </div>
                </div>
                <div class="tile _1x2x1 nozoom stacked-page">
                    <h3>Code</h3>
                    <div class= "inputsCylKey scrollable">
                        <div class="inputWithError">
                            <label for="">Actieve code:</label>
                            <input type="text" id="key-input-active" />
                            <label class="error" id="key-error-active"></label>
                        </div>
                        <div class="inputWithError">
                            <label for="">Passieve code:</label>
                            <input type="text" id="key-input-passive" />
                            <label class="error" id="key-error-passive"></label>
                        </div>
                        <div class="inputWithError">
                            <label for="">Laterale code:</label>
                            <input type="text" id="key-input-lateral" />
                            <label class="error" id="key-error-lateral"></label>
                        </div>
                    </div> 
                    <div class="buttonsMkpAction">
                    </div>
                </div>
            </div>
            <div class="tile _1x3x1 nozoom stacked-page">
                <h3>Cilinders</h3>
                <div class="inputsCylKey">
                    <div id="selectCylsOnKey" class="inputWithError">
                        <button id="cylOnKeyDelete"><span class="icon" data-icon="x"></span></button>
                        <select id="key-input-cyls">
                                        
                        </select>
                        <label class="error" id="key-error-cyls"></label>
                    </div>
                    <div id="containerCylsOnKey" class="scrollable">
                        <ul id="listCylsOnKey">
                                        
                        </ul>
                    </div>
                </div>
                <div class="buttonsMkpAction">
                    <button id="inputKeyCancel"><span class="icon" data-icon="v"></span></button>
                    <button id="inputKeyAction"><span class="icon" data-icon="#"></span></button>
                </div>
            </div>
        </div>
    </div>
</div>
