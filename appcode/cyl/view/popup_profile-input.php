<!--POPUP VOOR INVOER PROFIEL-->
<div id="profile-input" class="popup">
    <div class="popup-header _1x1x5">
        <a class="close-popup" id="close-popup-profile" href="#third-floor">
            <span class="icon" data-icon="9"></span>
            <span class="screen-reader-text">Back</span>
        </a> <!--sluiten-->
        <h1>Profiel</h1>
    </div>
    <div class="popup-body _4x1x5">
        <div class="_1x2x1">
            <input type="file" id="imageUpload" name="imageUpload" accept="image/*" />
                <img src="images/picture.png" alt="picture" id="image-profile-popup"/>
                <canvas id="canvas-profile-popup"></canvas><br/>
                <button class="image-button" id="scaleImageFit">-</button>
                <button class="image-button" id="scaleImageFill">+</button>
                <button class="image-button" id="imageRemove">Verwijderen</button>
                            
        </div>
        <div class="_1x2x1">
            <div class="_1x1x5">
                <label class="error" id="profile-brand-error"></label>
                <label for="brand">Merk: </label>
                <input type="text" id="profile-brand" name="profile-brand" />
                            
            </div>
            <div class="_1x1x5">
                <label class="error" id="profile-type-error"></label>
                <label for="type">Type: </label>
                <input type="text" id="profile-type" name="profile-type" />
                            
            </div>
            <div class="_3x1x5">
                <button id="profileCancel"><span class="icon" data-icon="v"></span></button>
                <button id="profileAction" ><span class="icon" data-icon="#"></span></button>
            </div>
        </div>
    </div>
</div>
