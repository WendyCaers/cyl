<!--PROFIELEN-->
<div class="floor" id="third-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <a id="backLinkProfiles" class="tile _1x14x1 nozoom" href="#second-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Profielen</h2>
    </div>
    <div class="room copper-rust">
        <div class="tile _1x1x1 nozoom stacked">
            <div class="tile _2x3x1 nozoom">
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div class="scrollable">
                        <ul id="listProfiles">
                            <?php 
                                if($sortedListProfiles != NULL){
                                    //voor elke rij een li element aanmaken
                                    foreach($sortedListProfiles as $row){ ?>
                                        <li value="<?php echo $row['Id']?>" id="<?php echo $row['Img']?>">
                                            <?php echo $row['Brand'] .' '. $row['Type']; ?>
                                        </li>
                                    <?php
                                    }
                                } 
                                else { ?> 
                                    <li>
                                        Er kunnen geen profielen worden weergegeven!!  
                                        <?php echo $logTextProfile;?> 
                                    </li> 
                            <?php 
                                }
                                ?>       
                        </ul>
                    </div>
                </div>
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div>
                        <span id="icon-profile" class="icon stacked-picture" data-icon="h">
                            <img id="image-profile" src="#" alt="profile" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="tile _1x3x1 nozoom stacked-page">
                <div>
                    <div class="button-x3">
                        <button id="profileNew" value="profileInsert">Nieuw</button>
                    </div>
                    <div class="button-x3">
                        <button id="profileChange" value="profileUpdate">Wijzigen</button>
                    </div>
                    <div class="button-x3">
                        <button id="profileRemove" value="profileDelete">Verwijderen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
