<!--HOMEPAGINA-->
 <div class="floor" id="first-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
    </div>
    <div class="room">
        <div class="tile _1x2x1">
            <a href="#first-floor" class="mask">
                <h2>Cilindermontage</h2>
                <p>Hulp bij het monteren van uw veiligheidscilinders: ophalen codes, 
                    cilinderonderdelen samenstellen, bestifting bepalen en de nodige know-how
                    om dit alles te realiseren.</p>
                <span class="action">Start</span>
            </a>
            <h1>Cilindermontage</h1>
            <span class="icon" data-icon="e"></span>
        </div>
        <div class="tile _3x2x5">
            <a href="#second-floor" class="mask">
                <h2>Sluitplanbeheer</h2>
                <p>Beheer al uw sluitplannen in alle mogelijke profielen.</p>
                <span class="action">Start</span>
            </a>
            <h1>Sluitplanbeheer</h1>
            <span class="icon" data-icon="f"></span>
        </div>
        <div class="tile _2x2x5">
            <a href="#first-floor" class="mask">
                <h2>About CYL</h2>
                <span class="action">Start</span>
            </a>
            <h1>About</h1>
            <span class="icon" data-icon="0"></span>
        </div>
    </div>
</div>
