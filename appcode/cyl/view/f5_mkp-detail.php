<!--SLUITPLAN IN DETAIL-->
<div class="floor" id="fifth-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <a class="tile _1x14x1 nozoom" href="#fourth-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Sluitplan in detail</h2>
    </div>
    <div class="room olive-green">
        <div class="tile _1x1x1 nozoom stacked">
            <div class="tile _1x1x1 nozoom">
                    <div class="tile _1x3x1 nozoom stacked-page">
                    <div>
                        <div id="MkpDetailsBasics" class="MkpBasics scrollable">
                            <div>
                                <label for="">Sluitplan:</label>
                                <label id="mkp-consult-number" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Datum:</label>
                                <label id="mkp-consult-date" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Profiel:</label>
                                <label id="mkp-consult-profile" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Factuurnummer:</label>
                                <label id="mkp-consult-invoice" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Aantal cilinders:</label>
                                <label id="mkp-consult-numberOfCyls" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Aantal sleutels:</label>
                                <label id="mkp-consult-numberOfKeys" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Omschrijving:</label>
                                <textarea id="mkp-consult-description" class="labelShow" rows="6" disabled>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tile _2x3x1 nozoom stacked-page scrollable">
                    <table id="mkpTable">
                    </table>
                              
                </div>
            </div>
        </div>
    </div>
</div>
