<!--HOME SLUITPLANBEHEER-->
<div class="floor" id="second-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
    </div>
    <div class="room">
        <div class="tile _2x3x1">
            <a href="#fourth-floor" class="mask">
                <h2>Sluitplannen</h2>
                <p>Uw sluitplannen van A tot Z</p>
                <span class="action">Start</span>
            </a>
            <h1>Sluitplannen</h1>
            <span class="icon" data-icon="g"></span>
        </div>
        <div class="tile _1x3x1">
            <a href="#third-floor" class="mask">
                <h2>Profielen</h2>
                <p>Beheer de gebruikte cilinderprofielen binnen uw sluitplannen.</p>
                <span class="action">Start</span>
            </a>
            <h1>Profielen</h1>
            <span class="icon" data-icon="h"></span>
        </div>
    </div>
</div>
