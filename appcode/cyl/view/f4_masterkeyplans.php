<!--SLUITPLANNEN-->
<div class="floor" id="fourth-floor">
    <div class="control-panel">
        <a class="tile _1x14x1 nozoom" href="#first-floor">
            <span class="icon" data-icon="2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <a class="tile _1x14x1 nozoom" href="#second-floor">
            <span class="icon" data-icon="}"></span>
            <span class="screen-reader-text">Previous</span>
        </a>
        <h1><span class="firstletter">C</span>reate 
            <span class="firstletter">Y</span>our 
            <span class="firstletter">L</span>ocksystem
        </h1>
        <h2>Sluitplannen</h2>
    </div>
    <div class="room olive-green">
        <div class="tile _1x1x1 nozoom stacked">
            <div class="tile _2x3x1 nozoom">
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div>
                        <div class="_1x1x5">
                            <input type="text" id="searchMkp" placeholder="Zoeken"/>
                        </div>
                        <div class=" _4x1x5 scrollable">
                            <ul id="listMkp">
                                <?php
                                if($listMKPs != NULL){
                                    //voor elke rij een li element aanmaken
                                    foreach($sortedListMkp as $row){ ?>
                                        <li value="<?php echo $row['Id']?>">
                                            <?php echo $row['Number'] ?>
                                        </li>
                                    <?php
                                    }
                                } 
                                else { ?> 
                                    <li>
                                        Er kunnen geen sluitplannen worden weergegeven!!  
                                        <?php echo $logTextMkp?> 
                                    </li> 
                            <?php 
                                }
                                ?>       
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tile _1x2x1 nozoom stacked-page">
                    <div>
                        <!--2 views: 1 voor wanneer er geen sluitpan is geselecteerd en 1 voor wanneer er wel 
                        één is geselecteerd.-->
                        <span id="icon-mkp" class="icon stacked-picture" data-icon="f"></span>
                        <div id="MkpStartBasics" class="MkpBasics scrollable">
                            <div>
                                <label for="">Sluitplan:</label>
                                <label id="mkpNumber" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Datum:</label>
                                <label id="mkpDate" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Profiel:</label>
                                <label id="mkpProfile" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Factuurnummer:</label>
                                <label id="mkpInvoice" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Aantal cilinders:</label>
                                <label id="mkpNumberOfCyls" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Aantal sleutels:</label>
                                <label id="mkpNumberOfKeys" class="labelShow"></label>
                            </div>
                            <div>
                                <label for="">Omschrijving:</label>
                                <textarea id="mkpDescription" class="labelShow" rows="6" disabled></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tile _1x3x1 nozoom stacked-page">
                <div>
                    <div class="button-x4">
                        <button id="mkpConsult">Raadplegen</button>
                    </div>
                    <div class="button-x4">
                        <button id="mkpNew" value="masterKeyPlanInsert">Nieuw</button>
                    </div>
                    <div class="button-x4">
                        <button id="mkpChange" value="masterKeyPlanUpdate">Wijzigen</button>
                    </div>
                    <div class="button-x4">
                        <button id="mkpRemove" value="masterKeyPlanDelete">Verwijderen</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
