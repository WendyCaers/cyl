<!--USER-ICON-->
<span id="userIcon" class="icon" data-icon=")">
    <span class="tooltip">
        <a id="loginLink" href="<?php echo $membership->getLoginHyperlink();?>"><?php echo $membership->getLoginText();?></a>
        <?php 
        if ($membership->isInRole('Administrator')){ ?>
            <a id="usersLink" href="#to-users">gebruikers</a>
        <?php 
        } ?>
    </span>
</span>
