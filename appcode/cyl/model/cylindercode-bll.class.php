<?php
namespace Cyl\CylinderCode;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $passive;
	protected $lateral;
	protected $active;
	protected $add1;
	protected $add2;
	protected $add3;

	public function getId()
	{
		return $this->id;
	}

	public function getPassive()
	{
		return $this->passive;
	}

	public function getLateral()
	{
		return $this->lateral;
	}

	public function getActive()
	{
		return $this->active;
	}

	public function getAdd1()
	{
		return $this->add1;
	}

	public function getAdd2()
	{
		return $this->add2;
	}

	public function getAdd3()
	{
		return $this->add3;
	}

	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('id');
			$this->feedback->setText('id moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setPassive($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('passive');
			$this->feedback->setText('passive is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('passive');
			$this->feedback->setText('passive moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->passive = $value;
		}
	}

	public function setLateral($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('lateral');
			$this->feedback->setText('lateral is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('lateral');
			$this->feedback->setText('lateral moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->lateral = $value;
		}
	}

	public function setActive($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('active');
			$this->feedback->setText('active is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('active');
			$this->feedback->setText('active moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->active = $value;
		}
	}

	public function setAdd1($value)
	{
        $value = strip_tags($value);
        if(strlen($value) > 0)
        {
            if(!is_numeric($value))
            {
                $this->feedback->start('add1');
			    $this->feedback->setText('add1 moet een numerieke waarde zijn');
			    $this->feedback->setIsError(TRUE);
			    $this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			    $this->feedback->end();
			    $this->log();
            }
            if (!$this->feedback->getIsError())
            {
                $this->add1 = $value;
            }
        }
        else
        {
            $this->add1 = -1;
        }
	}

	public function setAdd2($value)
	{
        $value = strip_tags($value);
		if(strlen($value) > 0)
        {
            if(!is_numeric($value))
            {
                $this->feedback->start('add2');
			    $this->feedback->setText('add2 moet een numerieke waarde zijn');
			    $this->feedback->setIsError(TRUE);
			    $this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			    $this->feedback->end();
			    $this->log();
            }
            if (!$this->feedback->getIsError())
            {
                $this->add2 = $value;
            }
        }
        else
        {
            $this->add2 = -1;
        }
	}

	public function setAdd3($value)
	{
        $value = strip_tags($value);
		if(strlen($value) > 0)
        {
            if(!is_numeric($value))
            {
                $this->feedback->start('add3');
			    $this->feedback->setText('add3 moet een numerieke waarde zijn');
			    $this->feedback->setIsError(TRUE);
			    $this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			    $this->feedback->end();
			    $this->log();
            }
            if (!$this->feedback->getIsError())
            {
                $this->add3 = $value;
            }
        }
        else
        {
            $this->add3 = -1;
        }
	}
}

?>