<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	// include connection class
	include ('../../helpers/connection.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('../../helpers/base-dal.class.php');
	// include bll class for table
	include ('cylinderkey-bll.class.php');
	// include dal class for table
	include ('cylinderkey-dal.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect
	$connection = new AnOrmApart\Dal\Connection($log);
	// create an instance of the DAL class for this table
	$dal = new Cyl\CylinderKey\Dal($log);
    $dal->setConnection($connection);
	// create an instance of the Bll class for this table
	$bll = new Cyl\CylinderKey\Bll($log);

	// we start with making a business object
	$bll->setId(999);
	$bll->setIdCylinder(1);
	$bll->setIdKey(1);

    //open connection
    $connection->open();

	// INSERT
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// and now it's time to insert
	            //$dal->insert();

	// UPDATE
	// first change bdo
    // set id to number id from row inserted
    $bll->setId(6);
	$bll->setIdCylinder(2);
	$bll->setIdKey(2);
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	        //$dal->update();

    //DELETE
    // delete row with Id set in bll object
	        //$dal->delete();

    //SELECTALL
    $list = $dal->selectAll();

    //SELECTBYID
    //outcome returns a profile
    $bll->setId(2);
    $selectById = $dal->selectById();

    //SELECTBYIDCYLINDER
    $bll->setIdCylinder(3);
    $selectByIdCylinder = $dal->selectByIdCylinder();

    //SELECTBYIDKEY
    $bll->setIdKey(4);
    $selectByIdKey = $dal->selectByIdKey();

    //SELECTONE
    //outcome returns true or false
    $bll->setId(1);
    $dal->selectOne();

	// disconnect
	$connection->close();


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test DAL CylinderKey</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>CylinderKey BLL</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdCylinder</label>
				<span><?php echo $bll->getIdCylinder();?></span>
			</div>
			<div>
				<label>IdKey</label>
				<span><?php echo $bll->getIdKey();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>CylinderKey DAL</caption>
			<tr>
                <th>Id</th>
                <th>IdCilinder</th>
                <th>IdSleutel</th>
                <th>Cilindernaam</th>
                <th>Cilinderindex</th>
                <th>Sleutelnaam</th>
                <th>Sleutelindex</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdCylinder'];?></td>
                <td><?php echo $row['IdKey'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectOne</th>
            </tr>
            
            <tr>
                <td><?php echo $bll->getId();?></td>
                <td><?php echo $bll->getIdCylinder();?></td>
                <td><?php echo $bll->getIdKey();?></td>
            </tr>
            <tr>
                <th>SelectById</th>
            </tr>
            <tr>
                <td><?php echo $selectById[0]['Id'];?></td>
                <td><?php echo $selectById[0]['IdCylinder'];?></td>
                <td><?php echo $selectById[0]['IdKey'];?></td>
                <td><?php echo $selectById[0]['CylinderName'];?></td>
                <td><?php echo $selectById[0]['CylinderIndex'];?></td>
                <td><?php echo $selectById[0]['KeyName'];?></td>
                <td><?php echo $selectById[0]['KeyIndex'];?></td>
            </tr>
            <tr>
                <th>SelectByIdCylinder</th>
            </tr>
            <?php
			foreach ($selectByIdCylinder as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdCylinder'];?></td>
                <td><?php echo $row['IdKey'];?></td>
                <td><?php echo $row['CylinderName'];?></td>
				<td><?php echo $row['CylinderIndex'];?></td>
                <td><?php echo $row['KeyName'];?></td>
				<td><?php echo $row['KeyIndex'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByIdKey</th>
            </tr>
            <?php
			foreach ($selectByIdKey as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdCylinder'];?></td>
                <td><?php echo $row['IdKey'];?></td>
                <td><?php echo $row['CylinderName'];?></td>
				<td><?php echo $row['CylinderIndex'];?></td>
                <td><?php echo $row['KeyName'];?></td>
				<td><?php echo $row['KeyIndex'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>


