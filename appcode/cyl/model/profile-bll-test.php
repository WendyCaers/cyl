<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('profile-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new Cyl\Profile\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d H:i:s");
	$bll->setId(999);
	$bll->setBrand('Brand');
	$bll->setType('Type');
    $bll->setImg('Img.png');
	$logbook = $log->getBook();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL Profile</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Profile</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Merk</label>
				<span><?php echo $bll->getBrand();?></span>
			</div>
			<div>
				<label>Type</label>
				<span><?php echo $bll->getType();?></span>
			</div>
            <div>
				<label>Image</label>
				<span><?php echo $bll->getImg();?></span>
			</div>
		</fieldset>
	</body>
</html>


