<?php
namespace Cyl\CylinderKey;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderKeyInsert(@pId, :pIdCylinder, :pIdKey, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdCylinder', $this->bdo->getIdCylinder(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdKey', $this->bdo->getIdKey(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderKeyInsert(). 
                        Rij is niet toegevoegd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeyInsert');
				$this->feedback->setContext('CylinderKey');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderKeyUpdate(:pId, :pIdCylinder, :pIdKey, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdCylinder', $this->bdo->getIdCylinder(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdKey', $this->bdo->getIdKey(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					if ($this->rowCount > 0)
					{
						// De rij met de opgegeven Id is geüpdated.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen groter dan 0 is.
						$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                            '</b> is gewijzigd.');
						$result = TRUE;
					}
					else
					{
						// De rij met de opgegeven Id bestaat niet.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen gelijk is aan 0.
						$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                            '</b> bestaat niet en is niet gewijzigd.');
						$result = FALSE;
					}
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderKeyUpdte(). 
                        Rij is niet gewijzigd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeyUpdate');
				$this->feedback->setContext('CylinderKey');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}
    
	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderKeyDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					if ($this->rowCount > 0)
					{
						// De rij met de opgegeven Id is gedeleted.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen groter dan 0 is.
						$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                            '</b> is verwijderd.');
						$result = TRUE;
					}
					else
					{
						// De rij met de opgegeven Id bestaat niet.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen gelijk is aan 0.
						$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                            '</b> bestaat niet en is dus niet gewijzigd.');
						$result = FALSE;
					}
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderKeyDelete(). 
                        Rij is niet gedeleted.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeyDelete');
				$this->feedback->setContext('CylinderKey');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
		$this->feedback->setContext('CylinderKey');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderKeySelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setIdCylinder($array['IdCylinder']);
					$this->bdo->setIdKey($array['IdKey']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is niet gevonden.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeySelectOne');
				$this->feedback->setContext('CylinderKey');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL CylinderKeySelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText("Tabel CylinderKey {$this->rowCount} rijen ingelezen.");
				}
				else
				{
					$this->feedback->setText('Tabel CylinderKey is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeySelectAll');
				$this->feedback->setContext('CylinderKey');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('CylinderKeySelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderKeySelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel CylinderKey gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderKey is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeySelectById');
				$this->feedback->setContext('CylinderKey');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function selectByIdCylinder()
	{
		$this->feedback->startTimeInKey('CylinderKeySelectByIdCylinder');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderKeySelectByIdCylinder(:pIdCylinder)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdCylinder', $this->bdo->getIdCylinder(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdCylinder()} in tabel CylinderKey gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderKey is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeySelectByIdCylinder');
				$this->feedback->setContext('CylinderKey');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDCYLINDER CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

	public function selectByIdKey()
	{
		$this->feedback->startTimeInKey('CylinderKeySelectByIdKey');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderKeySelectByIdKey(:pIdKey)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdKey', $this->bdo->getIdKey(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdKey()} in tabel CylinderKey gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderKey is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderKey');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderKeySelectByIdKey');
				$this->feedback->setContext('CylinderKey');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderKey');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDKEY CylinderKey');
			$this->feedback->setContext('CylinderKey');
		}
		$this->log();
		return $result;
	}

}

?>

