<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	// include connection class
	include ('../../helpers/connection.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('../../helpers/base-dal.class.php');
	// include bll class for table
	include ('profile-bll.class.php');
	// include dal class for table
	include ('profile-dal.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect
	$connection = new AnOrmApart\Dal\Connection($log);
	// create an instance of the DAL class for this table
	$dal = new Cyl\Profile\Dal($log);
    $dal->setConnection($connection);
	// create an instance of the Bll class for this table
	$bll = new Cyl\Profile\Bll($log);

	// we start with making a business object
	$bll->setId(999);
	$bll->setBrand('TestImgDal');
	$bll->setType('Img');
    $bll->setInsertedBy('WendyViaDal');
    //image
    $bll->setImg('../../../images/bg1.png');

    //open connection
    $connection->open();

	// INSERT
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// and now it's time to insert
	            //$dal->insert();

	// UPDATE
	// first change bdo
    // set id to number id from row inserted
    $bll->setId(9);
	$bll->setBrand('BrandUpdate');
	$bll->setType('TypeUpdate');
    $bll->setUpdatedBy('WendyViaDal');
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	        //$dal->update();

    //DELETE
    // delete row with Id set in bll object
	        //$dal->delete();

    //SELECTALL
    $list = $dal->selectAll();

    //SELECTBYID
    //outcome returns a profile
    $bll->setId(2);
    $selectById = $dal->selectById();
    
    //SELECTBYBRAND
    $bll->setBrand('Iseo');
    $selectByBrand = $dal->selectByBrand();

    //SELECTLIKEBRAND
    $bll->setBrand('Ab');
    $selectLikeBrand = $dal->selectLikeBrand();

    //SELECTLIKEXBRAND
    $bll->setBrand('s');
    $selectLikeXBrand = $dal->selectLikeXBrand();

    //SELECTONE
    //outcome returns true or false
    $bll->setId(1);
    $dal->selectOne();

	// disconnect
	$connection->close();

	$logbook = $log->getBook();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test DAL Profile</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Profile BLL</legend>
			<div>
				<label>ID</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Merk</label>
				<span><?php echo $bll->getBrand();?></span>
			</div>
			<div>
				<label>Type</label>
				<span><?php echo $bll->getType();?></span>
			</div>
            <div>
				<label>Img</label>
				<span><?php echo $bll->getImg();?></span>
			</div>
		</fieldset>
        <br/>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>Profile DAL</caption>
			<tr>
                <th>ID</th>
				<th>Merk</th>
                <th>Type</th>
                <th>Image</th>
                
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['Brand'];?></td>
                <td><?php echo $row['Type'];?></td>
                <td><?php echo $row['Img'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectOne</th>
            </tr>
            
            <tr>
                <td><?php echo $bll->getId();?></td>
                <td><?php echo $bll->getBrand();?></td>
                <td><?php echo $bll->getType();?></td>
                <td><?php echo $bll->getImg();?></td>
            </tr>
            <tr>
                <th>SelectById</th>
            </tr>
            
            <tr>
                <td><?php echo $selectById[0]['Id'];?></td>
                <td><?php echo $selectById[0]['Brand'];?></td>
                <td><?php echo $selectById[0]['Type'];?></td>
                <td><?php echo $selectById[0]['Img'];?></td>
            </tr>
            <tr>
                <th>SelectByBrand</th>
            </tr>
            <?php
			foreach ($selectByBrand as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['Brand'];?></td>
                <td><?php echo $row['Type'];?></td>
                <td><?php echo $row['Img'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeBrand</th>
            </tr>
            <?php
			foreach ($selectLikeBrand as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['Brand'];?></td>
                <td><?php echo $row['Type'];?></td>
                <td><?php echo $row['Img'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeXBrand</th>
            </tr>
            <?php
			foreach ($selectLikeXBrand as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['Brand'];?></td>
                <td><?php echo $row['Type'];?></td>
                <td><?php echo $row['Img'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>


