<?php
namespace Cyl\CylinderType;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		//if (!$this->bdo->isValid())
        if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderTypeInsert(@pId, :pName, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderTypeInsert(). 
                        Rij is niet toegevoegd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeInsert');
				$this->feedback->setContext('CylinderType');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if ($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderTypeUpdate(:pId, :pName, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderTypeUpdate(). 
                        Rij is niet gewijzigd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeUpdate');
				$this->feedback->setContext('CylinderType');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL CylinderTypeDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderTypeDelete(). 
                        Rij is niet verwijderd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeDelete');
				$this->feedback->setContext('CylinderType');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
		$this->feedback->setContext('CylinderType');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderTypeSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setName($array['Name']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is niet gevonden.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeSelectOne');
				$this->feedback->setContext('CylinderType');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL CylinderTypeSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText("Tabel CylinderType {$this->rowCount} rijen ingelezen.");
				}
				else
				{
					$this->feedback->setText('Tabel CylinderType is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeSelectAll');
				$this->feedback->setContext('CylinderType');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('CylinderTypeSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderTypeSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel CylinderType gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderType is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeSelectById');
				$this->feedback->setContext('CylinderType');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function selectByName()
	{
		$this->feedback->startTimeInKey('CylinderTypeSelectByName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderTypeSelectByName(:pName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getName()} in tabel CylinderType gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderType is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeSelectByName');
				$this->feedback->setContext('CylinderType');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYNAME CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

	public function selectLikeName()
	{
		$this->feedback->startTimeInKey('CylinderTypeSelectLikeName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderTypeSelectLikeName(:pName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getName()} in tabel CylinderType gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel CylinderType is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('CylinderType');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderTypeSelectSelectLikeName');
				$this->feedback->setContext('CylinderType');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to CylinderType');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKENAME CylinderType');
			$this->feedback->setContext('CylinderType');
		}
		$this->log();
		return $result;
	}

}

?>