<?php
namespace Cyl\CylinderKey;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $idCylinder;
	protected $idKey;

	public function getId()
	{
		return $this->id;
	}

	public function getIdCylinder()
	{
		return $this->idCylinder;
	}

	public function getIdKey()
	{
		return $this->idKey;
	}

	public function setId($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setIdCylinder($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idCylinder');
			$this->feedback->setText('idCylinder is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->idCylinder = $value;
		}
	}

	public function setIdKey($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idKey');
			$this->feedback->setText('idKey is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->idKey = $value;
		}
	}
}

?>

