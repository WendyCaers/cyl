<?php
namespace Cyl\Profile;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $brand;
	protected $type;
    protected $img;

	public function getId()
	{
		return $this->id;
	}

	public function getBrand()
	{
		return $this->brand;
	}

	public function getType()
	{
		return $this->type;
	}

    public function getImg()
    {
        return $this->img;
    }

	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('id');
			$this->feedback->setText('id moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setBrand($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('brand');
			$this->feedback->setText('brand is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->brand = $value;
		}
	}

	public function setType($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('type');
			$this->feedback->setText('type is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->type = $value;
		}
	}

    public function setImg($value)
    {
        $value = strip_tags($value);
        $this->img = $value;
    }

}

?>

