<?php
namespace Cyl\MasterKeyPlan;
class Dal extends \AnOrmApart\Base\Dal
{
    
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		//if (!$this->bdo->isValid()) verandert in hetgeen hieronder. voor meer uitleg kijk bij profile-dal.class
        if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL MasterKeyPlanInsert(@pId, 
                    :pIdProfile, :pNumber, :pDate, :pInvoice, :pDescription, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdProfile', $this->bdo->getIdProfile(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pNumber', $this->bdo->getNumber(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDate', $this->bdo->getDate(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInvoice', $this->bdo->getInvoice(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDescription', $this->bdo->getDescription(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MasterKeyPlanInsert(). 
                        Rij is niet toegevoegd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanInsert');
				$this->feedback->setContext('MasterKeyPlan');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		//if (!$this->bdo->isValid()) verandert in hetgeen hieronder. voor meer uitleg kijk bij profile-dal.class
        if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL MasterKeyPlanUpdate(:pId, 
                    :pIdProfile, :pNumber, :pDate, :pInvoice, :pDescription, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdProfile', $this->bdo->getIdProfile(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pNumber', $this->bdo->getNumber(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDate', $this->bdo->getDate(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInvoice', $this->bdo->getInvoice(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDescription', $this->bdo->getDescription(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					//$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MasterKeyPlanUpdte(). 
                        Rij is niet gewijzigd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanUpdate');
				$this->feedback->setContext('MasterKeyPlan');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL MasterKeyPlanDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MasterKeyPlanDelete(). 
                        Rij is niet verwijderd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanDelete');
				$this->feedback->setContext('MasterKeyPlan');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}


	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
		$this->feedback->setContext('MasterKeyPlan');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL MasterKeyPlanSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setIdProfile($array['IdProfile']);
					$this->bdo->setNumber($array['Number']);
					$this->bdo->setDate($array['Date']);
					$this->bdo->setInvoice($array['Invoice']);
					$this->bdo->setDescription($array['Description']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is niet gevonden.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectOne');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gekozen.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL MasterKeyPlanSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText("Tabel MasterKeyPlan {$this->rowCount} rijen ingelezen.");
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectAll');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rijen zijn niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectById()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectById');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gekozen.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

   
	public function selectByIdProfile()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectByIdProfile');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectByIdProfile(:pIdProfile)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdProfile', $this->bdo->getIdProfile(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdProfile()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectByIdProfile');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDPROFILE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectByNumber()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectByNumber');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectByNumber(:pNumber)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pNumber', $this->bdo->getNumber(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getNumber()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectByNumber');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYNUMBER MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectByInvoice()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectByInvoice');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectByInvoice(:pInvoice)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pInvoice', $this->bdo->getInvoice(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getInvoice()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectByInvoice');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYINVOICE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectLikeNumber()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectLikeNumber');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectLikeNumber(:pNumber)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pNumber', $this->bdo->getNumber(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getNumber()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectLikeNumber');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKENUMBER MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectLikeInvoice()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectLikeInvoice');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectLikeInvoice(:pInvoice)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pInvoice', $this->bdo->getInvoice(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getInvoice()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectLikeInvoice');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEINVOICE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
    public function selectLikeXNumber()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectLikeXNumber');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectLikeXNumber(:pNumber)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pNumber', $this->bdo->getNumber(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getNumber()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectLikeXNumber');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEXNUMBER MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}

    
	public function selectLikeXInvoice()
	{
		$this->feedback->startTimeInKey('MasterKeyPlanSelectLikeXInvoice');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL MasterKeyPlanSelectLikeXInvoice(:pInvoice)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pInvoice', $this->bdo->getInvoice(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getInvoice()} in tabel MasterKeyPlan gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MasterKeyPlan is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('MasterKeyPlan');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MasterKeyPlanSelectLikeXInvoice');
				$this->feedback->setContext('MasterKeyPlan');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MasterKeyPlan');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEXINVOICE MasterKeyPlan');
			$this->feedback->setContext('MasterKeyPlan');
		}
		$this->log();
		return $result;
	}
}

?>