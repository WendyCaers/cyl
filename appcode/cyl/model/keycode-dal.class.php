<?php
namespace Cyl\KeyCode;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL KeyCodeInsert(@pId, :pPassive, :pLateral, :pActive, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pPassive', $this->bdo->getPassive(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pLateral', $this->bdo->getLateral(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pActive', $this->bdo->getActive(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure KeyCodeInsert(). 
                        Rij is niet toegevoegd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeInsert');
				$this->feedback->setContext('KeyCode');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL KeyCodeUpdate(:pId, :pPassive, :pLateral, :pActive, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pPassive', $this->bdo->getPassive(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pLateral', $this->bdo->getLateral(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pActive', $this->bdo->getActive(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure KeyCodeUpdte(). 
                        Rij is niet gewijzigd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeUpdate');
				$this->feedback->setContext('KeyCode');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL KeyCodeDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure KeyCodeDelete(). 
                        Rij is niet verwijderd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeDelete');
				$this->feedback->setContext('KeyCode');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
		$this->feedback->setContext('KeyCode');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL KeyCodeSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setPassive($array['Passive']);
					$this->bdo->setLateral($array['Lateral']);
					$this->bdo->setActive($array['Active']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . 
                        '</b> is niet gevonden.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeSelectOne');
				$this->feedback->setContext('KeyCode');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL KeyCodeSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText("Tabel KeyCode {$this->rowCount} rijen ingelezen.");
				}
				else
				{
					$this->feedback->setText('Tabel KeyCode is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeSelectAll');
				$this->feedback->setContext('KeyCode');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('KeyCodeSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL KeyCodeSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel KeyCode gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel KeyCode is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeSelectById');
				$this->feedback->setContext('KeyCode');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}

	public function selectByCode()
	{
		$this->feedback->startTimeInKey('KeyCodeSelectByCode');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL KeyCodeSelectByCode(:pPassive, :pLateral, :pActive)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pPassive', $this->bdo->getPassive(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pLateral', $this->bdo->getLateral(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pActive', $this->bdo->getActive(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) in tabel KeyCode gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel KeyCode is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('KeyCode');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL KeyCodeSelectByCode');
				$this->feedback->setContext('KeyCode');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to KeyCode');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYCODE KeyCode');
			$this->feedback->setContext('KeyCode');
		}
		$this->log();
		return $result;
	}
}
?>