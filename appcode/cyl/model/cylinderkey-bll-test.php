<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('cylinderkey-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new Cyl\CylinderKey\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d H:i:s");
	$bll->setId(999);
	$bll->setIdCylinder(999);
	$bll->setIdKey(999);
	$bll->setInsertedBy('InsertedBy');
	$bll->setInsertedOn();
	$bll->setUpdatedBy('UpdatedBy');
	$bll->setUpdatedOn();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL CilinderKey</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h3><?php echo $key;?></h3>
			<h4><?php echo $feedback->getName();?></h4>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>CylinderKey</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdCylinder</label>
				<span><?php echo $bll->getIdCylinder();?></span>
			</div>
			<div>
				<label>IdKey</label>
				<span><?php echo $bll->getIdKey();?></span>
			</div>
			<div>
				<label>Inserted by</label>
				<span><?php echo $bll->getInsertedBy();?></span>
			</div>
			<div>
				<label>Inserted on</label>
				<span><?php echo $bll->getInsertedOn();?></span>
			</div>
			<div>
				<label>Updated by</label>
				<span><?php echo $bll->getUpdatedBy();?></span>
			</div>
			<div>
				<label>Updated on</label>
				<span><?php echo $bll->getUpdatedOn();?></span>
			</div>
		</fieldset>
	</body>
</html>


