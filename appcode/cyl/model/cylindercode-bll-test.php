<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('cylindercode-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new Cyl\CylinderCode\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d H:i:s");
	$bll->setId(999);
	$bll->setPassive(999);
	$bll->setLateral(999);
	$bll->setActive(999);
	$bll->setAdd1(999);
	$bll->setAdd2(999);
	$bll->setAdd3(999);
	$logbook = $log->getBook();
	//print_r(logbook);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL Cylindercode</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>CylinderCode</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Passief</label>
				<span><?php echo $bll->getPassive();?></span>
			</div>
			<div>
				<label>Lateraal</label>
				<span><?php echo $bll->getLateral();?></span>
			</div>
			<div>
				<label>Actief</label>
				<span><?php echo $bll->getActive();?></span>
			</div>
			<div>
				<label>Actief +1</label>
				<span><?php echo $bll->getAdd1();?></span>
			</div>
			<div>
				<label>Actief +2</label>
				<span><?php echo $bll->getAdd2();?></span>
			</div>
			<div>
				<label>Actief +3</label>
				<span><?php echo $bll->getAdd3();?></span>
			</div>
		</fieldset>
	</body>
</html>
