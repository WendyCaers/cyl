<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	// include connection class
	include ('../../helpers/connection.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('../../helpers/base-dal.class.php');
	// include bll class for table
	include ('cylinder-bll.class.php');
	// include dal class for table
	include ('cylinder-dal.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect
	$connection = new AnOrmApart\Dal\Connection($log);
	// create an instance of the DAL class for this table
	$dal = new Cyl\Cylinder\Dal($log);
    $dal->setConnection($connection);
	// create an instance of the Bll class for this table
	$bll = new Cyl\Cylinder\Bll($log);

	// we start with making a business object
	$bll->setId(999);
	$bll->setIdMasterKeyPlan(1);
	$bll->setIdCylinderCode(1);
	$bll->setIdCylinderSize(1);
	$bll->setName('Name');
	$bll->setIndex(1);
	$bll->setInitialQuantity(1);
	$bll->setBackorder(1);

    //open connection
    $connection->open();

	// INSERT
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// and now it's time to insert
	            //$dal->insert();

	// UPDATE
	// first change bdo
    // set id to number id from row inserted
    $bll->setId(6);
	$bll->setIdMasterKeyPlan(2);
	$bll->setIdCylinderCode(2);
	$bll->setIdCylinderSize(2);
	$bll->setName('NameUpdate');
	$bll->setIndex(2);
	$bll->setInitialQuantity(2);
	$bll->setBackorder(2);
    // now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	        //$dal->update();

    //DELETE
    // delete row with Id set in bll object
	        //$dal->delete();

    //SELECTALL
    $list = $dal->selectAll();

    //SELECTBYID
    //outcome returns a profile
    $bll->setId(2);
    $selectById = $dal->selectById();

    //SELECTBYIDMASTERKEYPLAN
    $bll->setIdMasterKeyPlan(3);
    $selectByIdMasterKeyPlan = $dal->selectByIdMasterKeyPlan();

    //SELECTBYIDCYLINDERCODE
    $bll->setIdCylinderCode(3);
    $selectByIdCylinderCode = $dal->selectByIdCylinderCode();

    //SELECTBYIDCYLINDERSIZE
    $bll->setIdCylinderSize(3);
    $selectByIdCylinderSize = $dal->selectByIdCylinderSize();

    //SELECTONE
    //outcome returns true or false
    $bll->setId(1);
    $dal->selectOne();
	
	// disconnect
	$connection->close();

	$logbook = $log->getBook();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test DAL Cylinder</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Cylinder BLL</legend>
			<div>
				<label>ID</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdSluitplan</label>
				<span><?php echo $bll->getIdMasterKeyPlan();?></span>
			</div>
			<div>
				<label>IdCode</label>
				<span><?php echo $bll->getIdCylinderCode();?></span>
			</div>
			<div>
				<label>IdCylindermaat</label>
				<span><?php echo $bll->getIdCylinderSize();?></span>
			</div>
			<div>
				<label>Naam</label>
				<span><?php echo $bll->getName();?></span>
			</div>
			<div>
				<label>Index</label>
				<span><?php echo $bll->getIndex();?></span>
			</div>
			<div>
				<label>Beginaantal</label>
				<span><?php echo $bll->getInitialQuantity();?></span>
			</div>
			<div>
				<label>Nabesteld</label>
				<span><?php echo $bll->getBackorder();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>Cylinder DAL</caption>
			<tr>
                <th>ID</th>
                <th>IdSluitplan</th>
                <th>IdCode</th>
                <th>IdCilindermaat</th>
                <th>Naam</th>
                <th>Index</th>
                <th>Beginaantal</th>
                <th>Nabesteld</th>
                <th>SP-nummer</th>
                <th>SP-datum</th>
                <th>SP-factuur</th>
                <th>SP-omschrijving</th>
                <th>Code-actief</th>
                <th>cilindermaat</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdMasterKeyPlan'];?></td>
                <td><?php echo $row['IdCylinderCode'];?></td>
                <td><?php echo $row['IdCylinderSize'];?></td>
                <td><?php echo $row['Name'];?></td>
				<td><?php echo $row['Index'];?></td>
                <td><?php echo $row['InitialQuantity'];?></td>
                <td><?php echo $row['Backorder'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectOne</th>
            </tr>
            
            <tr>
                <td><?php echo $bll->getId();?></td>
                <td><?php echo $bll->getIdMasterKeyPlan();?></td>
                <td><?php echo $bll->getIdCylinderCode();?></td>
                <td><?php echo $bll->getIdCylinderSize();?></td>
                <td><?php echo $bll->getName();?></td>
                <td><?php echo $bll->getIndex();?></td>
                <td><?php echo $bll->getInitialQuantity();?></td>
                <td><?php echo $bll->getBackorder();?></td>
            </tr>
            <tr>
                <th>SelectById</th>
            </tr>
            <tr>
                <td><?php echo $selectById[0]['Id'];?></td>
                <td><?php echo $selectById[0]['IdMasterKeyPlan'];?></td>
                <td><?php echo $selectById[0]['IdCylinderCode'];?></td>
                <td><?php echo $selectById[0]['IdCylinderSize'];?></td>
                <td><?php echo $selectById[0]['Name'];?></td>
                <td><?php echo $selectById[0]['Index'];?></td>
                <td><?php echo $selectById[0]['InitialQuantity'];?></td>
                <td><?php echo $selectById[0]['Backorder'];?></td>
                <td><?php echo $selectById[0]['Number'];?></td>
                <td><?php echo $selectById[0]['Date'];?></td>
                <td><?php echo $selectById[0]['Invoice'];?></td>
                <td><?php echo $selectById[0]['Description'];?></td>
                <td><?php echo $selectById[0]['Active'];?></td>
                <td><?php echo $selectById[0]['Size'];?></td>
            </tr>
            <tr>
                <th>SelectByIdMasterKeyPlan</th>
            </tr>
            <?php
			foreach ($selectByIdMasterKeyPlan as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdMasterKeyPlan'];?></td>
                <td><?php echo $row['IdCylinderCode'];?></td>
                <td><?php echo $row['IdCylinderSize'];?></td>
                <td><?php echo $row['Name'];?></td>
				<td><?php echo $row['Index'];?></td>
                <td><?php echo $row['InitialQuantity'];?></td>
                <td><?php echo $row['Backorder'];?></td>
                <td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
                <td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
                <td><?php echo $row['Active'];?></td>
                <td><?php echo $row['Size'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByIdCylinderCode</th>
            </tr>
            <?php
			foreach ($selectByIdCylinderCode as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdMasterKeyPlan'];?></td>
                <td><?php echo $row['IdCylinderCode'];?></td>
                <td><?php echo $row['IdCylinderSize'];?></td>
                <td><?php echo $row['Name'];?></td>
				<td><?php echo $row['Index'];?></td>
                <td><?php echo $row['InitialQuantity'];?></td>
                <td><?php echo $row['Backorder'];?></td>
                <td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
                <td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
                <td><?php echo $row['Active'];?></td>
                <td><?php echo $row['Size'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByIdCylinderSize</th>
            </tr>
            <?php
			foreach ($selectByIdCylinderSize as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdMasterKeyPlan'];?></td>
                <td><?php echo $row['IdCylinderCode'];?></td>
                <td><?php echo $row['IdCylinderSize'];?></td>
                <td><?php echo $row['Name'];?></td>
				<td><?php echo $row['Index'];?></td>
                <td><?php echo $row['InitialQuantity'];?></td>
                <td><?php echo $row['Backorder'];?></td>
                <td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
                <td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
                <td><?php echo $row['Active'];?></td>
                <td><?php echo $row['Size'];?></td>
			</tr>
			<?php
			}
			?>

		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>
