<?php
namespace Cyl\MasterKeyPlan;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $idProfile;
	protected $number;
	protected $date;
	protected $invoice;
	protected $description;

	public function getId()
	{
		return $this->id;
	}

	public function getIdProfile()
	{
		return $this->idProfile;
	}

	public function getNumber()
	{
		return $this->number;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function getInvoice()
	{
		return $this->invoice;
	}

	public function getDescription()
	{
		return $this->description;
	}


	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('id');
			$this->feedback->setText('id moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setIdProfile($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idProfile');
			$this->feedback->setText('idProfile is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('idProfile');
			$this->feedback->setText('idProfile moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->idProfile = $value;
		}
	}

	public function setNumber($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('number');
			$this->feedback->setText('number is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->number = $value;
		}
	}

	public function setDate($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('date');
			$this->feedback->setText('date is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        //eerst kijken of er een datumnotatie uitgehaald kan worden
        elseif(!date_create_from_format('Y-m-d', $value))
        {
            $this->feedback->start('date');
			$this->feedback->setText('ongeldige datumnotatie');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
        //zo ja kijken of het om een geldige Gregoriaanse datum gaat
        else
        {
            $date_arr = explode('-', $value);
            $year = $date_arr[0];
            $month = $date_arr[1];
            $day = $date_arr[2];
            if(!checkdate($month, $day, $year))
            {
                $this->feedback->start('date');
			    $this->feedback->setText('ongeldige datum');
			    $this->feedback->setIsError(TRUE);
			    $this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			    $this->feedback->end();
			    $this->log();
            }
        }
		if (!$this->feedback->getIsError())
		{
			$this->date = $value;
		}
	}

	public function setInvoice($value)
	{
        $value = strip_tags($value);
		$this->invoice = $value;
	}

	public function setDescription($value)
	{
        $value = strip_tags($value);
		$this->description = $value;
	}


}

?>