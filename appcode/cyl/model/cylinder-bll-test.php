<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('cylinder-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new Cyl\Cylinder\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d H:i:s");
	$bll->setId(999);
	$bll->setIdMasterKeyPlan(999);
	$bll->setIdCylinderCode(999);
	$bll->setIdCylinderSize(999);
	$bll->setName('Name');
	$bll->setIndex(999);
	$bll->setInitialQuantity(999);
	$bll->setBackorder(999);
	$logbook = $log->getBook();
	//print_r(logbook);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL Cylinder</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Cylinder</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdSluitplan</label>
				<span><?php echo $bll->getIdMasterKeyPlan();?></span>
			</div>
			<div>
				<label>IdCode</label>
				<span><?php echo $bll->getIdCylinderCode();?></span>
			</div>
			<div>
				<label>IdCylindermaat</label>
				<span><?php echo $bll->getIdCylinderSize();?></span>
			</div>
			<div>
				<label>Naam</label>
				<span><?php echo $bll->getName();?></span>
			</div>
			<div>
				<label>Index</label>
				<span><?php echo $bll->getIndex();?></span>
			</div>
			<div>
				<label>Beginaantal</label>
				<span><?php echo $bll->getInitialQuantity();?></span>
			</div>
			<div>
				<label>Nabesteld</label>
				<span><?php echo $bll->getBackorder();?></span>
			</div>
		</fieldset>
	</body>
</html>
