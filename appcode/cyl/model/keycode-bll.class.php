<?php
namespace Cyl\KeyCode;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $passive;
	protected $lateral;
	protected $active;

	public function getId()
	{
		return $this->id;
	}

	public function getPassive()
	{
		return $this->passive;
	}

	public function getLateral()
	{
		return $this->lateral;
	}

	public function getActive()
	{
		return $this->active;
	}

	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('id');
			$this->feedback->setText('id moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setPassive($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('passive');
			$this->feedback->setText('passive is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('passive');
			$this->feedback->setText('passive moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->passive = $value;
		}
	}

	public function setLateral($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('lateral');
			$this->feedback->setText('lateral is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('lateral');
			$this->feedback->setText('lateral moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->lateral = $value;
		}
	}

	public function setActive($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('active');
			$this->feedback->setText('active is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('active');
			$this->feedback->setText('active moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->active = $value;
		}
	}
}

?>