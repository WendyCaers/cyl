<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	// include connection class
	include ('../../helpers/connection.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('../../helpers/base-dal.class.php');
	// include bll class for table
	include ('cylindersize-bll.class.php');
	// include dal class for table
	include ('cylindersize-dal.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect
	$connection = new AnOrmApart\Dal\Connection($log);
	// create an instance of the DAL class for this table
	$dal = new Cyl\CylinderSize\Dal($log);
    $dal->setConnection($connection);
	// create an instance of the Bll class for this table
	$bll = new Cyl\CylinderSize\Bll($log);

	// we start with making a business object
	$bll->setId(999);
	$bll->setIdCylinderType(1);
	$bll->setName('Name');
    $bll->setInsertedBy('WendyViaDal');

    //open connection
    $connection->open();

	// INSERT
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// and now it's time to insert
	            //$dal->insert();
    
    // UPDATE
	// first change bdo
    // set id to number id from row inserted
    $bll->setId(6);
	$bll->setIdCylinderType(2);
	$bll->setName('NameUpdate');
    $bll->setUpdatedBy('WendyViaDal');
    // now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	        //$dal->update();

    //DELETE
    // delete row with Id set in bll object
	        //$dal->delete();

    //SELECTALL
    $list = $dal->selectAll();

    //SELECTBYID
    //outcome returns a profile
    $bll->setId(2);
    $selectById = $dal->selectById();

    //SELECTBYIDCYLINDERTYPE
    $bll->setIdCylinderType(1);
    $selectByIdCylinderType = $dal->selectByIdCylinderType();

    //SELECTBYNAME
    $bll->setName('30/30');
    $selectByName = $dal->selectByName();

    //SELECTLIKENAME
    $bll->setName('30');
    $selectLikeName = $dal->selectLikeName();

    //SELECTONE
    //outcome returns true or false
    $bll->setId(1);
    $dal->selectOne();

	// disconnect
	$connection->close();

	$logbook = $log->getBook();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test DAL Cylindersize</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>CylinderSize BLL</legend>
			<div>
				<label>ID</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>CylindertypeID</label>
				<span><?php echo $bll->getIdCylinderType();?></span>
			</div>
			<div>
				<label>naam</label>
				<span><?php echo $bll->getName();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>CylinderSize DAL</caption>
			<tr>
                <th>ID</th>
                <th>CylinderTypeID</th>
				<th>Naam</th>
                <th>Type</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdCylinderType'];?></td>
				<td><?php echo $row['Name'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectOne</th>
            </tr>
            
            <tr>
                <td><?php echo $bll->getId();?></td>
                <td><?php echo $bll->getIdCylinderType();?></td>
                <td><?php echo $bll->getName();?></td>
            </tr>
            <tr>
                <th>SelectById</th>
            </tr>
            
            <tr>
                <td><?php echo $selectById[0]['Id'];?></td>
                <td><?php echo $selectById[0]['IdCylinderType'];?></td>
                <td><?php echo $selectById[0]['Name'];?></td>
                <td><?php echo $selectById[0]['Type'];?></td>
            </tr>
            <tr>
                <th>SelectByIdCylinderType</th>
            </tr>
            <?php
			foreach ($selectByIdCylinderType as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdCylinderType'];?></td>
                <td><?php echo $row['Name'];?></td>
                <td><?php echo $row['Type'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByName</th>
            </tr>
            <?php
			foreach ($selectByName as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdCylinderType'];?></td>
                <td><?php echo $row['Name'];?></td>
                <td><?php echo $row['Type'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeName</th>
            </tr>
            <?php
			foreach ($selectLikeName as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
				<td><?php echo $row['IdCylinderType'];?></td>
                <td><?php echo $row['Name'];?></td>
                <td><?php echo $row['Type'];?></td>
			</tr>
			<?php
			}
            ?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>
