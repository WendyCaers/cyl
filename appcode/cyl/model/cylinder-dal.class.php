<?php
namespace Cyl\Cylinder;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
                    prepare('CALL CylinderInsert(@pId, :pIdMasterKeyPlan, :pIdCylinderCode, :pIdCylinderSize, :pName, 
                    :pIndex, :pInitialQuantity, :pBackorder, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdMasterKeyPlan', $this->bdo->getIdMasterKeyPlan(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdCylinderCode', $this->bdo->getIdCylinderCode(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdCylinderSize', $this->bdo->getIdCylinderSize(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pIndex', $this->bdo->getIndex(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInitialQuantity', $this->bdo->getInitialQuantity(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pBackorder', $this->bdo->getBackorder(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderInsert(). Rij is niet toegevoegd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderInsert');
				$this->feedback->setContext('Cylinder');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderUpdate(:pId, :pIdMasterKeyPlan, :pIdCylinderCode, :pIdCylinderSize, 
                    :pName, :pIndex, :pInitialQuantity, :pBackorder, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdMasterKeyPlan', $this->bdo->getIdMasterKeyPlan(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdCylinderCode', $this->bdo->getIdCylinderCode(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdCylinderSize', $this->bdo->getIdCylinderSize(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pIndex', $this->bdo->getIndex(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInitialQuantity', $this->bdo->getInitialQuantity(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pBackorder', $this->bdo->getBackorder(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderUpdte(). Rij is niet gewijzigd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderUpdate');
				$this->feedback->setContext('Cylinder');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzgid.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure CylinderDelete(). Rij is niet verwijderd.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderDelete');
				$this->feedback->setContext('Cylinder');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
		$this->feedback->setContext('Cylinder');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare('CALL CylinderSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setIdMasterKeyPlan($array['IdMasterKeyPlan']);
					$this->bdo->setIdCylinderCode($array['IdCylinderCode']);
					$this->bdo->setIdCylinderSize($array['IdCylinderSize']);
					$this->bdo->setName($array['Name']);
					$this->bdo->setIndex($array['Index']);
					$this->bdo->setInitialQuantity($array['InitialQuantity']);
					$this->bdo->setBackorder($array['Backorder']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is niet gevonden.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectOne');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL CylinderSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText("Tabel Cylinder {$this->rowCount} rijen ingelezen.");
				}
				else
				{
					$this->feedback->setText('Tabel Cylinder is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectAll');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('CylinderSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel Cylinder gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Cylinder is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectById');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectByIdMasterKeyPlan()
	{
		$this->feedback->startTimeInKey('CylinderSelectByIdMasterKeyPlan');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderSelectByIdMasterKeyPlan(:pIdMasterKeyPlan)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdMasterKeyPlan', $this->bdo->getIdMasterKeyPlan(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdMasterKeyPlan()} in tabel Cylinder gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Cylinder is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectByIdMasterKeyPlan');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDMASTERKEYPLAN Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectByIdCylinderCode()
	{
		$this->feedback->startTimeInKey('CylinderSelectByIdCylinderCode');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				    prepare("CALL CylinderSelectByIdCylinderCode(:pIdCylinderCode)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdCylinderCode', $this->bdo->getIdCylinderCode(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met {$this->bdo->getIdCylinderCode()} 
                        in tabel Cylinder gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Cylinder is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectByIdCylinderCode');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDCYLINDERCODE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}

	public function selectByIdCylinderSize()
	{
		$this->feedback->startTimeInKey('CylinderSelectByIdCylinderSize');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL CylinderSelectByIdCylinderSize(:pIdCylinderSize)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdCylinderSize', $this->bdo->getIdCylinderSize(), \PDO::PARAM_INT);
				$preparedStatement->execute();
					$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met {$this->bdo->getIdCylinderSize()} 
                        in tabel Cylinder gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Cylinder is leeg.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				$this->feedback->setContext('Cylinder');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL CylinderSelectByIdCylinderSize');
				$this->feedback->setContext('Cylinder');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to Cylinder');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDCYLINDERSIZE Cylinder');
			$this->feedback->setContext('Cylinder');
		}
		$this->log();
		return $result;
	}
}
?>