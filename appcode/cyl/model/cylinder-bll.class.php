<?php
namespace Cyl\Cylinder;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $idMasterKeyPlan;
	protected $idCylinderCode;
	protected $idCylinderSize;
	protected $name;
	protected $index;
	protected $initialQuantity;
	protected $backorder;

	public function getId()
	{
		return $this->id;
	}

	public function getIdMasterKeyPlan()
	{
		return $this->idMasterKeyPlan;
	}

	public function getIdCylinderCode()
	{
		return $this->idCylinderCode;
	}

	public function getIdCylinderSize()
	{
		return $this->idCylinderSize;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getIndex()
	{
		return $this->index;
	}

	public function getInitialQuantity()
	{
		return $this->initialQuantity;
	}

	public function getBackorder()
	{
		return $this->backorder;
	}

	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('id');
			$this->feedback->setText('id moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setIdMasterKeyPlan($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idMasterKeyPlan');
			$this->feedback->setText('idMasterKeyPlan is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('idMasterKeyPlan');
			$this->feedback->setText('idMasterKeyPlan moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->idMasterKeyPlan = $value;
		}
	}

	public function setIdCylinderCode($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idCylinderCode');
			$this->feedback->setText('idCylinderCode is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('idCylinderCode');
			$this->feedback->setText('idCylinderCode moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->idCylinderCode = $value;
		}
	}

	public function setIdCylinderSize($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idCylinderSize');
			$this->feedback->setText('idCylinderSize is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('idCylinderSize');
			$this->feedback->setText('idCylinderSize moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->idCylinderSize = $value;
		}
	}

	public function setName($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('name');
			$this->feedback->setText('name is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->name = $value;
		}
	}

	public function setIndex($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('index');
			$this->feedback->setText('index is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('index');
			$this->feedback->setText('index moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->index = $value;
		}
	}

	public function setInitialQuantity($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('initialQuantity');
			$this->feedback->setText('initialQuantity is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!is_numeric($value))
        {
            $this->feedback->start('initialQuantity');
			$this->feedback->setText('initialQuantity moet een numerieke waarde zijn');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->initialQuantity = $value;
		}
	}

	public function setBackorder($value)
	{
        $value = strip_tags($value);
        if(strlen($value) > 0)
        {
            if(!is_numeric($value))
            {
                $this->feedback->start('backorder');
			    $this->feedback->setText('backorder moet een numerieke waarde zijn');
			    $this->feedback->setIsError(TRUE);
			    $this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			    $this->feedback->end();
			    $this->log();
            }
            if (!$this->feedback->getIsError())
            {
                $this->backorder = $value;
            }
        }
	}
}

?>