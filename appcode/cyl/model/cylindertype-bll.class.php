<?php
namespace Cyl\CylinderType;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $name;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setId($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setName($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('name');
			$this->feedback->setText('name is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->name = $value;
		}
	}
}

?>