<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	// include connection class
	include ('../../helpers/connection.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('../../helpers/base-dal.class.php');
	// include bll class for table
	include ('masterkeyplan-bll.class.php');
	// include dal class for table
	include ('masterkeyplan-dal.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect
	$connection = new AnOrmApart\Dal\Connection($log);
	// create an instance of the DAL class for this table
	$dal = new Cyl\MasterKeyPlan\Dal($log);
    $dal->setConnection($connection);
	// create an instance of the Bll class for this table
	$bll = new Cyl\MasterKeyPlan\Bll($log);

	// we start with making a business object
    $bll->setId(999);
	$bll->setIdProfile(1);
	$bll->setNumber('Number');
    $bll->setDate(date("Y-m-d H:i:s"));
	$bll->setInvoice('Invoice');
	$bll->setDescription('Description');

     //open connection
    $connection->open();

	// INSERT
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// and now it's time to insert
	            //$dal->insert();
    
     // UPDATE
     // first change bdo
    // set id to number id from row inserted
    $bll->setId(6);
	$bll->setNumber('NumberUpdate');
    $bll->setDate(date("Y-m-d H:i:s"));
	$bll->setInvoice('InvoiceUpdate');
	$bll->setDescription('DescriptionUpdate');
    $bll->setUpdatedBy('WendyViaDal');
    // now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	        //$dal->update();

    //DELETE
    // delete row with Id set in bll object
	        //$dal->delete();

    //SELECTALL
    $list = $dal->selectAll();

    //SELECTBYID
    //outcome returns a profile
    $bll->setId(2);
    $selectById = $dal->selectById();

    //SELECTBYIDPROFILE
    $bll->setIdProfile(1);
    $selectByIdProfile = $dal->selectByIdProfile();

    //SELECTBYNUMBER
    $bll->setNumber('SH9999003');
    $selectByNumber = $dal->selectByNumber();

    //SELECTBYINVOICE
    $bll->setInvoice('F/9999050');
    $selectByInvoice = $dal->selectByInvoice();

    //SELECTLIKENUMBER
    $bll->setNumber('SH9999');
    $selectLikeNumber = $dal->selectLikeNumber();

    //SELECTLIKEINVOICE
    $bll->setInvoice('F/99990');
    $selectLikeInvoice = $dal->selectLikeInvoice();

    //SELECTLIKEXNUMBER
    $bll->setNumber('0');
    $selectLikeXNumber = $dal->selectLikeXNumber();

    //SELECTLIKEXINVOICE
    $bll->setInvoice('0');
    $selectLikeXInvoice = $dal->selectLikeXInvoice();

    //SELECTONE
    //outcome returns true or false
    $bll->setId(1);
    $dal->selectOne();

	// disconnect
	$connection->close();

	$logbook = $log->getBook();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test DAL MasterKeyPlan</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
        <br/>
		<fieldset>
			<legend>MasterKeyPlan BLL</legend>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdProfile</label>
				<span><?php echo $bll->getIdProfile();?></span>
			</div>
			<div>
				<label>Sluitplannummer</label>
				<span><?php echo $bll->getNumber();?></span>
			</div>
			<div>
				<label>Datum</label>
				<span><?php echo $bll->getDate();?></span>
			</div>
			<div>
				<label>Factuur</label>
				<span><?php echo $bll->getInvoice();?></span>
			</div>
			<div>
				<label>Omschrijving</label>
				<span><?php echo $bll->getDescription();?></span>
			</div>
		</fieldset>
        <br/>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>MasterKeyPlan DAL</caption>
			<tr>
                <th>Id</th>
                <th>IdProfile</th>
				<th>Sluitplannummer</th>
                <th>Datum</th>
				<th>Factuur</th>
                <th>Omschrijving</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectOne</th>
            </tr>
            
            <tr>
                <td><?php echo $bll->getId();?></td>
                <td><?php echo $bll->getIdProfile();?></td>
                <td><?php echo $bll->getNumber();?></td>
                <td><?php echo $bll->getDate();?></td>
                <td><?php echo $bll->getInvoice();?></td>
                <td><?php echo $bll->getDescription();?></td>
            </tr>
            <tr>
                <th>SelectById</th>
            </tr>
            
            <tr>
                <td><?php echo $selectById[0]['Id'];?></td>
                <td><?php echo $selectById[0]['IdProfile'];?></td>
                <td><?php echo $selectById[0]['Number'];?></td>
                <td><?php echo $selectById[0]['Date'];?></td>
                <td><?php echo $selectById[0]['Invoice'];?></td>
                <td><?php echo $selectById[0]['Description'];?></td>
            </tr>
            <tr>
                <th>SelectByIdProfile</th>
            </tr>
            <?php
			foreach ($selectByIdProfile as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByNumber</th>
            </tr>
            <?php
			foreach ($selectByNumber as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectByInvoice</th>
            </tr>
            <?php
			foreach ($selectByInvoice as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeNumber</th>
            </tr>
            <?php
			foreach ($selectLikeNumber as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeInvoice</th>
            </tr>
            <?php
			foreach ($selectLikeInvoice as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeXNumber</th>
            </tr>
            <?php
			foreach ($selectLikeXNumber as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
            <tr>
                <th>SelectLikeXInvoice</th>
            </tr>
            <?php
			foreach ($selectLikeXInvoice as $row)
			{
			?>
			<tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['IdProfile'];?></td>
				<td><?php echo $row['Number'];?></td>
                <td><?php echo $row['Date'];?></td>
				<td><?php echo $row['Invoice'];?></td>
                <td><?php echo $row['Description'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>
