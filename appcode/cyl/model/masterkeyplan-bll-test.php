<?php
	include ('../../helpers/feedback.class.php');
	include ('../../helpers/log.class.php');
	include ('../../helpers/base-bll.class.php');
	include ('masterkeyplan-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new Cyl\MasterKeyPlan\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d");
	$bll->setId(999);
	$bll->setIdProfile(999);
	$bll->setNumber('Number');
    $bll->setDate($startTimeStamp);
	$bll->setInvoice('Invoice');
	$bll->setDescription('Description');
	$logbook = $log->getBook();
	//print_r(logbook);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL MasterKeyPlan</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>MasterKeyPlan</legend>
			<div>
				<label>Id</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>IdProfile</label>
				<span><?php echo $bll->getIdProfile();?></span>
			</div>
			<div>
				<label>Sluitplannummer</label>
				<span><?php echo $bll->getNumber();?></span>
			</div>
			<div>
				<label>Datum</label>
				<span><?php echo $bll->getDate();?></span>
			</div>
			<div>
				<label>Factuur</label>
				<span><?php echo $bll->getInvoice();?></span>
			</div>
			<div>
				<label>Omschrijving</label>
				<span><?php echo $bll->getDescription();?></span>
			</div>
		</fieldset>
	</body>
</html>
