<?php

function prepareImageForSave($imgData)
{
    //spaties vervangen door + teken
    $imgDataPlus = str_replace(' ','+',$imgData);
    // data: vervangen door data:// Dit is nodig om te werken in php
    $imgPhp = 'data://' . substr($imgDataPlus, 5);
    $imgBinary = file_get_contents($imgPhp);
    return $imgBinary;
}

function deleteImageFromServer($path)
{
    $pathServer = '../../' . $path;
    if (file_exists($pathServer)) 
    {
        unlink($pathServer); 
    }
}

?>


