<?php
    function sortListOnOneColumn($list, $col)
    {
        foreach ($list as $key => $row) {
            $column[$key] = $row[$col];
        }
        array_multisort($column, SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $list);
        return $list;
    }

    function sortListOnTwoColumns($list, $col1, $col2)
	{
		foreach ($list as $key => $row) {
            $column1[$key]  = $row[$col1];
        }
        foreach ($list as $key => $row) {
            $column2[$key]  = $row[$col2];
        }
        array_multisort($column1, SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $column2, SORT_ASC, SORT_NATURAL|SORT_FLAG_CASE, $list);
        return $list;
	}
?>