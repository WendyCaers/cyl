<?php
    // include helpers database
    include ('../helpers/base-bll.class.php');
    include ('../helpers/base-dal.class.php');
    include ('../helpers/feedback.class.php');
    include ('../helpers/log.class.php');
    include ('../helpers/connection.class.php');

    //includes from cyl
    include ('../cyl/model/profile-bll.class.php');
    include ('../cyl/model/profile-dal.class.php');
    include ('../cyl/model/masterkeyplan-bll.class.php');
    include ('../cyl/model/masterkeyplan-dal.class.php');
    include ('../cyl/model/cylinder-bll.class.php');
    include ('../cyl/model/cylinder-dal.class.php');
    include ('../cyl/model/key-bll.class.php');
    include ('../cyl/model/key-dal.class.php');
    include ('../cyl/model/cylindercode-bll.class.php');
    include ('../cyl/model/cylindercode-dal.class.php');
    include ('../cyl/model/keycode-bll.class.php');
    include ('../cyl/model/keycode-dal.class.php');
    include ('../cyl/model/cylinderkey-bll.class.php');
    include ('../cyl/model/cylinderkey-dal.class.php');
    include ('../cyl/helpers/pictures.php');

    // membership include's
    include ('../membership/helpers/session.class.php');
    include ('../membership/model/membership-bll.class.php');
    include ('../membership/model/membership-dal.class.php');
    include ('../membership/helpers/password.php');
    include ('../membership/model/membership.class.php');
    
    $log = new \AnormApart\Helpers\Log();
    //connectie maken en openen
    $connection = new \AnOrmApart\Dal\Connection($log);
    $connection->open();
    //aanmaken membership
    $membership = new \AnOrmApart\Membership($connection);
    //dal en bll objecten  aanmaken
    $profileDal = new Cyl\Profile\Dal($log);
    $profileBll = new Cyl\Profile\Bll($log);
    $mkpDal = new Cyl\MasterKeyPlan\Dal($log);
    $mkpBll = new Cyl\MasterKeyPlan\Bll($log);
    $cylinderDal = new Cyl\Cylinder\Dal($log);
    $cylinderBll = new Cyl\Cylinder\Bll($log);
    $keyDal = new Cyl\key\Dal($log);
    $keyBll = new Cyl\key\Bll($log);
    $cylinderCodeDal = new Cyl\CylinderCode\Dal($log);
    $cylinderCodeBll = new Cyl\CylinderCode\Bll($log);
    $keyCodeDal = new Cyl\KeyCode\Dal($log);
    $keyCodeBll = new Cyl\KeyCode\Bll($log);
    $cylinderKeyDal = new Cyl\CylinderKey\Dal($log);
    $cylinderKeyBll = new Cyl\CylinderKey\Bll($log);
    //connectie aan dals meegeven
    $profileDal->setConnection($connection);
    $mkpDal->setConnection($connection);
    $cylinderDal->setConnection($connection);
    $keyDal->setConnection($connection);
    $cylinderCodeDal->setConnection($connection);
    $keyCodeDal->setConnection($connection);
    $cylinderKeyDal->setConnection($connection);
    //setBdo
    $profileDal->setBdo($profileBll);
    $mkpDal->setBdo($mkpBll);
    $cylinderDal->setBdo($cylinderBll);
    $keyDal->setBdo($keyBll);
    $cylinderCodeDal->setBdo($cylinderCodeBll);
    $keyCodeDal->setBdo($keyCodeBll);
    $cylinderKeyDal->setBdo($cylinderKeyBll);
  
    // controller section
    if (isset($_REQUEST['action']))
    {
        $action = $_REQUEST['action'];
        switch($action)  
        {  
             
        //-----PROFIELEN-----
            case 'profileInsert' :
                $answer;
                if(isset($_POST['brand'], $_POST['type'])) 
                {
                    $profileBll->setBrand($_POST['brand']);
	                $profileBll->setType($_POST['type']);
                    if(isset($_POST['img'])) 
                    {
                        $imgBinary = prepareImageForSave($_POST['img']);
                        $brand = $profileBll->getBrand();
                        $type = $profileBll->getType();
                        //2 filepaths omdat het opslaan van de afbeelding van hieruit gebeurt
                        //maar het opvragen achteraf van de index.php. Het pad dat we gaan opslaan
                        //in de DB is dus anders dan het pad dat we hier gebruiken om de afbeeling op te
                        //slaan op de server.
                        $filepath = '../../images/profiles/' . $brand . '_' . $type . '.png';
                        $filepathDB = 'images/profiles/' . $brand . '_' . $type . '.png';
                        file_put_contents($filepath, $imgBinary);
                        $profileBll->setImg($filepathDB);
                    }
                    $isInserted = $profileDal->insert();
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'INSERT');
                    $answer = ['profileInsert', $isInserted, $logEntry];
                }
                else
                {
                    $answer = ['profileInsert', FALSE, 'Niet alle data ontvangen.'];
                }
                echo json_encode($answer);
                break;

            case 'profileUpdate' :
                $answer;
                if(isset($_POST['brand'], $_POST['type'], $_POST['id']))
                {
                    $profileBll->setId($_POST['id']);
                    $profileBll->setBrand($_POST['brand']);
	                $profileBll->setType($_POST['type']);
                    if(isset($_POST['img'])) 
                    {
                        $imgBinary = prepareImageForSave($_POST['img']); //helpers cyl
                        $brand = $profileBll->getBrand();
                        $type = $profileBll->getType();
                        //2 filepaths omdat het opslaan van de afbeelding van hieruit gebeurt
                        //maar het opvragen achteraf van de index.php. Het pad dat we gaan opslaan
                        //in de DB is dus anders dan het pad dat we hier gebruiken om de afbeeling op te
                        //slaan op de server.
                        $filepath = '../../images/profiles/' . $brand . '_' . $type . '.png';
                        $filepathDB = 'images/profiles/' . $brand . '_' . $type . '.png';
                        file_put_contents($filepath, $imgBinary);
                        //nagaan of filepath gewijzigd is, zo ja het oude file verwijderen van server
                        if(isset($_POST['path']))
                        {
                            if($_POST['path'] != $filepathDB)
                            {   
                                deleteImageFromServer($_POST['path']); //helpers cyl
                            }
                        }
                        $profileBll->setImg($filepathDB);
                    }
                    //wanneer er geen afbeelding gekozen is maar er wel al 1 gekoppeld was voor de wijziging
                    else
                    {
                        if(isset($_POST['path']))
                        {
                            deleteImageFromServer($_POST['path']);
                        }
                    }
                    $isUpdated = $profileDal->update();
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'UPDATE');
                    $answer = ['profileUpdate', $isUpdated, $logEntry];
                }
                else
                {
                    $answer = ['profileUpdate', FALSE, 'Niet alle data ontvangen.'];
                }
                echo json_encode($answer);
                break;

            case 'profileDelete' :
                $answer;
                if(isset($_POST['id'])) 
                {
                    $profileBll->setId($_POST['id']);
                    $isDeleted = $profileDal->delete(); 
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'DELETE');
                    //image file verwijderen van server
                    if($isDeleted)
                    {
                        if(isset($_POST['path']))
                        {
                            deleteImageFromServer($_POST['path']);
                        }
                    }
                    $answer = ['profileDelete', $isDeleted, $logEntry];
                }
                else
                {
                    $answer = ['profileDelete', FALSE, 'Niet alle data ontvangen.'];
                }
                echo json_encode($answer);
                break;

            case 'profileSelectById' :
                if(isset($_POST['id']))
                {
                    $profileBll->setId($_POST['id']);
                    $profile = $profileDal->selectById();
                    if($profile != FALSE)
                    {
                        echo json_encode($profile[0]);
                    } 
                    else echo FALSE;
                }
                else echo FALSE;
                break;

        //-----SLUITPLANNEN-----
            case 'masterKeyPlanSelectAll' :
                $listMkp = $mkpDal->selectAll();
                if($listMkp != FALSE)
                {
                    echo json_encode($listMkp);
                }
                else echo FALSE;
                break;                    

            case 'masterKeyPlanSelectLikeXNumber' :
                if(isset($_POST['number']))
                {
                    $mkpBll->setNumber($_POST['number']);
                    $listMkp = $mkpDal->selectLikeXNumber();
                    if($listMkp != FALSE)
                    {
                        echo json_encode($listMkp);
                    }
                    else echo FALSE;                
                }
                break;

            case 'masterKeyPlanSelectById' :
                if(isset($_POST['id']))
                {
                    $mkpBll->setId($_POST['id']);
                    $mkp = $mkpDal->selectById();
                    if($mkp != FALSE)
                    {
                        echo json_encode($mkp[0]);
                    } 
                    else echo FALSE;
                }
                break;
            
            case 'masterKeyPlanSelectByIdProfile' :
                if(isset($_POST['idProfile']))
                {
                    $mkpBll->setIdProfile($_POST['idProfile']);
                    $listMkp = $mkpDal->selectByIdProfile();
                    if($listMkp != FALSE)
                    {
                        echo json_encode($listMkp);
                    }
                    else echo FALSE;                
                }
                break;

            case 'masterKeyPlanInsert' :
                $answer;
                if(isset($_POST['number'], $_POST['date'], $_POST['profileId'])) 
                {
                    $mkpBll->setNumber($_POST['number']);
	                $mkpBll->setDate($_POST['date']);
                    $mkpBll->setIdProfile($_POST['profileId']);
                    if(isset($_POST['invoice'])) 
                    {
                        $mkpBll->setInvoice($_POST['invoice']);
                    }
                    if(isset($_POST['description'])) 
                    {
                        $mkpBll->setDescription($_POST['description']);
                    }
                    $isInserted = $mkpDal->insert();
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'INSERT');
                    $mkpId = -1;
                    if($isInserted)
                    {
                        $mkpId = $mkpBll->getId();
                    }
                    $answer = ['masterKeyPlanInsert', $isInserted, $mkpId, $logEntry];
                }
                else
                {
                    $answer = ['masterKeyPlanInsert', FALSE, -1, 'Niet alle data ontvangen.'];
                }
                echo json_encode($answer);
                break;

            case 'masterKeyPlanUpdate' :
                $answer;
                if(isset($_POST['number'], $_POST['date'], $_POST['profileId'], $_POST['id'])) 
                {
                    $mkpBll->setId($_POST['id']);
                    $mkpBll->setNumber($_POST['number']);
	                $mkpBll->setDate($_POST['date']);
                    $mkpBll->setIdProfile($_POST['profileId']);
                    if(isset($_POST['invoice'])) 
                    {
                        $mkpBll->setInvoice($_POST['invoice']);
                    }
                    if(isset($_POST['description'])) 
                    {
                        $mkpBll->setDescription($_POST['description']);
                    }
                    $isUpdated = $mkpDal->update();
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'UPDATE');
                    $answer = ['masterKeyPlanUpdate', $isUpdated, $_POST['id'], $logEntry];
                }
                else
                {
                    $answer = ['masterKeyPlanUpdate', FALSE, -1, 'Niet alle data ontvangen.'];
                }
                echo json_encode($answer);
                break;

            case 'masterKeyPlanDelete' :
                //gekoppelde cilinders en sleutels eerst verwijderen
                $answer;
                if(isset($_POST['id'])) 
                {
                    $keysDeleted = TRUE;
                    $cylsDeleted = TRUE;
                    //opvragen gekoppelde cilinders
                    $cylinderBll->setIdMasterKeyPlan($_POST['id']);
                    $listCyls = $cylinderDal->selectByIdMasterKeyPlan();
                    //opvragen gekoppelde sleutels
                    $keyBll->setIdMasterKeyPlan($_POST['id']);
                    $listKeys = $keyDal->selectByIdMasterKeyPlan();
                    //voor elke sleutel de koppelingen met cilinders opvragen en verwijderen
                    foreach($listKeys as $key)
                    {
                        $cylinderKeyBll->setIdKey($key['Id']);
                        $listCylsOnKey = $cylinderKeyDal->selectByIdKey();
                        $couplingDeleted = TRUE;
                        if($listCylsOnKey != FALSE)
                        {
                            //de koppelingen verwijderen
                            foreach($listCylsOnKey as $couple)
                            {
                                $cylinderKeyBll->setId($couple['Id']);
                                if(!$cylinderKeyDal->delete()) $couplingDeleted = FALSE;
                            }
                        }
                        //als het verwijderen van de koppelingen is gelukt de sleutel verwijderen
                        if($couplingDeleted == TRUE)
                        {
                            $keyBll->setId($key['Id']);
                            if(!$keyDal->delete()) $keysDeleted = FALSE;
                        }
                        //als koppelingen verwijderen is mislukt het verwijderen van het sluitplan op false zetten
                        else
                        {
                            $keysDeleted = FALSE;
                            $answer = ['masterKeyPlanDelete', FALSE, 
                                'Fout bij het verwijderen van gekoppelde sleutels/cilinders'];
                        }
                    }
                    //als de sleutels verwijderd zijn gaan we ook de cilinders verwijderen
                    if($keysDeleted == TRUE)
                    {
                        foreach($listCyls as $cylinder)
                        {
                            $cylinderBll->setId($cylinder['Id']);
                            if(!$cylinderDal->delete()) $cylsDeleted = FALSE;
                        }
                    }
                    //als de sleutels niet verwijderd zijn het verwijderen van het sluitplan op false zetten
                    else
                    {
                        $cylsDeleted = FALSE;
                        $answer = ['masterKeyPlanDelete', FALSE, 
                            'Fout bij het verwijderen van gekoppelde sleutels/cilinders'];
                    }
                    //als alle sleutels en cilinders verwijderd zijn het sluitplan zelf verwijderen
                    if($cylsDeleted == TRUE && $keysDeleted == TRUE)
                    {
                        $mkpBll->setId($_POST['id']);
                        $isDeleted = $mkpDal->delete(); 
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'DELETE');
                        $answer = ['masterKeyPlanDelete', $isDeleted, $logEntry];
                    }
                }
                else
                {
                    $answer = ['masterKeyPlanDelete', FALSE, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer);
                break;

            case'masterKeyPlanConsult' :
                $answer;
                $answerCyls;
                $answerKeys;
                if(isset($_POST['idMkp']))
                {
                    //cilinders opvragen met de koppelingen naar sleutels
                    $cylinderBll->setIdMasterKeyPlan($_POST['idMkp']);
                    $listCyls = $cylinderDal->selectByIdMasterKeyPlan();
                    if($listCyls != FALSE)
                    {
                        //id, name en index uithalen, alsook de koppelingen
                        $answerCyls = [];
                        foreach($listCyls as $row)
                        {
                            $coupledKeys = [];
                            //koppeling met sleutels in array opnemen
                            $cylinderKeyBll->setIdCylinder($row['Id']);
                            $tempCouples = $cylinderKeyDal->selectByIdCylinder();
                            if($tempCouples != FALSE)
                            {
                                foreach($tempCouples as $couple)
                                {
                                    $tempKey = array(
                                        "IdKey"=>$couple['IdKey']
                                    );
                                    array_push($coupledKeys, $tempKey);
                                }
                            }
                            //benodigde cilindereigenschappen in array plaatsen en 
                            //toevoegen aan de array van cilinders die we gaan terugsturen
                            $tempCyl = array(
                                "Id"=>$row['Id'], 
                                "Name"=>$row['Name'], 
                                "Index"=>$row['Index'],
                                "Keys"=>$coupledKeys

                            );
                            array_push($answerCyls, $tempCyl);
                        }
                    }
                    else $answerCyls = FALSE;
                    //sleutels opvragen
                    $keyBll->setIdMasterKeyPlan($_POST['idMkp']);
                    $listKeys = $keyDal->selectByIdMasterKeyPlan();
                    if($listKeys != FALSE)
                    {
                        //id, name en index uithalen
                        $answerKeys = [];
                        foreach($listKeys as $row)
                        {
                            $tempArray = array(
                                "Id"=>$row['Id'], 
                                "Name"=>$row['Name'], 
                                "Index"=>$row['Index']
                                );
                            array_push($answerKeys, $tempArray);
                        }
                    }
                    else $answerKeys = FALSE;
                    $answer = [$answerCyls, $answerKeys];
                }
                else $answer = [FALSE, FALSE];
                echo json_encode($answer);
                break;

        //-----CILINDERS-----
            case 'cylinderSelectById' :
                if(isset($_POST['id']))
                {
                    $cylinderBll->setId($_POST['id']);
                    $cylinder = $cylinderDal->selectById();
                    if($cylinder != FALSE)
                    {
                        echo json_encode($cylinder[0]);
                    } 
                    else echo FALSE;
                }
                break;

            case 'cylinderSelectByIdMasterKeyPlan' :
                if(isset($_POST['idMkp'])) 
                {
                    $cylinderBll->setIdMasterKeyPlan($_POST['idMkp']);
                    $listCyls = $cylinderDal->selectByIdMasterKeyPlan();
                    if($listCyls != FALSE)
                    {
                        echo json_encode($listCyls);
                    }
                    else echo FALSE;
                }
                break;

            case 'cylinderInsert' :
                $answer;
                if(isset($_POST['index'], $_POST['mkpId'], $_POST['name'], $_POST['cylinderSizeId'], 
                    $_POST['initialQuantity'], $_POST['active'], $_POST['passive'], $_POST['lateral']))
                {
                    //cylindercode-Id verkrijgen
                    $codeId = NULL;
                    //eerst checken of code misschien al bestaat
                    $cylinderCodeBll->setActive($_POST['active']);
                    $cylinderCodeBll->setPassive($_POST['passive']);
                    $cylinderCodeBll->setLateral($_POST['lateral']);
                    //als er geen ophoogcodes ingegeven zijn op -1 zetten voor db
                    if (isset($_POST['add1'])) 
                    {
                        $cylinderCodeBll->setAdd1($_POST['add1']);
                    }
                    else $cylinderCodeBll->setAdd1(-1);
                    if (isset($_POST['add2']))
                    {
                        $cylinderCodeBll->setAdd2($_POST['add2']);
                    } 
                    else $cylinderCodeBll->setAdd2(-1);
                    if (isset($_POST['add3']))
                    {
                        $cylinderCodeBll->setAdd3($_POST['add3']);
                    } 
                    else $cylinderCodeBll->setAdd3(-1);
                    $listCodes = $cylinderCodeDal->selectByCode();
                    if($listCodes != FALSE) 
                    {
                        $codeId = $listCodes[0]['Id'];
                    }
                    else
                    {
                        $isInserted = $cylinderCodeDal->insert();
                        if($isInserted)
                        {
                            $codeId = $cylinderCodeBll->getId();
                        }
                    }
                    if($codeId != NULL )
                    {
                        $cylinderBll->setIdCylinderCode($codeId);
                        $cylinderBll->setIdMasterKeyPlan($_POST['mkpId']);
                        $cylinderBll->setIdCylinderSize($_POST['cylinderSizeId']);
                        $cylinderBll->setIndex($_POST['index']);
                        $cylinderBll->setName($_POST['name']);
                        $cylinderBll->setInitialQuantity($_POST['initialQuantity']);
                        if (isset($_POST['backorder']))
                        {
                            $cylinderBll->setBackorder($_POST['backorder']);
                        } 
                        $isInserted = $cylinderDal->insert();
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'INSERT');
                        $answer = ['cylinderInsert', $isInserted, $_POST['mkpId'], $logEntry];
                    }
                    else 
                    {
                        $answer = ['cylinderInsert', FALSE, -1, 
                            'Fout bij het invoeren van de cilindercode'];
                    }
                }
                else
                {
                    $answer = ['cylinderInsert', FALSE, -1, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer);   
                break;

            case 'cylinderUpdate' :
                $answer;
                if(isset($_POST['index'], $_POST['mkpId'], $_POST['name'], $_POST['cylinderSizeId'], 
                    $_POST['initialQuantity'], $_POST['active'], $_POST['passive'], $_POST['lateral'], $_POST['id']))
                {
                    //cylindercode-Id verkrijgen
                    $codeId = NULL;
                    $cylinderCodeBll->setActive($_POST['active']);
                    $cylinderCodeBll->setPassive($_POST['passive']);
                    $cylinderCodeBll->setLateral($_POST['lateral']);
                    if (isset($_POST['add1'])) 
                    {
                        $cylinderCodeBll->setAdd1($_POST['add1']);
                    }
                    else $cylinderCodeBll->setAdd1(-1);
                    if (isset($_POST['add2']))
                    {
                        $cylinderCodeBll->setAdd2($_POST['add2']);
                    } 
                    else $cylinderCodeBll->setAdd2(-1);
                    if (isset($_POST['add3']))
                    {
                        $cylinderCodeBll->setAdd3($_POST['add3']);
                    } 
                    else $cylinderCodeBll->setAdd3(-1);
                    $listCodes = $cylinderCodeDal->selectByCode();
                    //als het dezelfde of een andere reeds bestaande code is
                    if($listCodes != FALSE) 
                    {
                        $codeId = $listCodes[0]['Id'];
                    }
                    else
                    {
                        //als het een nieuwe code is, niet gewoon de code updaten omdat er
                        //eventueel nog een andere cilinder aan die code gekoppeld kan zijn.
                        //we gaan dan gewoon de gewijzigde code als nieuwe invoegen
                        $isInserted = $cylinderCodeDal->insert();
                        if($isInserted)
                        {
                            $codeId = $cylinderCodeBll->getId();
                        }
                    }
                    if($codeId != NULL )
                    {
                        $cylinderBll->setId($_POST['id']);
                        $cylinderBll->setIdCylinderCode($codeId);
                        $cylinderBll->setIdMasterKeyPlan($_POST['mkpId']);
                        $cylinderBll->setIdCylinderSize($_POST['cylinderSizeId']);
                        $cylinderBll->setIndex($_POST['index']);
                        $cylinderBll->setName($_POST['name']);
                        $cylinderBll->setInitialQuantity($_POST['initialQuantity']);
                        if (isset($_POST['backorder']))
                        {
                            $cylinderBll->setBackorder($_POST['backorder']);
                        } 
                        $isUpdated = $cylinderDal->update();
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'UPDATE');
                        $answer = ['cylinderUpdate', $isUpdated, $_POST['mkpId'], $logEntry];
                    }
                    else 
                    {
                        $answer = ['cylinderUpdate', FALSE, -1, 
                            'Fout bij het invoeren van de cilindercode'];
                    }
                }
                else 
                {
                    $answer = ['cylinderUpdate', FALSE, -1, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer); 
                break;

            case 'cylinderDelete' :
                $answer;
                if(isset($_POST['id'], $_POST['mkpId'])) 
                {
                    $cylinderBll->setId($_POST['id']);
                    $isDeleted = $cylinderDal->delete(); 
                    $logbook = $log->getBook();                    $logEntry = $log->getLogTextByName($logbook, 'DELETE');
                    $answer = ['cylinderDelete', $isDeleted, $_POST['mkpId'], $logEntry];
                }
                else
                {
                    $answer = ['cylinderDelete', FALSE, -1, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer);
                break;

        //-----SLEUTELS-----
            case 'keySelectById' :
                if(isset($_POST['id']))
                {
                    $keyBll->setId($_POST['id']);
                    $key = $keyDal->selectById();
                    if($key != FALSE)
                    {
                        echo json_encode($key[0]);
                    } 
                    else echo FALSE;
                }
                break;    

            case 'keySelectByIdMasterKeyPlan' :
                if(isset($_POST['idMkp'])) 
                {
                    $keyBll->setIdMasterKeyPlan($_POST['idMkp']);
                    $listKeys = $keyDal->selectByIdMasterKeyPlan();
                    if($listKeys != FALSE)
                    {
                        echo json_encode($listKeys);
                    }
                    else echo FALSE;
                }
                break;

            case 'keyInsert' :
                $answer;
                if(isset($_POST['index'], $_POST['mkpId'], $_POST['name'], $_POST['initialQuantity'], 
                    $_POST['active'], $_POST['passive'], $_POST['lateral'], $_POST['cylinders']))
                {
                    //keycode-Id verkrijgen
                    $codeId = NULL;
                    //eerst checken of code misschien al bestaat
                    $keyCodeBll->setActive($_POST['active']);
                    $keyCodeBll->setPassive($_POST['passive']);
                    $keyCodeBll->setLateral($_POST['lateral']);
                    $listCodes = $keyCodeDal->selectByCode();
                    if($listCodes != FALSE) 
                    {
                        $codeId = $listCodes[0]['Id'];
                    }
                    else
                    {
                        $isCodeInserted = $keyCodeDal->insert();
                        if($isCodeInserted)
                        {
                            $codeId = $keyCodeBll->getId();
                        }
                    }
                    //sleutel inserten
                    if($codeId != NULL )
                    {
                        $keyBll->setIdKeyCode($codeId);
                        $keyBll->setIdMasterKeyPlan($_POST['mkpId']);
                        $keyBll->setIndex($_POST['index']);
                        $keyBll->setName($_POST['name']);
                        $keyBll->setInitialQuantity($_POST['initialQuantity']);
                        if (isset($_POST['backorder']))
                        {
                            $keyBll->setBackorder($_POST['backorder']);
                        } 
                        $isInserted = $keyDal->insert();
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'INSERT');
                        //als sleutel is toegevoegd koppelen aan de juiste cilinders
                        $isCoupled = FALSE;
                        if($isInserted)
                        {
                            $cyls = json_decode($_POST['cylinders']);
                            $cylinderKeyBll->setIdKey($keyBll->getId());
                            $isCoupled = TRUE;
                            foreach ($cyls as $value)
                            {
                                $cylinderKeyBll->setIdCylinder($value);
                                if(!$cylinderKeyDal->insert()) $isCoupled = FALSE;
                            }
                        }
                        $answer = ['keyInsert', $isInserted, $_POST['mkpId'], $isCoupled, $logEntry];
                    }
                    else 
                    {
                        $answer = ['keyInsert', FALSE, -1, FALSE, 
                            'Fout bij het invoeren van de sleutelcode'];
                    }    
                }
                else
                {
                    $answer = ['keyInsert', FALSE, -1, FALSE, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer); 
                break;

            case 'keyUpdate' :
                $answer;
                if(isset($_POST['id'], $_POST['index'], $_POST['mkpId'], $_POST['name'], $_POST['initialQuantity'], 
                    $_POST['active'], $_POST['passive'], $_POST['lateral'], $_POST['cylinders']))
                {
                    //keycode-Id verkrijgen
                    $codeId = NULL;
                    $keyCodeBll->setActive($_POST['active']);
                    $keyCodeBll->setPassive($_POST['passive']);
                    $keyCodeBll->setLateral($_POST['lateral']);
                    $listCodes = $keyCodeDal->selectByCode();
                    //als het dezelfde of een andere reeds bestaande code is
                    if($listCodes != FALSE) 
                    {
                        $codeId = $listCodes[0]['Id'];
                    }
                    else
                    {
                        //als het een nieuwe code is, niet gewoon de code updaten omdat er
                        //eventueel nog een andere cilinder aan die code gekoppeld kan zijn.
                        //we gaan dan gewoon de gewijzigde code als nieuwe invoegen
                        $isInserted = $keyCodeDal->insert();
                        if($isInserted)
                        {
                            $codeId = $keyCodeBll->getId();
                        }
                    }
                    //sleutel updaten
                    if($codeId != NULL )
                    {
                        $keyBll->setId($_POST['id']);
                        $keyBll->setIdKeyCode($codeId);
                        $keyBll->setIdMasterKeyPlan($_POST['mkpId']);
                        $keyBll->setIndex($_POST['index']);
                        $keyBll->setName($_POST['name']);
                        $keyBll->setInitialQuantity($_POST['initialQuantity']);
                        if (isset($_POST['backorder']))
                        {
                            $keyBll->setBackorder($_POST['backorder']);
                        } 
                        $isUpdated = $keyDal->update();
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'UPDATE');
                        //als sleutel is gewijzigd koppelen aan de juiste cilinders
                        $isCoupled = FALSE;
                        if($isUpdated)
                        {
                            //nieuwe koppelingen
                            $cylsNew = json_decode($_POST['cylinders']);
                            //oude koppelingen opvragen
                            $cylinderKeyBll->setIdKey($keyBll->getId());
                            $cylsOld = $cylinderKeyDal->selectByIdKey();
                            $isCoupled = TRUE;
                            if($cylsOld != FALSE)
                            {
                                //nieuwe lijst doorlopen en nagaan of cilinder in oude lijst staat
                                foreach($cylsNew as $newCylId)
                                {
                                    $isExisting = FALSE;
                                    foreach($cylsOld as $key=>$row)
                                    {
                                        if($row['IdCylinder'] == $newCylId)
                                        {
                                            //ja = koppeling bestaat al, id verwijderen uit oude lijst
                                            $isExisting = TRUE;
                                            unset($cylsOld[$key]);
                                        }
                                    }
                                    if(!$isExisting)
                                    {
                                        //neen = koppeling aanmaken
                                        $cylinderKeyBll->setIdCylinder($newCylId);
                                        if(!$cylinderKeyDal->insert()) $isCoupled = FALSE;
                                    }
                                }
                                //kijken of er nog id's over zijn in de oude lijst, zo ja deze koppelingen verwijderen
                                foreach($cylsOld as $row)
                                {
                                    $cylinderKeyBll->setId($row['Id']);
                                    if(!$cylinderKeyDal->delete()) $isCoupled = FALSE;
              
                                }
                            }
                            else $isCoupled = FALSE;
                        }
                        $answer = ['keyUpdate', $isUpdated, $_POST['mkpId'], $isCoupled, $logEntry];
                    }
                    else 
                    {
                        $answer = ['keyUpdate', FALSE, -1, FALSE,
                            'Fout bij het invoeren van de sleutelcode'];
                    }    
                }
                else
                {
                    $answer = ['keyUpdate', FALSE, -1, FALSE, 
                        'Niet alle data ontvangen'];
                }
                echo json_encode($answer); 
                break;

            case 'keyDelete' :
                $answer;
                if(isset($_POST['id'], $_POST['mkpId'])) 
                {
                    //eerst koppelingen ophalen en verwijderen
                    $cylinderKeyBll->setIdKey($_POST['id']);
                    $listCylsOnKey = $cylinderKeyDal->selectByIdKey();
                    $couplingDeleted = TRUE;
                    if($listCylsOnKey != FALSE)
                    {
                        foreach($listCylsOnKey as $row)
                        {
                            $cylinderKeyBll->setId($row['Id']);
                            if(!$cylinderKeyDal->delete()) $couplingDeleted = FALSE;
                        }
                    }
                    //als het verwijderen van de koppelingen is gelukt de sleutel verwijderen
                    if($couplingDeleted == TRUE)
                    {
                        $keyBll->setId($_POST['id']);
                        $isDeleted = $keyDal->delete(); 
                        $logbook = $log->getBook();                        $logEntry = $log->getLogTextByName($logbook, 'DELETE');
                        $answer = ['keyDelete', $isDeleted, $_POST['mkpId'], $logEntry];
                    }
                    //als koppelingen verwijderen is mislukt de sleutel ook niet verwijderen
                    else
                    {
                        $answer = ['keyDelete', FALSE, -1, 
                            'Fout bij het verwijderen van de cilinderkoppelingen'];
                    }
                }
                else
                {
                    $answer = ['keyDelete', FALSE, -1, 'Niet alle data ontvangen'];
                }
                echo json_encode($answer);
                break;

        //-----TUSSENTABEL CILINDERS-SLEUTELS-----
            case 'cylinderKeySelectByIdKey' :
                if(isset($_POST['idKey'])) 
                {
                    $cylinderKeyBll->setIdKey($_POST['idKey']);
                    $listCylsOnKey = $cylinderKeyDal->selectByIdKey();
                    if($listCylsOnKey != FALSE)
                    {
                        echo json_encode($listCylsOnKey);
                    }
                    else echo FALSE;
                }
                break;

            case 'cylinderKeySelectByIdCylinder' :
                if(isset($_POST['idCylinder'])) 
                {
                    $cylinderKeyBll->setIdCylinder($_POST['idCylinder']);
                    $listKeysOnCyl = $cylinderKeyDal->selectByIdCylinder();
                    if($listKeysOnCyl != FALSE)
                    {
                        echo json_encode($listKeysOnCyl);
                    }
                    else echo FALSE;
                }
                break;

        //-----MEMBERSHIP-----
            case 'login' :
                $result = FALSE;
                if(isset($_POST['username'], $_POST['password'])) 
                { 
                    $result = $membership->login($_POST['username'], $_POST['password']);
                }
                echo $result;
                break;

            case 'register' :
                //result een array [member insert, memberrole insert];
                $result = [FALSE, FALSE];
                if(isset($_POST['usernameregister'], $_POST['emailregister'], $_POST['passwordregister'], 
                    $_POST['roleId']))
                {
                    //gebruiker toevoegen
                    $memberId = $membership->register($_POST['usernameregister'], 
                        $_POST['passwordregister'], $_POST['emailregister']);
                    if($memberId != FALSE)
                    {
                        //rol aan gebruiker toekennen
                        $roleAdded = $membership->addRole($memberId, $_POST['roleId']);
                        $result = [TRUE, $roleAdded];
                    } 
                }
                echo json_encode($result);
                break;

            case 'logout' :
                $membership->logOut();
                break;

            case 'getAllUsers' :
                $answer = FALSE;
                $users = $membership->getAllMembers();
                if($users != FALSE)
                {
                    $answer = json_encode($users);
                }
                echo $answer;
                break;

            case 'userDelete' :
                $answer = FALSE;
                if(isset($_POST['memberId']))
                {
                    $rolesDeleted = $membership->removeRoles($_POST['memberId']);
                    if($rolesDeleted == TRUE)
                    {
                        $answer = $membership->deleteMember($_POST['memberId']);
                    }
                }
                echo $answer;
                break;

            default:
                break;
        } //end switch
    }

    $connection->close();
    
?>

