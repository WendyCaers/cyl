<?php
    namespace AnOrmApart\Helpers;
    /**
    * Logbook class.
    * This class logs feedback objects
    * @lastmodified 
    * @since 25/8/2014          
    * @author Entreprise de Modes et de Manieres Modernes - e3M       
    * @version 0.1
    */    
    class Log
    {
        private $book;

        public function __construct()
        {
            $this->book = array();
        }

        public function getBook()
        {
            return $this->book;
        }

        // return one entry in the log based on the name
        public function getEntry($name) 
        {
            return $this->book[$name];
            
        }

        //zelf toegevoegd: geeft de text en boodschap van de error terug 
        //wanneer er een entry is waar de opgegeven naam in voorkomt
        public function getLogTextByName($logbook, $name)
        {
            $logText = 'Geen log-entry gevonden.';
            if (count($logbook) > 0)
		    {
			    foreach ($logbook as $key => $feedback){
			        if(strpos($feedback->getName(),$name) !== false){
			            $logText = $feedback->getText() . ' ' . $feedback->getErrorMessage();
			        } 
			    }
            }
            return $logText;
        }

        public function push($feedbackObject)
        {
            // haal de naam eruit want die wil ik
            // als string index gebruiken voor 
            // de book array
            // naam is niet hooflettergevoelig
            $name = $feedbackObject->getName();
            $this->book[$name] = $feedbackObject;
        }

        public function delete($name)
        {
            $entry = $this->getEntry($name);
            // unset is of type void
            unset($this->book[$name]);
            return $entry;
        }

        public function clear() 
        {
            $this->book = array();
        }

        public function append($value)
        {
            if (count($this->book) == 0)
            {
                $this->book = $value;
            }
            else
            {
                $this->book = array_merge($this->book, $value);
            }
        }
        
    }
?>

