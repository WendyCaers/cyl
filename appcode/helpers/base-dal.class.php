<?php
namespace AnOrmApart\Base;
class Dal
{
	protected $feedback;
	protected $log;
	protected $rowCount;
	protected $connection;
	protected $bdo;

	// feedback and log getters
	public function getFeedback()
	{
		return $this->feedback;
	}

	public function getLog()
	{
		return $this->log;
	}

	public function book()
	{
		return $this->log->getBook();
	}

	protected function log()
	{
		$this->log->push($this->feedback->copy());
	}

	// database related getters
	public function getRowCount()
	{
		return $this->rowCount;
	}

	public function getBdo()
	{
		return $this->bdo;
	}

	public function setBdo($value)
	{
		$this->bdo = $value;
	}

	public function setConnection($connection)
	{
		$this->connection = $connection;
	}

	public function getConnection()
	{
		return $this->connection;
	}

	public function __construct($log)
	{
		$this->feedback = new \AnOrmApart\Helpers\Feedback();
		$this->log = $log;
		$this->bdo = new Bll($log);
        //weggelaten want ik werk met 1 algemene connectie voor alle
        //dal objecten 
	//	$this->connection = new \AnOrmApart\Dal\Connection($log);
	}

	public function startFeedback($name)
	{
		$this->feedback->start($name);
	}

	public function endFeedback($name)
	{
		$this->feedback->end();
	}
}
?>