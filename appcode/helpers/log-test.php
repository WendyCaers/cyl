<?php
    include ('feedback.class.php');
    include ('log.class.php');
    // instanciate feedback class
    // make an object of the Feedback class
    // object maken van de Feedback klasse
    $feedback = new \AnOrmApart\Helpers\Feedback();
    $log = new \AnOrmApart\Helpers\Log();
    // first feedback
    $feedback->setText('Feedback');
    $feedback->setErrorCode('000');
    $feedback->setErrorMessage('Feedback errormessage');
    $feedback->setErrorCodeDriver('Feedback errorCodeDriver');
    $feedback->setIsError(TRUE);
    $feedback->setName('test');
    $feedback->setStartTime();
    $feedback->setEndTime();
    // now remember this feedback
    $log->push($feedback->copy());
    // the second feedback
    // de gebruiksprocedure
    // de Start methode initialiseert de naam, 
    // registreert de starttijd en zet
    // alle andere waarden op de standaardwaarde
    $feedback->Start('de gebruiksprocedure');
    $feedback->End();
    // remember the second feedback
    $log->push($feedback->copy());
    $logBook = $log->getBook();
    // print_r($log);
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Log test</title>
    </head>
    <body>
        <?php
            foreach ($logBook as $key => $feedback)
            {
        ?>
        <h1><?php echo $key;?></h1>
        <p><b>Name</b> <?php echo $feedback->getName();?></p>
        <p><b>Feedback</b> <?php echo $feedback->getText();?></p>
        <p><b>Error code</b> <?php echo $feedback->getErrorCode();?></p>
        <p><b>Error message</b> <?php echo $feedback->getErrorMessage();?></p>
        <p><b>Error Code Driver</b> <?php echo $feedback->getErrorCodeDriver();?></p>
        <p><b>Is error</b> <?php echo $feedback->getIsError();?></p>
        <p><b>Start</b> <?php echo $feedback->getStartTime();?></p>
        <p><b>End</b> <?php echo $feedback->getEndTime();?></p>

        <?php
            }
        ?>
    </body>
</html>


