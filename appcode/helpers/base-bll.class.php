<?php
namespace AnOrmApart\Base;
class Bll
{
	protected $insertedBy;
	protected $insertedOn;
	protected $updatedBy;
	protected $updatedOn;
	// feedback and log fields
	protected $feedback;
	protected $log;

	public function __construct($log)
	{
		$this->feedback = new \AnOrmApart\Helpers\Feedback();
		$this->log = $log; 
	}

	// validation method returns log
	// if log is empty, no errors
	//public function isValid()
	//{
	//	return (count($this->log->getBook()) == 0);
	//}

	public function book()
	{
		return $this->log->getBook();
	}

	protected function log()
	{
		$this->log->push($this->feedback->copy());
	}

    //toegevoegd om vanuit de dal de feedback van de bll te kunnen
    //opvragen om een de geldigheid van de ingevoerde gegevens testen
    public function getFeedback()
    {
        return $this->feedback;
    }

	public function getInsertedBy()
	{
		return $this->insertedBy;
	}

	public function getInsertedOn()
	{
		return $this->insertedOn;
	}

	public function getUpdatedBy()
	{
		return $this->updatedBy;
	}

	public function getUpdatedOn()
	{
		return $this->updatedOn;
	}


	public function setInsertedBy($value)
	{
		$this->insertedBy = $value;
	}

	public function setInsertedOn()
	{
		$this->insertedOn = date("Y-m-d H:i:s");
	}

	public function setUpdatedBy($value)
	{
		$this->updatedBy = $value;
	}

	public function setUpdatedOn()
	{
		$this->updatedOn = date("Y-m-d H:i:s");
	}

    
}

?>

