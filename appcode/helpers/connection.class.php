<?php
    /**
        Connection class
        @lastmodified 25 august 2014
        @author CW
        @version 0.0
    */

    // name of namespace should be semantically meaningfull:
    // cover the domain of the code
    namespace AnOrmApart\Dal;

    class Connection 
    {
        protected $pdo;
        protected $databaseName;
	    private $feedback;
        protected $log;

	    public function getFeedback()
	    {
		    return $this->feedback;
	    }

	    public function getPdo()
	    {
		    return $this->pdo;
	    }

	    public function isConnected()
	    {
            return $this->pdo;
	    }

	    public function getDatabaseName()
	    {
		    return $this->databaseName;
	    }
	    public function setDatabaseName($value)
	    {
		    $this->databaseName = $value;
	    }

        // constructor wordt uitgevoerd met
        // het new keyword
        // Als de constructor van de klasse
        // die overerft iets moet doen
        // dat de constructor van de basisklasse
        // niet doet, kan je de constructor
        // van de basisklasse oproepen
        public function __construct($log)
        {
            //\AnOrmApart\Helpers\Feedback::__construct();
		    $this->feedback = new \AnOrmApart\Helpers\Feedback();
            $this->feedback->startTimeInKey('connection');
            $this->databaseName = 'stshbe1q_cyl';
            $this->log = $log;
        }

	    protected function log()
	    {
		    $this->log->push($this->feedback->copy());
	    }

        /**
        * Maakt een connectie met de database
        * @return Bool true als de connectie is gelukt, false als mislukt
        */
        public function open()
        {
            if ($this->pdo)
            {
                    $this->feedback->setText("Already connected to {$this->databaseName} database.");
                    $this->log();
            }
            else
            {
                try
                {
                    //om lokaal te werken
                    //$connectionString = 
                        //"mysql:host=195.238.74.69;dbname={$this->databaseName}";
                    //om te publishen: aanvraag komt van WW51 server
                    $connectionString = 
                        "mysql:host=localhost;dbname={$this->databaseName}";
                    // werkt ook met inloggevens cpanel
                    $password = 'test123';
                    $userName = 'stshbe1q_test123';                    
                    // je moet aangeven dat de PDO klasse in de root namespace
                    // gezocht moet worden in niet in AnOrmApart\Dal
                    //new pdo return true on succes and false on failure
                    $this->pdo = new \PDO($connectionString, 
                        $userName, $password);
                        //$test = $this->getPdo();
                        //print_r($test);
                        //$preparedStatement = $this->pdo->prepare('CALL ProfileSelectAll()');
				        //$preparedStatement->execute();
				        //$result = $preparedStatement->fetchAll();
                        //print_r($result);
                    $this->feedback->setText("Connected to {$this->databaseName}.");
 			        $this->feedback->setErrorCodeDriver('AnOrmApart DAL Connction');
                    $this->log();
                }
                catch (\PDOException $e)
                {
                    $this->feedback->setText("Connection with database {$this->databaseName} failed.");
				    $this->feedback->setErrorMessage('Fout: ' . $e->getMessage());
				    $this->feedback->setErrorCode($e->getCode());
 			        $this->feedback->setErrorCodeDriver('AnOrmApart DAL Connection');
                    $this->feedback->End();
                    $this->log();
               }
            }
            return (!is_null($this->pdo));
        }

        public function close()
        {
            if (is_null($this->pdo))
            {
                $this->feedback->setText("Not connected to {$this->databaseName}.");
                $this->log();
            }
            else
            {
                $this->pdo = NULL;
                $this->feedback->End();
                $this->log();
            }
        }
    }
?>

