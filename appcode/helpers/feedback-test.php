<?php
    include ('feedback.class.php');
    // instanciate feedback class
    // make an object of the Feedback class
    // object maken van de Feedback klasse
    $feedback = new \AnOrmApart\Helpers\Feedback();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Feedback test</title>
    </head>
    <body>
        <?php
            $feedback->setText('Feedback');
            $feedback->setErrorCode('000');
            $feedback->setErrorMessage('Feedback errormessage');
            $feedback->setErrorCodeDriver('Feedback errorCodeDriver');
            $feedback->setIsError(TRUE);
            $feedback->setName('test');
            $feedback->setStartTime();
            $feedback->setEndTime();
        ?>
        <h1>De getters en setters testen</h1>
        <p><b>Name</b> <?php echo $feedback->getName();?></p>
        <p><b>Feedback</b> <?php echo $feedback->getText();?></p>
        <p><b>Error code</b> <?php echo $feedback->getErrorCode();?></p>
        <p><b>Error message</b> <?php echo $feedback->getErrorMessage();?></p>
        <p><b>Error Code Driver</b> <?php echo $feedback->getErrorCodeDriver();?></p>
        <p><b>Is error</b> <?php echo $feedback->getIsError();?></p>
        <p><b>Start</b> <?php echo $feedback->getStartTime();?></p>
        <p><b>End</b> <?php echo $feedback->getEndTime();?></p>

        <!-- gebruiksprocedure -->
        <?php
            // de Start methode initialiseert de naam, 
            // registreert de starttijd en zet
            // alle andere waarden op de standaardwaarde
            $feedback->Start('Plopperdeplop');
        ?>
        <h1>De Start methode testen</h1>
        <p><b>Name</b> <?php echo $feedback->getName();?></p>
        <p><b>Feedback</b> <?php echo $feedback->getText();?></p>
        <p><b>Error code</b> <?php echo $feedback->getErrorCode();?></p>
        <p><b>Error message</b> <?php echo $feedback->getErrorMessage();?></p>
        <p><b>Error Code Driver</b> <?php echo $feedback->getErrorCodeDriver();?></p>
        <p><b>Is error</b> <?php echo $feedback->getIsError();?></p>
        <p><b>Start</b> <?php echo $feedback->getStartTime();?></p>
        <p><b>End</b> <?php echo $feedback->getEndTime();?></p>
    </body>
</html>


