<?php
	include ('feedback.class.php');
	include ('log.class.php');
	include ('base-bll.class.php');
	// only required when there is a password property
	// include ('../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	$bll = new AnOrmApart\Base\Bll($log);
	// test setters
	// remember start timestap for later testing
	$startTimeStamp = date("Y-m-d H:i:s");
	$bll->setInsertedBy('InsertedBy');
	$bll->setInsertedOn();
	$bll->setUpdatedBy('UpdatedBy');
	$bll->setUpdatedOn();
	$logbook = $log->getBook();
	//print_r(logbook);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL</title>
	</head>
	<body>
		<?php
		if (count($logbook) > 0)
		{
			foreach ($logbook as $key => $feedback)
			{?>
			<h1><?php echo $feedback->getName();?></h1>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Base</legend>
			<div>
				<label>Inserted by</label>
				<span><?php echo $bll->getInsertedBy();?></span>
			</div>
			<div>
				<label>Inserted on</label>
				<span><?php echo $bll->getInsertedOn();?></span>
			</div>
			<div>
				<label>Updated by</label>
				<span><?php echo $bll->getUpdatedBy();?></span>
			</div>
			<div>
				<label>Updated on</label>
				<span><?php echo $bll->getUpdatedOn();?></span>
			</div>
		</fieldset>
	</body>
</html>


