<?php
    namespace AnOrmApart\Web;
    class Session
    {
        protected $name;
        protected $https;
        protected $httpOnly;

        public function Start() 
        {
            ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
            $cookieParams = session_get_cookie_params(); // Gets current cookies params.
            session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $this->https, $this->httpOnly); 
            session_name($this->name); // Sets the session name to the one set above.
            session_start(); // Start the php session
            session_regenerate_id(); // regenerated the session, delete the old one.  
        }

        public function End()
        {
            // session is started in the constructor
            // Unset all session values
            $_SESSION = array();
            // get session parameters 
            $params = session_get_cookie_params();
            // Delete the actual cookie.
            setcookie(session_name(), '', time() - 42000, 
                $params['path'], 
                $params['domain'], 
                $params['secure'], 
                $params['httponly']);
            // Destroy session
            session_destroy();
        }
    }

?>
