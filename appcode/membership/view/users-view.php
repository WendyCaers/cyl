<div id="users" class="popup">
    <div class="popup-header _1x1x5">
        <a id="usersClose" class="close-popup" href="#first-floor">
            <span class="icon" data-icon="9"></span>
            <span class="screen-reader-text">Back</span>
        </a> <!--sluiten-->
        <h1>Gebruikers</h1>
    </div>
    <div class="popup-body _4x1x5">
        <div class="_1x2x1 scrollable">
            <ul id="listUsers"></ul>
        </div>
        <div class="_1x2x1">
            <div class="_2x1x3">
                <span class="icon picture" data-icon=")"></span>
            </div>
            <div class="_1x1x3">
                <button id="userDeleteButton" type="button" value="userDelete" class="membership-button">
                    <span class="icon" data-icon="x"></span>
                </button>
                <button id="userAddButton" type="button" class="membership-button">
                    <span class="icon" data-icon="@"></span>
                </button>
            </div>
        </div>
    </div>
</div>