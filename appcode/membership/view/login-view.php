<div id="login" class="popup">
    <div class="popup-header _1x1x5">
        <a id="loginClose" class="close-popup" href="#first-floor">
            <span class="icon" data-icon="9"></span>
            <span class="screen-reader-text">Back</span>
        </a> <!--sluiten-->
        <h1>Aanmelden</h1>
    </div>
    <div class="popup-body _4x1x5">
        <div id="loginFeedback" class="feedback _1x1x5"></div>
        <div class="_1x1x5 inputWithError">
            <label for="username" data-icon=")" >Gebruikersnaam: </label>
            <input id="username" name="username" type="text" placeholder="bv. MyName " />
            <label class="error" id="username-error"></label>
        </div>
        <div class="_1x1x5 inputWithError">
            <label for="password" data-icon="w">Wachtwoord: </label>
            <input id="password" name="password" type="password" placeholder="bv. X8df!90EO" />
            <label class="error" id="password-error"></label>
        </div>
        <div class="_2x1x5">
            <button id="loginButton" class="membership-button" type="button" value="login">Aanmelden</button>
        </div>
    </div>
</div>