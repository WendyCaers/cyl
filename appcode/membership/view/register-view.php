<div id="register" class="popup">
    <div class="popup-header _1x1x5">
        <a id="registerClose" class="close-popup" href="#first-floor">
            <span class="icon" data-icon="9"></span>
            <span class="screen-reader-text">Back</span>
        </a> <!--sluiten-->
        <h1>Registreren</h1>
    </div>
    <div class="popup-body _4x1x5">
        <div id="registerFeedback" class="feedback _1x1x10"></div>
        <div class="_1x1x10 inputWithError">
            <label for="usernameregister" data-icon=")" >Gebruikersnaam: </label>
            <input id="usernameregister" name="usernameregister"  placeholder="bv. MyName" type="text" />
            <label class="error" id="usernameregister-error"></label>
        </div>
        <div class="_1x1x10 inputWithError">
            <label for="emailregister" data-icon="(">Email: </label>
            <input id="emailregister" name="emailregister" type="email" placeholder="bv. mymail@mail.com" />
            <label class="error" id="emailregister-error"></label>
        </div>
        <div class="_1x1x10 inputWithError">
            <label for="passwordregister" data-icon="w">Wachtwoord: </label>
            <input id="passwordregister" name="passwordregister" type="password" placeholder="bv. X8df!90EO" />
            <label class="error" id="passwordregister-error"></label>
        </div>
        <div class="_1x1x10 inputWithError">
            <label for="passwordregister-confirm" data-icon="w">Wachtwoord bevestigen: </label>
            <input id="passwordregister-confirm" name="passwordregister-confirm" 
                type="password" placeholder="bv. X8df!90EO" />
            <label class="error" id="passwordregister-confirm-error"></label>
        </div>
        <div class="_1x1x10 inputWithError">
            <label for="roleregister">Accounttype: </label>
            <select id="roleregister">
                <option value="none">--Kies een accounttype--</option>
                <option value="2">Gebruiker</option>
                <option value="1">Administrator</option>
            </select>
            <label class="error" id="roleregister-error"></label>
        </div>
        <div class="_2x1x5">
            <button id="registerButton" class="membership-button" type="button" value="register">
                Registreren
            </button>
        </div>
    </div>
</div>
