<?php
namespace AnOrmApart\LoginAttempt;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT LoginAttempt');
            $this->feedback->setContext('LoginAttempt');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL LoginAttemptInsert(@pId, :pIdMember, :pTime,  :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pTime', $this->bdo->getTime(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is geïnserted.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure LoginAttemptInsert(). Rij is niet geïnserted.');
					
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptInsert');
                $this->feedback->setContext('LoginAttempt');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT LoginAttempt');
            $this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if ($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE LoginAttempt');
            $this->feedback->setContext('LoginAttempt');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL LoginAttemptUpdate(:pId, :pIdMember, :pTime, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pTime', $this->bdo->getTime(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is geüpdate.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure LoginAttemptUpdte(). Rij is niet geüpdated.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet geUpdated.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptUpdate');
                $this->feedback->setContext('LoginAttempt');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet geüpdated.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE LoginAttempt');
            $this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL LoginAttemptDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijerd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure LoginAttemptDelete(). Rij is niet verwijderd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
		}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptDelete');
                $this->feedback->setContext('LoginAttempt');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE LoginAttempt');
            $this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
        $this->feedback->setContext('LoginAttempt');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL LoginAttemptSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setIdMember($array['IdMember']);
					$this->bdo->setTime($array['Time']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is niet gevonden.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptSelectOne');
                $this->feedback->setContext('LoginAttempt');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet geselecteerd.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SelectOne LoginAttempt');
			$this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL LoginAttemptSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText('Tabel LoginAttempt ingelezen.');
				}
				else
				{
					$this->feedback->setText('Tabel LoginAttempt is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptSelectAll');
                $this->feedback->setContext('LoginAttempt');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rijen zijn niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SelectAll LoginAttempt');
			$this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('LoginAttemptSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL LoginAttemptSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met {$this->bdo->getId()} 
                        in tabel LoginAttempt gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel LoginAttempt is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptSelectById');
                $this->feedback->setContext('LoginAttempt');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet ingelezen.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SelectById LoginAttempt');
			$this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

	public function selectByIdMember()
	{
		$this->feedback->startTimeInKey('LoginAttemptSelectByIdMember');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL LoginAttemptSelectByIdMember(:pIdMember)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met {$this->bdo->getIdMember()} 
                        in tabel LoginAttempt gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel LoginAttempt is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL LoginAttemptSelectOne');
                $this->feedback->setContext('LoginAttempt');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen ingelezen.');
			$this->feedback->setErrorMessage('Not connected to LoginAttempt');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SelectByIdMember LoginAttempt');
			$this->feedback->setContext('LoginAttempt');
		}
		$this->log();
		return $result;
	}

}

namespace AnOrmApart\Member;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Member');
            $this->feedback->setContext('Member');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberInsert(@pId, :pUserName, :pEmail, :pPassword, :pAuthenticated, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pUserName', $this->bdo->getUserName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pEmail', $this->bdo->getEmail(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pPassword', $this->bdo->getPassword(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pAuthenticated', $this->bdo->getAuthenticated(), \PDO::PARAM_BOOL);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberInsert(). Rij is niet toegevoegd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberInsert');
                $this->feedback->setContext('Member');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Member');
            $this->feedback->setContext('Member');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberUpdate(:pId, :pUserName, :pEmail, :pPassword, :pAuthenticated, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pUserName', $this->bdo->getUserName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pEmail', $this->bdo->getEmail(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pPassword', $this->bdo->getPassword(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pAuthenticated', $this->bdo->getAuthenticated(), \PDO::PARAM_BOOL);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberUpdte(). Rij is niet gewijzigd.');
					
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberUpdate');
                $this->feedback->setContext('Member');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Member');
             $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is  verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberDelete(). Rij is niet verwijderd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberDelete');
                $this->feedback->setContext('Member');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
        $this->feedback->setContext('Member');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setUserName($array['UserName']);
					$this->bdo->setEmail($array['Email']);
					$this->bdo->setPassword($array['Password']);
					$this->bdo->setLastActivity($array['LastActivity']);
					$this->bdo->setFirstLogin($array['FirstLogin']);
					$this->bdo->setAuthenticated($array['Authenticated']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is niet gevonden.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectOne');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL MemberSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText('Tabel Member ingelezen.');
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectAll');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('MemberSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel Member gevonden.";
					$this->feedback->setText($value);
                }
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectById');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}
    
	public function selectByUserName()
	{
		$this->feedback->startTimeInKey('MemberSelectByUserName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectByUserName(:pUserName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pUserName', $this->bdo->getUserName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getUserName()} in tabel Member gevonden.";
					$this->feedback->setText($value);
                    $array = $result[0];
					$this->bdo->setId($array['Id']);
					$this->bdo->setUserName($array['UserName']);
					$this->bdo->setEmail($array['Email']);
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectByUserName');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYUSERNAME Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectByEmail()
	{
		$this->feedback->startTimeInKey('MemberSelectByEmail');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectByEmail(:pEmail)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pEmail', $this->bdo->getEmail(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getEmail()} in tabel Member gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectByEmail');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYEMAIL Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectLikeId()
	{
		$this->feedback->startTimeInKey('MemberSelectLikeId');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectLikeId(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel Member gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectLikeId');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
         else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEID Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectLikeUserName()
	{
		$this->feedback->startTimeInKey('MemberSelectLikeUserName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectLikeUserName(:pUserName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pUserName', $this->bdo->getUserName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getUserName()} in tabel Member gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectLikeUserName');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEUSERNAME Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}

	public function selectLikeEmail()
	{
		$this->feedback->startTimeInKey('MemberSelectLikeEmail');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberSelectLikeEmail(:pEmail)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pEmail', $this->bdo->getEmail(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getEmail()} in tabel Member gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Member is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberSelectLikeEmail');
                $this->feedback->setContext('Member');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Member');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKEEMAIL Member');
            $this->feedback->setContext('Member');
		}
		$this->log();
		return $result;
	}
}


namespace AnOrmApart\Role;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Role');
            $this->feedback->setContext('Role');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL RoleInsert(@pId, :pName, :pDescription, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDescription', $this->bdo->getDescription(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure RoleInsert(). Rij is niet toegevoegd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleInsert');
                $this->feedback->setContext('Role');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT Role');
            $this->feedback->setContext('Role');      
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Role');
            $this->feedback->setContext('Role');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL RoleUpdate(:pId, :pName, :pDescription, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pDescription', $this->bdo->getDescription(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure RoleUpdte(). Rij is niet gewijzigd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleUpdate');
                $this->feedback->setContext('Role');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL RoleDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure RoleDelete(). Rij is niet verwijderd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleDelete');
                $this->feedback->setContext('Role');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
        $this->feedback->setContext('Role');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL RoleSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setName($array['Name']);
					$this->bdo->setDescription($array['Description']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is niet gevonden.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleSelectOne');
                $this->feedback->setContext('Role');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL RoleSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText('Tabel Role ingelezen.');
				}
				else
				{
					$this->feedback->setText('Tabel Role is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleSelectAll');
                $this->feedback->setContext('Role');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('RoleSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL RoleSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel Role gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Role is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleSelectById');
                $this->feedback->setContext('Role');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function selectByName()
	{
		$this->feedback->startTimeInKey('RoleSelectByName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL RoleSelectByName(:pName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getName()} in tabel Role gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Role is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleSelectByName');
                $this->feedback->setContext('Role');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYNAME Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}

	public function selectLikeName()
	{
		$this->feedback->startTimeInKey('RoleSelectLikeName');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL RoleSelectLikeName(:pName)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pName', $this->bdo->getName(), \PDO::PARAM_STR);
				$preparedStatement->execute();
			    $this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getName()} in tabel Role gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel Role is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('Role');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL RoleSelectLikeName');
                $this->feedback->setContext('Role');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to Role');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTLIKENAME Role');
            $this->feedback->setContext('Role');
		}
		$this->log();
		return $result;
	}
}

namespace AnOrmApart\MemberRole;
class Dal extends \AnOrmApart\Base\Dal
{
	public function insert()
	{
		$this->feedback->startTimeInKey('insert');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet inserten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberRoleInsert(@pId, :pIdMember, :pIdRole, :pInsertedBy)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdRole', $this->bdo->getIdRole(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pInsertedBy', $this->bdo->getInsertedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					// fetch the output
					$this->bdo->setId($this->connection->getPdo()->query('select @pId')->fetchColumn());
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is toegevoegd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberRoleInsert(). Rij is niet toegevoegd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet toegevoegd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleInsert');
                $this->feedback->setContext('MemberRole');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet toegevoegd.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL INSERT MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function update()
	{
		$this->feedback->startTimeInKey('update');
		$result = FALSE;
		if($this->bdo->getFeedback()->getIsError())
		{
			$this->feedback->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->feedback->setErrorMessage('zie BLL feedback voor details');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		elseif ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberRoleUpdate(:pId, :pIdMember, :pIdRole, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pIdRole', $this->bdo->getIdRole(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gewijzigd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberRoleUpdte(). Rij is niet gewijzigd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gewijzigd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleUpdate');
                $this->feedback->setContext('MemberRole');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet gewijzigd.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL UPDATE MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function delete()
	{
		$this->feedback->startTimeInKey('delete');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberRoleDelete(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is verwijderd.');
					$result = TRUE;
				}
				else
				{
					$this->feedback->setText('Fout in stored procedure MemberRoleDelete(). Rij is niet verwijderd.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet verwijderd.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleDelete');
                $this->feedback->setContext('MemberRole');
			}
		}
		else
		{
			$this->feedback->setText('Rij is niet verwijderd.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL DELETE MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function selectOne()
	{
		$this->feedback->startTimeInKey('select one');
        $this->feedback->setContext('MemberRole');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberRoleSelectOne(:pId)');
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				$array = $preparedStatement->fetch(\PDO::FETCH_ASSOC);
				if ($array)
				{
					$this->bdo->setId($array['Id']);
					$this->bdo->setIdMember($array['IdMember']);
					$this->bdo->setIdRole($array['IdRole']);
					$this->bdo->setInsertedBy($array['InsertedBy']);
					$this->bdo->setInsertedOn($array['InsertedOn']);
					$this->bdo->setUpdatedBy($array['UpdatedBy']);
					$this->bdo->setUpdatedOn($array['UpdatedOn']);
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is gevonden.');
					//Since column in the where must be primary key
					// rowCount() should return 1. Can be used as validation.
					$this->rowCount = $preparedStatement->rowCount();
					$result = ($preparedStatement->rowCount() == 1);
				}
				else
				{
					$this->feedback->setText('Rij met id <b> ' . $this->bdo->getId() . '</b> is niet gevonden.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectOne');
                $this->feedback->setContext('MemberRole');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTONE MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function selectAll()
	{
		$this->feedback->startTimeInKey('select all');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->prepare('CALL MemberRoleSelectAll()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback->setText('Tabel MemberRole ingelezen.');
				}
				else
				{
					$this->feedback->setText('Tabel MemberRole is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectAll');
                $this->feedback->setContext('MemberRole');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTALL MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function selectById()
	{
		$this->feedback->startTimeInKey('MemberRoleSelectById');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberRoleSelectById(:pId)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pId', $this->bdo->getId(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getId()} in tabel MemberRole gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MemberRole is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Rij is niet gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectById');
                $this->feedback->setContext('MemberRole');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Rij is niet gevonden.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYID MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function selectByIdMember()
	{
		$this->feedback->startTimeInKey('MemberRoleSelectByIdMember');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberRoleSelectByIdMember(:pIdMember)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdMember()} in tabel MemberRole gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MemberRole is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectByIdMember');
                $this->feedback->setContext('MemberRole');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDMEMBER MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}

	public function selectByIdRole()
	{
		$this->feedback->startTimeInKey('MemberRoleSelectByIdRole');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberRoleSelectByIdRole(:pIdRole)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdRole', $this->bdo->getIdRole(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($result = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdRole()} in tabel MemberRole gevonden.";
					$this->feedback->setText($value);
				}
				else
				{
					$this->feedback->setText('Tabel MemberRole is leeg.');
				}
                $sQLErrorInfo = $preparedStatement->errorInfo();
				$this->feedback->setErrorCode($sQLErrorInfo[0]);
				$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->feedback->setErrorMessage($sQLErrorInfo[2]);
                $this->feedback->setContext('MemberRole');
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectByIdRole');
                $this->feedback->setContext('MemberRole');
				$this->rowCount = -1;
			}
		}
        else
		{
			$this->feedback->setText('Geen rijen gevonden.');
			$this->feedback->setErrorMessage('Not connected to MemberRole');
			$this->feedback->setErrorCodeDriver('none');
			$this->feedback->setErrorCode('DAL SELECTBYIDROLE MemberRole');
            $this->feedback->setContext('MemberRole');
		}
		$this->log();
		return $result;
	}
}

?>