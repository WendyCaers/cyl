<?php
// overwrite the standard AnOrmApart\LoginAttempt\Dal class
namespace AnOrmApart\Membership\LoginAttempt;
class Dal extends \AnOrmApart\LoginAttempt\Dal
{
	public function IsBruteForceAttack()
	{
		$this->feedback->startTimeInKey('isBruteForceAttack');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
                // Get timestamp of current time 
                $now = time();
                 // All login attempts are counted from the past 2 hours. 
                $validSince = $now - (2 * 60 * 60);
                // Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
                prepare("CALL LoginAttemptSelectByIdMember(:pIdMember)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($this->rowCount > 3)
				{
                    $attemptsAll = $preparedStatement->fetchAll();
                    $attemptsLastTwoHours = 0;
                    foreach($attemptsAll as $row)
                    {
                        if($row['Time'] > $validSince) $attemptsLastTwoHours ++;
                    }
                    if($attemptsLastTwoHours > 3) $result = $attemptsLastTwoHours;
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('CATCH: er is iets misgelopen.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('LoginAttempt DAL IsBruteForceAttack');
				$this->rowCount = -1;
			}
		}
		$this->log();
		return $result;
	}
}

// overwrite the standard AnOrmApart\MembreRole\Dal class
namespace AnOrmApart\Membership\MemberRole;
class Dal extends \AnOrmApart\MemberRole\Dal
{
	public function selectByIdMemberRoleNameOnly()
	{
		$this->feedback->startTimeInKey('MemberRoleSelectByIdMemberRoleNameOnly');
		$result = FALSE;
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare("CALL MemberRoleSelectByIdMemberRoleNameOnly(:pIdMember)");
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				// fetch the output
				if ($array = $preparedStatement->fetchAll())
				{
					$value = "{$preparedStatement->rowCount()} rij(en) met 
                        {$this->bdo->getIdMember()} in tabel MemberRole gevonden.";
					$this->feedback->setText($value);
                    // convert to single array
                    foreach ($array as $element => $value)
                    {
                        $result[] = $value['Name'];
                    }
				}
				else
				{
					$this->feedback->setText('Tabel MemberRole is leeg.');
					$sQLErrorInfo = $preparedStatement->errorInfo();
					$this->feedback->setErrorCode($sQLErrorInfo[0]);
					$this->feedback->setErrorCodeDriver($sQLErrorInfo[1]);
					$this->feedback->setErrorMessage($sQLErrorInfo[2]);
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback->setText('Geen rijen gevonden.');
				$this->feedback->setErrorMessage($e->getMessage());
				$this->feedback->setErrorCodeDriver($e->getCode());
				$this->feedback->setErrorCode('DAL MemberRoleSelectByIdMemberRoleNameOnly');
				$this->rowCount = -1;
			}
		}
		$this->log();
		return $result;
	}
}

namespace AnOrmApart;
class Membership extends \AnOrmApart\Web\Session
{
    private $authenticated;
    private $loggedIn;
    private $userName;
    private $email;
    private $memberDal;
    private $memberBll;
    private $loginAttemptDal;
    private $loginAttemptBll;
    private $memberRoleBll;
    private $memberRoleDal;
    private $log;
    // array to store roles for one member
    private $memberRoles;

    public function getMemberBll()
    {
        return $this->memberBll;
    }

    public function getLoginAttemptBll()
    {
        return $this->loginAttemptBll;
    }

    public function getMemberRoleBll()
    {
        return $this->memberRoleBll;
    }

    public function getMemberDal()
    {
        return $this->memberDal;
    }

    public function getLoginAttemptDal()
    {
        return $this->loginAttemptDal;
    }
 
    public function getMemberRoleDal()
    {
        return $this->memberRoleDal;
    }

    public function getMemberRoles()
    {
        return $this->memberRoles;
    }

    public function isAuthenticated()
    {
        return ($this->authenticated && $this->loggedIn);
    }

    public function isLoggedIn()
    {
        return $this->loggedIn;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function getLoginText()
    {
        //heb er loginCheck van gemaakt ipv loggedIn want anders werkte het niet
        return ($this->loginCheck() ? 'afmelden' : 'aanmelden');
    }

    /*
    *   get the link (href) for the login link
    */
    public function getLoginHyperlink()
    {
        //aangepast om te werken met ajax
        //return ($this->loginCheck() ? htmlentities($_SERVER['PHP_SELF']) . '?action=logout' : '#to-login');
        return ($this->loginCheck() ? '#first-floor'  : '#to-login');
    }

    public function __construct($connection) 
    {
        $log = new \AnOrmApart\Helpers\Log();
        // visitor is not yet authenticated
        $this->authenticated = FALSE;
        $this->loggedIn = FALSE;
        // Set a custom session name, secure membership
        // abbreviated to sec_ms
        $this->name = 'sec_ms'; 
        // Set to true if using https.
        $this->https = FALSE; 
        // This stops javascript being able to access the session id.
        $this->httponly = true;  
        $this->Start(); 
        $this->memberBll = new \AnOrmApart\Member\Bll($log);
        $this->memberDal = new \AnOrmApart\Member\Dal($log);
        $this->loginAttemptBll = new \AnOrmApart\LoginAttempt\Bll($log);
        $this->loginAttemptDal = new \AnOrmApart\Membership\LoginAttempt\Dal($log);
        $this->memberRoleBll = new \AnOrmApart\MemberRole\Bll($log);
        $this->memberRoleDal = new \AnOrmApart\Membership\MemberRole\Dal($log);
	    // connect
        $this->memberDal->setConnection($connection);
        $this->loginAttemptDal->setConnection($connection);
        $this->memberRoleDal->setConnection($connection);
        // pass the business dataobject to the DAL class
	    $this->memberDal->setBdo($this->memberBll);
	    $this->loginAttemptDal->setBdo($this->loginAttemptBll);
	    $this->memberRoleDal->setBdo($this->memberRoleBll);
    }
    
    function __destruct() 
    {
	    // disconnect
	    //$this->memberDal->disConnect();
	    //$this->loginAttemptDal->disConnect();
    }

    public function register($userName, $password, $email)
    {
        $answer = FALSE;
        $this->userName = $userName;
        $this->email = $email;
        $this->memberBll->setUserName($userName);   
        $this->memberBll->setEmail($email);   
        $this->memberBll->hashPassword($password);
        $this->memberBll->setInsertedBy('Cyl Register');
        $isInserted = $this->memberDal->Insert();
        if($isInserted)
        {
            $answer = $this->memberBll->getId();
        }
        else $answer = FALSE;
        return $answer;
    }

    //zelf toegevoegd om user te kunnen verwijderen
    public function deleteMember($memberId)
    {
        $this->memberBll->setId($memberId);
        return $this->memberDal->delete();
    }

    //zelf toegevoegd om rollen te kunnen toekennen aan gebruikers
    public function addRole($memberId, $roleId)
    {
        $this->memberRoleBll->setIdMember($memberId);
        $this->memberRoleBll->setIdRole($roleId);
        $isInserted = $this->memberRoleDal->insert();
        return $isInserted;
    }

    //zelf toegevoegd om rollen te kunnen verwijderen
    public function removeRoles($memberId)
    {
        //standaard op TRUE zetten want als er geen rollen gekoppeld zijn aan de gebruikers
        //moet er toch verder kunnen gegaan worden met het verwijderen van de gebruiker zelf.
        //indien er natuurlijk een fout zit in het ophalen van de rollen gaat hij toch nog true
        //teruggeven zonder de rollen te hebben verwijderd. Dit is een afweging die is gemaakt
        //om de koppeling naar de rollen te laten staan en toch de gebruiker te verwijderen ipv 
        //dat het verwijderen blokkeert en mislukt. Voor de klant is het belangrijker dat de gebruiker
        //geen gebruik meer kan maken van de app dan dat er 1 niet gebruikt record overblijf in de memberRoles
        //Er zullen toch maar enkele gebruikers worden geregistreerd.
        $rolesDeleted = TRUE;
        $this->memberRoleBll->setIdMember($memberId);
        $memberRoles = $this->memberRoleDal->selectByIdMember();
        if($memberRoles != NULL && $memberRoles != FALSE)
        {
            foreach($memberRoles as $role)
            {
                $this->memberRoleBll->setId($role['Id']);
                if(!$this->memberRoleDal->delete()) $rolesDeleted = FALSE;
            }
        }
        return $rolesDeleted;
    }

    public function login($userName, $password) 
    {
        $this->memberBll->setUserName($userName);
        // see of the user exists
        $this->memberDal->selectByUserName();
        if ($this->memberDal->getRowCount() == 1)
        {
            // We check if the account is locked from too many login attempts
            if($this->IsBruteForceAttack($this->memberBll->getId()))
            { 
                // Account is locked
                return -1;
            } 
            else 
            {
                // fetch all columns from table
                // SelectBy fetches only searchable columns
                $this->memberDal->selectOne();
                // Check if the password in the database matches
                // the password provided by the user.
                if ($this->memberBll->verifyPassword($password))
                { 
                    // Get the user-agent string of the user.
                    $userBrowser = $_SERVER['HTTP_USER_AGENT']; 
                    // XSS protection as we might print this value
                    // Wat blijft er dan nog van over, lege string?
                    // nee, er staat een kapje voor, dit wil zeggen dat 
                    // alle tekens die geen cijfers zijn worden vervangen
                    // door niets!
                    $userId = preg_replace("/[^0-9]+/", "", $this->memberBll->getId());
                    $_SESSION['userId'] = $userId;
                    // XSS protection as we might print this value
                    $this->username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $this->memberBll->getUserName()); 
                    $_SESSION['userName'] = $this->username;
                    $_SESSION['loginString'] = hash('sha512', $this->memberBll->getPassword() . $userBrowser);
                    $this->authenticated = $this->memberBll->getAuthenticated();
                    // update
                    $this->memberBll->setLastActivity(date("Y-m-d H:i:s"));
                    $this->memberBll->setUpdatedOn();
                    $this->memberBll->setUpdatedBy('Membership Login');
                    $this->memberDal->update();
                    // get the roles for this memeber
                    $this->memberRoleBll->setIdMember($this->memberBll->getId());
                    $this->memberRoles = $this->memberRoleDal->selectByIdMemberRoleNameOnly();
                    $_SESSION['memberRoles'] = $this->memberRoles;
                    // Login successful.
                    $this->loggedIn = TRUE;
                    return true;    
                } 
                else 
                {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $this->loginAttemptBll->setIdMember($this->memberBll->getId());
        	        // pass the business dataobject to the DAL class
                    $this->loginAttemptBll->SetTime($now);
                    $this->loginAttemptBll->setInsertedBy('Membership Login');
                    $this->loginAttemptDal->insert();
                    return false;
                } 
            } // brute force
        }
        else 
        {
            // No user exists.
            return false;
        } 
    } 

    public function isBruteForceAttack($userId) 
    {
        $result = FALSE;
        $this->loginAttemptBll->setIdMember($userId);
        //retourneert het aantal mislukte inlogpogingen wanneer dit meer is dan 3,
        //anders FALSE
        $count = $this->loginAttemptDal->IsBruteForceAttack();
        if($count != FALSE)
        {
            //we bepalen dat een brute force attack
            //pas geldt vanaf meer dan 5 mislukte pogingen
            if($count > 5) $result = TRUE;
        }
        return $result;
    }

    public function loginCheck() 
    {
        $this->loggedIn = FALSE;
        // Check if all session variables are set
        if(isset($_SESSION['userId'], $_SESSION['userName'], $_SESSION['loginString'])) 
        {
            $userId = $_SESSION['userId'];
            $loginString = $_SESSION['loginString'];
            $userName = $_SESSION['userName'];
            // Get the user-agent string of the user.
            $userBrowser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
            $this->memberBll->setUserName($userName);
            $this->memberBll->setId($userId);
            // see of the user exists
            $this->memberDal->selectOne();
            if ($this->memberDal->GetRowCount() == 1)
            {
                    $loginCheck = hash('sha512', $this->memberBll->getPassword() . $userBrowser);
                    if($loginCheck == $loginString) 
                    {
                        $this->loggedIn = TRUE;
                        // Logged In
                    } 
            }
        }
        return $this->loggedIn;
    }

    public function logOut()
    {
        // method of session clas
        $this->End();
        $this->loggedIn = FALSE;
    }

    public function isInRole($value)
    {
        if($this->LoginCheck())
        {
            //memberroles in sessie geplaatst omdat na de reloads van de pagina de
            //variabel memberroles dan leeg is.
            $memberRoles = $_SESSION['memberRoles'];
            if($memberRoles != NULL )
            {
                //returns true when found in array
                return in_array($value, $memberRoles);
            }
            else return FALSE;
        }
        else
            return FALSE;
    }

    public function getAllMembers()
    {
        return $this->memberDal->selectAll();
    }
}
?>


