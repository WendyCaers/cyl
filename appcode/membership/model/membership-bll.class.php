<?php
namespace AnOrmApart\LoginAttempt;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $idMember;
	protected $time;

	public function getId()
	{
		return $this->id;
	}

	public function getIdMember()
	{
		return $this->idMember;
	}

	public function getTime()
	{
		return $this->time;
	}

	public function setId($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setIdMember($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idMember');
			$this->feedback->setText('idMember is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->idMember = $value;
		}
	}

	public function setTime($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('time');
			$this->feedback->setText('time is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->time = $value;
		}
	}
}

namespace AnOrmApart\Member;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $userName;
	protected $email;
	protected $password;
	protected $lastActivity;
	protected $firstLogin;
	protected $authenticated;

	public function getId()
	{
		return $this->id;
	}

	public function getUserName()
	{
		return $this->userName;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function verifyPassword($value)
	{
		return password_verify($value, $this->password);
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getLastActivity()
	{
		return $this->lastActivity;
	}

	public function getFirstLogin()
	{
		return $this->firstLogin;
	}

	public function getAuthenticated()
	{
		return $this->authenticated;
	}

	public function setId($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setUserName($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('userName');
			$this->feedback->setText('userName is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->userName = $value;
		}
	}

	public function setEmail($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('email');
			$this->feedback->setText('email is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
        elseif(!filter_var($value, FILTER_VALIDATE_EMAIL)) 
        {
            $this->feedback->start('email');
			$this->feedback->setText('ongeldig emailadres');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
        }
		if (!$this->feedback->getIsError())
		{
			$this->email = $value;
		}
	}

	public function hashPassword($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('password');
			$this->feedback->setText('password is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->password = password_hash($value,  PASSWORD_DEFAULT);
		}
	}

	public function setPassword($value)
	{
        $value = strip_tags($value);
		if(strlen($value) <= 0)
		{
			$this->feedback->start('password');
			$this->feedback->setText('password is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->password = $value;
		}
	}

	public function setLastActivity()
	{
		$this->lastActivity = date("Y-m-d H:i:s");
	}

	public function setFirstLogin()
	{
		$this->firstLogin = date("Y-m-d H:i:s");
	}

	public function setAuthenticated($value)
	{
        $value = strip_tags($value);
		$this->authenticated = $value;
	}
}

namespace AnOrmApart\Role;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $name;
	protected $description;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setId($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setName($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('name');
			$this->feedback->setText('name is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->name = $value;
		}
	}

	public function setDescription($value)
	{
		$this->description = $value;
	}
}

namespace AnOrmApart\MemberRole;
class Bll extends \AnOrmApart\Base\Bll
{
	protected $id;
	protected $idMember;
	protected $idRole;

	public function getId()
	{
		return $this->id;
	}

	public function getIdMember()
	{
		return $this->idMember;
	}

	public function getIdRole()
	{
		return $this->idRole;
	}

	public function setId($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('id');
			$this->feedback->setText('id is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->id = $value;
		}
	}

	public function setIdMember($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idMember');
			$this->feedback->setText('idMember is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->idMember = $value;
		}
	}

	public function setIdRole($value)
	{
		if(strlen($value) <= 0)
		{
			$this->feedback->start('idRole');
			$this->feedback->setText('idRole is verplicht veld');
			$this->feedback->setIsError(TRUE);
			$this->feedback->setErrorCodeDriver('AnOrmApart BLL');
			$this->feedback->end();
			$this->log();
		}
		if (!$this->feedback->getIsError())
		{
			$this->idRole = $value;
		}
	}
}

?>

